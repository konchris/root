# coding: utf-8
"""The Helper Module for setting up sample-specific generic IPython Notebooks

This module combines elements that are the same across all IPython Notebooks
for this sample.

"""
import os
import sys
import re
import traceback
import pandas as pd

HOME = os.path.expanduser('~')
DATA_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'data')
RAW_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'raw-data')
SAMPLE_DIR = os.path.join('sio2al005', 'cryo_measurements')
SAMPLE_RUN = os.path.join('2012-10-08')
RESULTS_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'results')
DEVICES = ['all', 'posFFT', 'negFFT']
SWEEPS = ['bsweep', 'tsweep', 'ivsweep', 'bramp']


def load_sweep_data(sweep_type, sample_dir=SAMPLE_DIR, sample_run=SAMPLE_RUN):
    """Load the data from the files listed in the file_list file_list_filename

    Returns
    -------
    df : dict
        A dictionary where the top level is the measurement key (i.e.
        temperature and maybe sweep direction), followed by dictionaries
        sorted by device name, then each device's data in a pandas DataFrame.
        pdf = {}
        df['0327mK up 01'] = {}
        df['0327mK up 01']['ADWin'] = pandas.DataFrame()
        df['0327mK up 01']['filename'] =
                '2015-03-26T17-09-31_BSweep_327mK_+300mT_1mT'
        df['0327mK up 01']['modified'] = False
    df_extra : pandas.DataFrame
        This contains
        1. The estimated critical fields (positive and negative)
        2. The coherence lengths (based on critical field)
        3. The resistance at the critical field
        4. The resistance in normal conducting regime

    """
    assert sweep_type in SWEEPS, (sweep_type + ' is not a valid sweep. Valid '
                                  'sweep types are: ' + ', '.join(SWEEPS))

    # Generate the filename of the file that has the list of the good data
    # files
    file_list_filename = '{}_files.csv'.format(sweep_type)
    full_path = os.path.join(DATA_DIR, sample_dir, sample_run,
                             file_list_filename)

    # Sometimes value errors were raised. I cannot remember anymore why.
    try:
        file_lists = pd.DataFrame.from_csv(full_path)
    except ValueError:
        print('ValueError raised for {}'.format(file_list_filename))

    # Count the number of sweeps at a specific temperature
    # i = 1

    # Initialize the empty data dictionary
    df = {}

    # Go through each file in the list and load its data
    for fname in file_lists['file name']:

        full_path = os.path.join(DATA_DIR, sample_dir, sample_run,
                                 fname + '.h5')

        for device in DEVICES:

            # Create the HDFStore object
            hdf_store = pd.HDFStore(full_path)

            # Generate the key under which the device's data was stored
            hdf_key = '/proc01/{}'.format(device)

            if 'bsweep' in sweep_type:
                temp = hdf_store['/proc01/all']['THe3'].mean()

            # print(hdf_key)

            # print(hdf_store.keys())

            if hdf_key in hdf_store.keys():

                try:
                    temp = hdf_store[hdf_key]['THe3'].mean()
                except KeyError:
                    pass

                if 'bsweep' in sweep_type:
                    key = '{t:04.0f} mK'.format(t=temp*1000)
                elif 'tsweep' in sweep_type:
                    key = fname.split('_')[-1]

                if key not in df:
                    df[key] = {}
                    df[key]['filename'] = fname
                    df[key]['modified'] = False

                if device not in df[key].keys():
                    try:
                        df[key][device] = hdf_store[hdf_key]
                    except KeyError:
                        pass

            hdf_store.close()

    return df


def save_data(df, sample_dir=SAMPLE_DIR, sample_run=SAMPLE_RUN, root='proc01'):
    """Save the data from the dataset back into the files.

    Parameters
    ----------
    df : dict
        A dictionary where the top level is the measurement key (i.e.
        temperature and maybe sweep direction), followed by dictionaries
        sorted by device name, then each device's data in a pandas DataFrame.
        df = {}
        df['0327mK up 01'] = {}
        df['0327mK up 01']['ADWin'] = pandas.DataFrame()
        df['0327mK up 01']['filename'] =
                '2015-03-26T17-09-31_BSweep_327mK_+300mT_1mT'
    sample_dir : str
    sample_run : str
    root : str
        The name of the root folder in the HDF5 file structure to place the
        data.
        DEFAULT 'proc01'

    """
    # Generate the filename of the file that has the list of the good data
    # files
    for key in df.keys():
        if df[key]['modified']:
            filename = df[key]['filename'] + '.h5'
            full_path = os.path.join(DATA_DIR, sample_dir, sample_run,
                                     filename)
            print('Saving {}'.format(filename))

            for device in DEVICES:

                if device in df[key]:

                    # Create the HDFStore object
                    hdf_store = pd.HDFStore(full_path)

                    # Generate the key under which the device's data was stored
                    hdf_key = '/{r}/{d}'.format(r=root, d=device)

                    if hdf_key in hdf_store:
                        test_df = hdf_store[hdf_key]
                        if not df[key][device].equals(test_df):
                            print('Writing {} data.'.format(device))
                            del test_df
                            hdf_store[hdf_key] = df[key][device]
                    else:
                        print('Writing {} data.'.format(device))
                        hdf_store.append(hdf_key, df[key][device])

                    hdf_store.close()
