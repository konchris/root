import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import os
sns.set_context("talk", font_scale=1.25, rc={"lines.linewidth": 3})
sns.set_style('whitegrid')
data_dir = '/home/chris/Documents/PhD/root/data'
sample = 'fonin_heliox'
sample_runs = ['2014-08-28-Testing-Run', '2014-09-12-Testing-Run',
               '2014-09-22-Testing-Run', '2014-10-08-Testing-Run',
               '2014-10-22-Testing-Run', '2014-10-28-Testing-Run']
results_dir = '/home/chris/Documents/PhD/root/results'
for sample_run in sample_runs:
    print(sample_run)
    full_dir = os.path.join(data_dir, sample, sample_run)
    for f in os.listdir(full_dir):
        print('\t', f)
        full_path = os.path.join(full_dir, f)
        if not f == '.directory':
            hdf_store = pd.HDFStore(full_path)
            try:
                df = hdf_store['/raw/ITC 503']
            except KeyError as err:
                print('Got the error {e} on file {f}'.format(e=err, f=f))
            hdf_store.close()
            df.index = df.index.tz_localize('UTC').tz_convert('Europe/Berlin')
            df = df[df > 0]
            start_time = df.index[0]
            df['Time_m'] = pd.Series((df.index.values - start_time.value).astype(np.int64)/60E9, index=df.index)

            fig, axes = plt.subplots()
            df.plot(ax=axes, x='Time_m')
            axes.set_title(f)
            axes.set_xlabel('Timestamp')
            axes.set_ylabel('Temperature [K]')
            axes.grid(True)
            fig_filename = '{}.png'.format(f.split('.')[0])
            full_fig_path = os.path.join(results_dir, sample, sample_run, fig_filename)
            print('\t', full_fig_path)
            if not os.path.exists(full_fig_path.rstrip(fig_filename)):
                os.makedirs(full_fig_path.rstrip(fig_filename))
            fig.savefig(full_fig_path, bbox_inches='tight')
