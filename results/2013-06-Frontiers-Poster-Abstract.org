#+OPTIONS: html-postamble:auto html-preamble:t tex:t
#+CREATOR: <a href="http://www.gnu.org/software/emacs/">Emacs</a> 24.3.1 (<a href="http://orgmode.org">Org</a> mode 8.0.2)
#+HTML_CONTAINER: div
#+HTML_DOCTYPE: <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
#+HTML_HEAD:
#+HTML_HEAD_EXTRA:
#+HTML_INCLUDE_SCRIPTS:
#+HTML_INCLUDE_STYLE:
#+HTML_LINK_HOME:
#+HTML_LINK_UP:
#+HTML_MATHJAX:
#+INFOJS_OPT:
#+TITLE: Large oscillations of the magnetoresistance in nanopatterned thin
#+TITLE: aluminum films
#+OPTIONS: texht:t toc:nil <:nil num:nil timestamp:nil 
#+LATEX_CLASS: koma-article
#+LATEX_CLASS_OPTIONS: [a4paper]
#+LATEX_HEADER: %\linespread{1.6}
#+LATEX_HEADER_EXTRA:
#+BIBLIOGRAPHY: /home/chris/Documents/PhD/Literature/Bibliography/phd_refs unsrt

* Abstract
Little and Parks showed with their experiments that the transition temperature
of a superconducting cylinder oscillates in an external magnetic field with a
period of $\Phi = h/2e$ \cite{Little62}. 
Theoretical studies have found that there should be a $h/e$ periodicity in
ring-shaped or networked nano-ring unconventional superconductors that can
arise via various mechanisms when the ring diameter becomes smaller than the
zero-temperature coherence length, $\xi_{0}$.
Proposed mechanisms that might give rise to $h/e$ periodicity include the
Aharonov-Bohm effect \cite{Wei08}, a mechanism based on the dependence of the
Cooper pair's internal energy on its motion \cite{Vakaryuk08}, or due to the
formation of quasi-particles \cite{Loder09}.
There is even speculation that this periodicity should be seen in rings made
out of aluminum, a conventional superconductor, with diameters smaller than the
coherence length \cite{Loder08prb}.

Carillo /et al./ investigated this proposed periodicity in a single YBCO loop
\cite{Carillo10}, Gammel /et al./ investigated simple networks of YBCO
\cite{Gammel90}, and Sochnikov /et al./ began investigating this proposed
periodicity in double network patterns of LSCO \cite{Sochnikov10prb}.
None of these investigations found the sought after periodicity and the
amplitude of the observed oscillations was much larger than one would expect
from the Little-Parks effect.
Sochnikov /et al./ propose that this enhanced amplitude is due to interaction
between thermally excited moving vortices and the oscillating persistent
currents in the loops.
Based on their experiments on a niobium ladder, Berdiyorov /et al./ propose that
enhanced amplitude is rather due to interaction between the applied current and
the Meissner currents in the superconductor \cite{Berdiyorov12}.

In this poster we show the results of the first experiments on such double
networks of aluminum nano-rings where we also see $h/2e$ oscillations with
amplitudes much larger than predicted by the Little-Parks effect.
We also did not find any indication of the $h/e$ periodicity, which may be due
to technical aspects of the experiment.
\cite{Annett09} [[bibtex:Annett09][10] [[bib:Annett09][11]

\bibliographystyle{unsrt}
\bibliography{/home/chris/Documents/PhD/Literature/Bibliography/phd_refs}

[2013-06-12 Wed 08:57]

#  LocalWords:  fluxoid
