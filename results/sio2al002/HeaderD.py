#!/usr/bin/env python
# coding: utf-8
import os
import sys
import datetime

sys.path.append(os.path.join(os.path.expanduser('~'), 'Documents', 'PhD',
                             'root', 'lib'))

import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
from matplotlib2tikz import save as tikz_save

from scipy.interpolate import interp1d, UnivariateSpline
from scipy.optimize import curve_fit

# The following is bad practice, but it works for me for now.
from SuperconductivityCalculations import my_tanh
from MiscFunctions import (nearest_value, save_figure, nextpow2, remove_valley,
                           plot_bound_data)
from MungingFunctions import (normalize_resistances, calculate_flux_quanta,
                              savitzky_golay)
from Helper_Mod import load_sweep_data, save_data
from Locations import DATA_DIR, RESULTS_DIR, PGF_DIR
from Constants import PHI_0
from generate_generic_fit import generate_x_fits

from sio2al002d import RES_DEVICE as res_device
from sio2al002d import NORMAL_RESISTANCE as normal_resistance
from sio2al002d import RESISTANCE_OFFSET as resistance_offset

from sio2al002d import RADIUS as r
from sio2al002d import FACTOR as factor
#from sio2al002d import B_OFFSETS
from sio2al002d import BOUNDARIES as boundaries
from sio2al002d import SMOOTHING as smoothing
#from sio2al002d import FIT_BOUNDARIES as fit_boundaries

sns.set_context("talk", font_scale=1.25, rc={"lines.linewidth": 1.5})
sns.set_style("whitegrid")

sample_name = 'sio2al002'
sample_network = '2d'
sample_run = 'cooldown-2012-04-12'
sample_name = os.path.join(sample_name, sample_network)
sample_directory = os.path.join(DATA_DIR, sample_name, 'cryo_measurements')
sample_results = os.path.join(RESULTS_DIR, sample_name, 'cryo_measurements')
pgf_path = os.path.join(PGF_DIR, sample_name, sample_run)


def main(argv=None):

    print("Nothing to see here. Move along, folks.")

    sys.exit()

if __name__ == "__main__":
    main()
