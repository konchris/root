#!/usr/bin/env python
# coding: utf-8
import os
import sys
import datetime

sys.path.append(os.path.join(os.path.expanduser('~'), 'Documents', 'PhD',
                             'root', 'lib'))

import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
from matplotlib2tikz import save as tikz_save

from scipy.interpolate import interp1d, UnivariateSpline
from scipy.optimize import curve_fit

# The following is bad practice, but it works for me for now.
from SuperconductivityCalculations import my_tanh
from MiscFunctions import (nearest_value, save_figure, nextpow2, remove_valley,
                           plot_bound_data)
from MungingFunctions import (normalize_resistances, calculate_flux_quanta,
                              savitzky_golay)
from Helper_Mod import load_sweep_data, save_data
from Locations import DATA_DIR, RESULTS_DIR, PGF_DIR
from Constants import PHI_0
from generate_generic_fit import generate_x_fits

from lot12 import RES_DEVICE as res_device
from lot12 import NORMAL_dRESISTANCE_280 as normal_dresistance_280
from lot12 import dRESISTANCE_OFFSET_280 as dresistance_offset_280

from lot12 import NORMAL_dRESISTANCE_1750 as normal_dresistance_1750
from lot12 import dRESISTANCE_OFFSET_1750 as dresistance_offset_1750

from lot12 import RADIUS as r
from lot12 import FACTOR as factor
from lot12 import TC_JUMP
from lot12 import B_OFFSET
from lot12 import BOUNDARIES as boundaries
from lot12 import FIT_BOUNDARIES as fit_boundaries
from lot12 import SG_WINDOWS as sg_windows

sns.set_context("talk", font_scale=1.25, rc={"lines.linewidth": 1.5})
sns.set_style("whitegrid")

sample_name = 'lot12'
sample_network = ''
sample_run = 'cooldown-2014-12-01'
sample_name = os.path.join(sample_name, sample_network)
sample_directory = os.path.join(DATA_DIR, sample_name, 'cryo_measurements')
sample_results = os.path.join(RESULTS_DIR, sample_name, 'cryo_measurements')
pgf_path = os.path.join(PGF_DIR, sample_name, sample_run)


def main(argv=None):

    print("Nothing to see here. Move along, folks.")

    sys.exit()

if __name__ == "__main__":
    main()
