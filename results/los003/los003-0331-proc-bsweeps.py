#!/usr/bin/env ipython
# coding: utf-8

get_ipython().magic('run -n Header03.py')
df = load_sweep_data('bsweep', sample_directory, sample_run, root='measurement')


# Did one sweep with a direct measurement of $T_c$, i.e. instead of holding temperature stable, held resistance of samle stable. Throw this out for this analysis.

# In[ ]:

df_ohm = {}

orig_keys = sorted(df.keys())

for key in orig_keys:
    if 'Ohm' in key:
        print(key)
        df_ohm[key] = df[key].copy()
        del df[key]


# ## Plot the data to see what it looks like

# In[ ]:

keys = sorted(df.keys())[::-1]

sns.set_palette('coolwarm_r', len(keys))

#keys = ['1315mK up 01']

fig_all, ax_all = plt.subplots()

lens = []

for key in keys:
    temp_df = df[key][res_device][df[key][res_device]['B'] >= -0.0115][::10]
    
    t = temp_df.Time_m
    T = temp_df.TSample_AD
    b = temp_df.B
    dR = temp_df.dR
    I = temp_df.I
    
    half = 0.5*normal_dresistance
    
    #idxes = dR[np.abs(dR - half)]
    
    #print(key, dR[nearest_value(dR, half)[0]], T.mean())
    #if np.abs(I.mean()) < 0.5:
    #    #print(key)
    #    ax_all.plot(b, dR, label=key)
    if T.mean() < 1.456:
        idx, val = nearest_value(dR, 0.5*normal_dresistance)
        idxes = dR[np.abs(dR - half) < 0.15].index
        lens.append(len(idxes))
        #print(key, type(idxes))
        #print(key, val)
        x = T[idxes]
        y = b[idxes] * 1000 * (300**2) / PHI_0
        #print('\t', x, y)
        
        ax_all.plot(x, y, 'd', label=key)
    
    #ax_all.plot(t, T, label=key)
    #ax_all.plot(t, I, label=key)
    
lens = np.array(lens)
print(lens.min())
    
ax_all.xaxis.set_minor_locator(AutoMinorLocator(5))
ax_all.grid(True, 'minor', ls='--')
ax_all.legend(loc='center left', bbox_to_anchor=(1.0, 0.5), ncol=2, prop={'size':12})

ax_all.set_ylabel('$\Phi/\Phi_0$')
#ax_all.set_xlabel('dR [$\Omega$]')
ax_all.set_xlabel('$T_c$ [K]')


ax_all.set_xlim(1.28, 1.46)
ax_all.set_ylim(-0.5, 2.5)


# In[ ]:

#save_figure(fig_all, 'extracted-Tc.png', sample_name, sample_run, dpi=180);


# ### Find and Remove Tails

# In[ ]:

keys = sorted(df.keys())[::-1]

#sns.set_palette('deep')
sns.set_palette('coolwarm_r', len(keys))

fig_tail, ax_tail = plt.subplots()

n = 6

y1 = np.array([])

for key in keys: #keys[n:n+1]:
    #print(key)
    temp_df = df[key][res_device]
    
    if key == '1500mK down 01':
        temp_df = temp_df[temp_df.Time_m >= 2]
    if key == '1495mK down 01':
        temp_df = temp_df[temp_df.Time_m >= 0.75]
    if key == '1485mK down 01':
        temp_df = temp_df[(temp_df.Time_m >= 3) & (temp_df.Time_m <= 51.5)]
    if key == '1480mK down 01':
        temp_df = temp_df[(temp_df.Time_m >= 1) & (temp_df.Time_m <= 43.2)]
    if key == '1470mK down 01':
        temp_df = temp_df[(temp_df.Time_m <= 42)]
    if key == '1360mK down 01':
        temp_df = temp_df[(temp_df.Time_m >= 4)]
    if key == '1335mK down 02':
        temp_df = temp_df[(temp_df.Time_m <= 150)]
    if key == '1315mK up 01':
        temp_df = temp_df[(temp_df.Time_m <= 130)]
    if key == '1000mK up 01':
        temp_df = temp_df[(temp_df.Time_m <= 145)]
    if key == '0750mK down 01':
        temp_df = temp_df[(temp_df.Time_m <= 310)]
        
    t = temp_df.Time_m
    T = temp_df.TSample_AD
    b = temp_df.B
    
    ax_tail.plot(t, T, label=key)
    if y1.any():
        ax_tail_b = ax_tail.twinx()
        ax_tail_b.grid(False)
        ax_tail_b.plot(x, y1, label=key + ' B', color = sns.xkcd_rgb['medium green'])
        
    if temp_df.count()[-1] != df[key][res_device].count()[-1]:
        #print(key, temp_df.count()[-1], df[key][res_device].count()[-1])
        mod_string = 'Removed temperature tails'
        print(key, mod_string)
        df[key][res_device] = temp_df
        df[key]['mods'].append(mod_string)
    
#ax_tail.set_xlim(0, 5)
ax_tail.legend(loc='center left', bbox_to_anchor=(1.0, 0.5), ncol=2, prop={'size':12})


# ## Find and Remove Offsets from Data

# ### Offset Discovery: Nothing is written to the DataFrame

# In[ ]:

#sns.set_palette('coolwarm_r', len(df.keys()))
sns.set_palette('deep')

#fig_first, ax_first = plt.subplots(ncols=2, sharey=True)
ax1 = plt.subplot2grid((2, 2), (0, 0))
ax2 = plt.subplot2grid((2, 2), (0, 1))
ax3 = plt.subplot2grid((2, 2), (1, 0), colspan=2)

ax_first = np.array([ax1, ax2, ax3])

keys = sorted(df.keys())[::-1]
#keys = [x for x in keys if '1315mK down' in x]
keys = [x for x in keys if '2540' in x]

xlim = (-0.025, -0.015, 0.015, 0.025)

all_lims = (-0.01, 0.01)

for key in keys:

    #temp_df = df[key]['IPS']
    
    #idx_max = temp_df.B.argmax()
    if '0400' in key:
        offset = -0.0005
    elif '1000' in key:
        offset = -0.001
    elif '1260' in key:
        offset = -0.0005
    elif '1305' in key:
        offset = -0.0005
    elif '1315mK down' in key:
        offset = -0.00125
    elif '1320' in key:
        offset = -0.0005
    elif '1325' in key:
        offset = -0.00075
    elif '1330' in key:
        offset = -0.00075
    elif '1335' in key:
        offset = -0.00075
    elif '1345' in key:
        offset = -0.00075
    elif '1360' in key:
        offset = -0.00075
    elif '1395' in key:
        offset = -0.00075
    elif '1400' in key:
        offset = -0.00075
    elif '1405' in key:
        offset = -0.00075
    elif '1410' in key:
        offset = -0.00075
    elif '1415' in key:
        offset = -0.00075
    elif '1420' in key:
        offset = -0.00075
    elif '1465' in key:
        offset = -0.0011
    else:
        offset = -0.00075
    #B = temp_df.zMagnet[temp_df.zMagnet < temp_df.zMagnet.max()]
    
    
    print(offset)

    
    temp_df_neg = df[key][res_device][(df[key][res_device]['B'] - offset >= xlim[0]) &
                                      (df[key][res_device]['B'] - offset <= xlim[1])]
    temp_df_pos = df[key][res_device][(df[key][res_device]['B'] - offset >= xlim[2]) &
                                      (df[key][res_device]['B'] - offset <= xlim[3])]
    temp_df_all = df[key][res_device][(df[key][res_device]['B'] - offset >= all_lims[0]) &
                                      (df[key][res_device]['B'] - offset <= all_lims[1])]
    
    #temp_df = temp_df.drop(temp_df.B[temp_df.B == 0].index)
    
    #ax_first.axhline(temp_df.zMagnet[idx_max], color=sns.xkcd_rgb['pale red'], ls='--', lw=1)
    
    ax_first[0].plot(temp_df_neg.B - offset, temp_df_neg.dR, label=key)
    ax_first[1].plot(temp_df_pos.B - offset, temp_df_pos.dR, label=key)
    ax_first[2].plot(temp_df_all.B - offset, temp_df_all.dR, label=key)
    #ax_first[1].plot(temp_df.B, temp_df.I)
    #ax_first.plot(temp_df.Magnetfield)
    #ax_first.plot(np.diff(temp_df.zMagnet))
    
#ax_first.set_ylim(0.114, 0.118)
ax_first[0].set_xlim(xlim[:2])
ax_first[0].xaxis.set_minor_locator(AutoMinorLocator(2))
ax_first[0].grid(True, 'minor', ls='--')
ax_first[0].yaxis.get_major_formatter().set_useOffset(False)
ax_first[0].set_ylabel('dR [$\Omega$]')

ax_first[1].set_xlim(xlim[2:])
ax_first[1].xaxis.set_minor_locator(AutoMinorLocator(2))
ax_first[1].grid(True, 'minor', ls='--')
ax_first[1].yaxis.get_major_formatter().set_useOffset(False)

ax_first[2].set_xlim(all_lims)
#ax_first[2].set_ylim(0, 0.1)
ax_first[2].xaxis.set_minor_locator(AutoMinorLocator(5))
ax_first[2].grid(True, 'minor', ls='--')
    
ax_first[2].legend(loc='best')
ax_first[2].set_xlabel('B [T]')
ax_first[2].set_ylabel('dR [$\Omega$]')


# In[ ]:

ups = []
downs = []

for k, v in B_OFFSETS.items():
    if 'up' in k:
        ups.append(v)
    elif 'down' in k:
        downs.append(v)
    #print(k, v)
    
ups = np.array(ups)
downs = np.array(downs)
print('ups average', ups.mean())
print('downs average', downs.mean())


# ### Review all offsets saved in los003.py

# In[ ]:

keys = sorted(df.keys())[::-1]

keys = [k for k in keys if 'down' in k]

sns.set_palette('coolwarm_r', len(keys))

fig_all, ax_all = plt.subplots()

for key in keys:
    temp_df = df[key][res_device][::10]
    
    offset = 0 #B_OFFSETS[key]
    
    if 'up' in key:
        offset = -8.33E-5
    elif 'down' in key:
        offset = -0.0007
    
    x = temp_df.B - offset
    y = temp_df.dR
    
    ax_all.plot(x, y, label=key)
    
ax_all.xaxis.set_minor_locator(AutoMinorLocator(5))
ax_all.grid(True, 'minor', ls='--')
ax_all.legend(loc='center left', bbox_to_anchor=(1.0, 0.5), ncol=2, prop={'size':12})

ax_all.set_xlabel('B [T]')
ax_all.set_ylabel('dR [$\Omega$]')

ax_all.set_xlim(-0.01, 0.01)
#ax_all.set_ylim(0, 0.1)


# Remove the offsets

# In[ ]:

mod_string = 'Removing the BField offset from channel ADWin/B'

for key in keys:
    if mod_string not in df[key]['mods']:
        print(key, ':', mod_string)
        df[key][res_device]['B'] -= B_OFFSETS[key]
        df[key]['mods'].append(mod_string)


# ### Verify the data was changed correctly in the DataFrame

# In[ ]:

keys = sorted(df.keys())[::-1]

keys = [k for k in keys if 'down' in k]

#sns.set_palette('coolwarm_r', len(df.keys()))

fig_offsets, ax_offsets = plt.subplots()

for key in keys: #sorted(df.keys())[::-1]:
    temp_df = df[key][res_device][::10]
    
    x = temp_df.B
    y = temp_df.dR
    
    ax_offsets.plot(x,y, label=key)
    
ax_offsets.set_xlim(-0.01, 0.01)
#ax_offsets.set_ylim(0, 20)
    
ax_offsets.set_xlabel('B [T]')
ax_offsets.set_ylabel(r'dR [$\Omega$]')
ax_offsets.legend(loc='center right', bbox_to_anchor=(1.6, 0.5), ncol=2, prop={'size': 12})


# ### Add normalized Resistance and $\Delta dR$

# In[ ]:

#df = normalize_resistances(df, res_device, dfactor=normal_dresistance, doffset=dresistance_offset)


# ### Add Total Flux and Number of Flux Quanta

# In[ ]:

print(r, factor)


# In[ ]:

df = calculate_flux_quanta(df, res_device, r, factor, overwrite=False)


# ### Save all the new data

# In[ ]:

for key in sorted(df.keys()):
    if df[key]['modified']:
        print(key)
        for mod in df[key]['mods']:
            print('\t', mod)


# In[ ]:

save_data(df, sample_directory, sample_run, root='proc02', overwrite=True)


# In[ ]:

for key in sorted(df.keys()):
    df[key]['modified'] = False
    df[key]['mods'] = []


# ## Plot the dR vs B for all sweeps

# In[ ]:

keys = sorted([k for k in df if np.abs(df[k][res_device]['I'].mean()) < 0.5])[::-1]

sns.set_palette('coolwarm_r', len(keys))
fig01, ax01 = plt.subplots();

ax01_b = ax01.twiny();

legend_entry = []

for key in keys:
    temp_df = df[key][res_device][::10]
    
    t = temp_df.Time_m   
    n = temp_df['$\Phi / \Phi_0$']
    #dRn = temp_df['$dR/dR_N$']
    dR = temp_df.dR
    I = temp_df.I
    
    ax01.plot(n, dR, label=key)

ax01.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=2, prop={'size':12})

ax01.set_xlim(-2, 2)
ax01.xaxis.set_minor_locator(AutoMinorLocator(4))
ax01.grid(True, 'minor', ls='--')
#ax01.set_ylim(0.0, 5000)

ax01_b.grid(False)
bticks = np.array(ax01.get_xticks()) * PHI_0 / r**2
ax01_b.set_xticks(bticks)
ax01_b.set_xlabel('B [mT]')

ax01.set_ylabel('differential Resistance [$\Omega$]');
ax01.set_xlabel(r'$\Phi / \Phi_0$');

ax01.set_title('Magnet Field Sweeps', y=1.11);
#fig01.tight_layout()


# In[ ]:

fig02, ax02 = plt.subplots();

ax02_b = ax02.twiny();

for key in keys:
    
    temp_df = df[key][res_device][df[key][res_device]['$\Phi / \Phi_0$'] >= -0.2][::10]
    
    t = temp_df.Time_m   
    n = temp_df['$\Phi / \Phi_0$']
    #dRn = temp_df['$dR/dR_N$']
    dR = temp_df.dR
    
    ax02.plot(n, dR, label=key)

ax02.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=2, prop={'size':12})

ax02.set_xlim(-0., 2)
ax02.xaxis.set_minor_locator(AutoMinorLocator(4))
ax02.grid(True, 'minor', ls='--')
#ax02.set_ylim(0.0, 5000)

ax02_b.grid(False)
bticks = np.array(ax02.get_xticks()) * PHI_0 / r**2
ax02_b.set_xticks(bticks)
ax02_b.set_xlabel('B [mT]')

ax02.set_ylabel('differential Resistance [$\Omega$]');
ax02.set_xlabel(r'$\Phi / \Phi_0$');

ax02.set_title('Magnet Field Sweeps', y=1.11);

