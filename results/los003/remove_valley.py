def remove_valley(measurement, key, bounds=(0.0, 0.0)):
    """Split BSweep into positive and negative parts, removing middle.

    This assumes that the key $\Phi / \Phi_0$ is present in the measurement,
    if not it will fall back to BField

    Parameters
    ----------
    old_df : dict
        The data frame to modify
    bounds : tuple with two floats
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.

    Return
    ------
    fig_test, ax_test : matplotlib figure and axis
        The figure and axis showing the data
    """
    # Get the name and units of the x channel to use. Priority is given to 'B'
    if 'B' in measurement:
        b_key = 'B'
        xunit = '[T]'
    elif 'BField' in measurement:
        b_key = 'BField'
        xunit = '[mT]'
    elif 'BzField' in measurement:
        b_key = 'BzField'
        xunit = '[mT]'
    else:
        print('Could not find a valid channel for representing the magnet'
              ' field. Please try something else.')
        return

    # Get the name and units of the  y channel to use. Priority is given to 'dR'
    yunit = r'[$\Omega$]'
    if 'dR' in measurement:
        r_key = 'dR'
    elif 'dRSample' in measurement:
        r_key = 'dRSample'
    elif 'R' in measurement:
        r_key = 'R'
    elif 'RSample' in measurement:
        r_key = 'RSample'
    else:
        print('Could not find a valid channel for representing the resistance.'
              ' Please try something else.')
        return

    # Get the upper boundary for the negative branch
    neg_bndry = bounds[0]
    # Get the lower boundary for the positive branch
    pos_bndry = bounds[1]

    # Set the offset for the positive and negative x-axis plotting limits
    xlim = 0.02 # T
    # Set the lower offset of the y-axis plotting limit
    ylim_low = 1 # Ohm
    # Set the upper offset of the y-axis plotting limit
    ylim_high = 5 # Ohm

    # Grab the negative branch data
    temp_neg = measurement[measurement[b_key] < neg_bndry]
    # Grab the positive branch data
    temp_pos = measurement[measurement[b_key] > pos_bndry]

    # Get the negative and positive y-channels
    y_neg = temp_neg[r_key]
    y_pos = temp_pos[r_key]

    # Set the y-axis plotting limits
    lim_neg = y_neg[np.abs(y_neg - neg_bndry).argmin()]
    lim_pos = y_pos[np.abs(y_pos - pos_bndry).argmin()]
    
    y_neg_min = y_neg.argmin()
    y_pos_min = y_pos.argmin()

    lim = (lim_pos + lim_neg) / 2
    
    if lim - ylim_low > y_pos.min():
        ylim_low = lim - y_pos.min() + 1
    if lim - ylim_low > y_neg.min():
        ylim_low = lim - y_neg.min() + 1

    # Get the x-axis channels
    x_pos = temp_pos[b_key]
    x_neg = temp_neg[b_key]

    fig_test, ax_test = plt.subplots()

    # Plot the data
    # Plot the complete data in grey and dashed in the background for reference
    ax_test.plot(measurement[b_key][::10], measurement[r_key][::10],
                 label='complete', ls='--', color=sns.xkcd_rgb['grey'],
                 lw=0.75)
    # Plot the negative branch
    ax_test.plot(x_neg[::10], y_neg[::10], label='negative')
    # Plot the positive branch
    ax_test.plot(x_pos[::10], y_pos[::10], label='positive')

    # Put red lines where the upper negative and lower positive boundaries are
    ax_test.axvline(neg_bndry, color='red')
    ax_test.axvline(pos_bndry, color='red')

    # Set the plotting axes' limits
    ax_test.set_ylim(lim - ylim_low, lim + ylim_high)
    ax_test.set_xlim(neg_bndry - xlim, pos_bndry + xlim)

    minor_locator = AutoMinorLocator(5)
    ax_test.xaxis.set_minor_locator(minor_locator)
    plt.grid(True, 'both')
    plt.grid(True, 'minor', ls='--', lw=1)

    # Find and show if there is a value even lower than the selected boundary
    if y_pos.min() < y_neg.min():
        ax_test.axvline(x_pos[y_pos_min], color=sns.xkcd_rgb['pale red'], lw=1)
        ax_test.text(x_pos[y_pos_min] + 0.02, y_pos.min() + 0.5, x_pos[y_pos_min])
    elif y_pos.min() > y_neg.min():
        ax_test.text(x_neg[y_neg_min] + 0.02, y_neg.min() + 0.5, x_neg[y_neg_min])
        ax_test.axvline(x_neg[y_neg_min], color=sns.xkcd_rgb['pale red'], lw=1)

    ax_test.set_title('Cut of BSweep: {}'.format(key))
    ax_test.set_xlabel('{k} {u}'.format(k=b_key, u=xunit))
    ax_test.set_ylabel('{k} {u}'.format(k=r_key, u=yunit))

    fig_test.tight_layout()

    return fig_test, ax_test