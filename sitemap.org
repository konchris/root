#+TITLE: Sitemap

   + bin
     + [[file:bin/index.org][MesoSup Binaries and Scripts]]
   + css
     + [[file:css/index.org][MesoSup Cascading Style Sheets]]
     + [[file:css/level-1a.org][index]]
     + [[file:css/level-1.org][index]]
     + [[file:css/level-0a.org][index]]
     + [[file:css/level-0.org][index]]
   + data
     + [[file:data/index.org][MesoSup Massaged Data]]
   + doc
     + [[file:doc/data-operations.org][Data Formats Used in This Project]]
     + [[file:doc/data-formats.org][Data Formats Used in This Project]]
     + [[file:doc/documentation.org][General Documentation]]
     + [[file:doc/project-summary.org][Large oscillations of the magnetoresistance in nanopatterned thin superconducting films]]
     + [[file:doc/index.org][MesoSup Documentation]]
   + etc
     + [[file:etc/index.org][MesoSup Configuration Files]]
   + img
     + [[file:img/index.org][MesoSup Images]]
   + raw-data
     + raw-data
       + [[file:raw-data/raw-data/overview.org][Espy Ph.D. Sample Overview]]
     + [[file:raw-data/overview.org][Espy Ph.D. Sample Overview]]
     + [[file:raw-data/index.org][MesuSup Raw Data]]
   + results
     + [[file:results/2013-06-Frontiers-Poster-Abstract.org][Large oscillations of the magnetoresistance in nanopatterned thin aluminum films]]
     + [[file:results/index.org][MesoSup Results]]
   + src
     + [[file:src/index.org][MesoSup Source Code]]
   + [[file:index.org][Mesoscopic Superconductivity: Unconventional superconducting properties of nano arrays and loops]]
   + [[file:shit.org][Shit]]
