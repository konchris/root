#+SETUPFILE: ../css/level-1a.org
#+TITLE:     General Documentation

* Introduction
This is the documentation on how to do documentation in this project.

[2013-05-11 Sat 14:06]
* File Hierarchy Documentation
The project files are organized into a hierarchical structure, with the root
directory at the top.
The subsequent directories are the following: bin, data, doc, src, results.
*Each directory has a README file in it containing prose descriptions of the
directory's contents, but NOT the contents on any sub directories.*
The README file will, for now, be a UTF-8 Unicode text file.
(What other file types might be better or more useful?
I am partial to Org-Mode files as I use them for other things and they can
easily be used to generate other file types, like UTF/ASCII, LaTeX, html, and
OpenDocument Text (odt).)

[2013-05-11 Sat 14:08]
** bin
The bin directory contains program and script files.

[2013-05-11 Sat 14:10]
* Measurement Metadata

[2013-05-11 Sat 14:07]
