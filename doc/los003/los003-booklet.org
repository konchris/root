#+SETUPFILE: ../css/level-1a.org
#+TITLE: LOS003 Documentation
#+DATE: <2015-11-19 Thu>
#+AUTHOR: Christopher Espy
#+EMAIL: christopher.espy@uni-konstanz.de
#+OPTIONS: ':nil *:t -:t ::t <:nil H:3 \n:nil ^:nil arch:headline author:t
#+OPTIONS: c:nil creator:comment d:(not "LOGBOOK") date:nil e:nil email:nil f:t
#+OPTIONS: inline:t num:t p:nil pri:nil stat:t tags:t tasks:t tex:t timestamp:nil
#+OPTIONS: toc:nil todo:t |:t
#+CREATOR: Emacs 24.3.1 (Org mode 8.2.10)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: en
#+SELECT_TAGS: export
#+LATEX_CLASS: classic-article
#+LATEX_CLASS_OPTIONS: [twoside,numbers=noenddot,headinclude,footinclude=true,cleardoublepage=empty,abstractoff,BCOR=5mm,pagesize,fontsize=10pt,ngerman,american]
#+LATEX_HEADER: \usepackage{colortbl}
#+LATEX_HEADER: \usepackage{pdfpages}
#+LATEX_HEADER: \includepdfset{pages=-}
#+LATEX_HEADER_EXTRA: \geometry{a4paper,showcrop}

\cleardoublepage

\includepdf[pages=-,nup=1x2,landscape,signature=12]{los003.pdf}
