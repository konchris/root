#+SETUPFILE: ../css/level-1a.org
#+TITLE:     Data Formats Used in This Project
#+LATEX_CLASS: classic-article
#+LATEX_CLASS_OPTIONS: [twoside,titlepage,numbers=noenddot,headinclude,footinclude=true,cleardoublepage=empty,abstractoff,BCOR=5mm,pagesize,fontsize=10pt,ngerman,american]
#+LATEX_HEADER: \usepackage{colortbl}
#+LATEX_HEADER: \usepackage{longtable}
#+LATEX_HEADER_EXTRA: \geometry{a4paper,showcrop}


* Introduction
The measurement data for this project has been stored in a few different
various formats.
Some of these different formats contain a complete set of data, which means it
includes calculated data like resistance -- calculated from the directly
measured voltage and current, and some of it does not.
The following is a list of the names of calculated measurement variables:
| Calculated Variable                           | Dependencies               |
|-----------------------------------------------+----------------------------|
| RSample                                       | VSample, ISample           |
| dVSample                                      | dVSamplex, dVSampley       |
| dISample                                      | dISamplex, dISampley       |
| /RTSample (Resistance of sample thermometer)/ | /VRSample*/                |
| TSample                                       | RTSample                   |
| /B[x,z]Field/                                 | /Not sure if this is true/ |
|                                               |                            |

[2013-07-23 Tue 08:15]
** Used Channel Lists: Inputs
:PROPERTIES:
:ID:       fb698ca4-dd78-4f74-8a1c-877855f345d8
:END:
#+ATTR_LATEX: :environment longtable 
| Sample     | Temps      | BSweeps    |
|------------+------------+------------|
| SiO2Al002D | Time_s     | Time_s     |
|            | THe3       | THe3       |
|            | RSample    | BField     |
|            |            | RSample    |
|------------+------------+------------|
| SiO2Al002E | Time_s     | Time_s     |
|            | TSample    | TSample    |
|            | dRSample   | BzField    |
|            | RSample    | dRSample   |
|            |            | RSample    |
|------------+------------+------------|
| Omri01     | Time_m     | Time_m     |
|            | TSample    | TSample    |
|            | dRSample   | xMagnet    |
|            |            | dRSample   |
|------------+------------+------------|
| SiO2Al122  | Time_m     | Time_m     |
|            | TSample_AD | TSample_AD |
|            | dRSample   | zMagnet    |
|            |            | dRSample   |
|------------+------------+------------|
| SiO2Al143  | Time_m     | Time_m     |
|            | TSample_AD | TSample_AD |
|            | dR         | B          |
|            |            | dR         |
|------------+------------+------------|
| LOT12      | Time_m     | Time_m     |
|            | TSample_AD | TSample_AD |
|            | dR         | dR         |
|------------+------------+------------|
| LOS001     | Time_m     | Time_m     |
|            | TSample_AD | TSample_AD |
|            | dR         | B          |
|            |            | dR         |
|------------+------------+------------|
| LOS003     | Time_m     | Time_m     |
|            | TSample_AD | TSample_AD |
|            | dR         | B          |
|            |            | dR         |
|------------+------------+------------|

[2015-12-24 Thu 16:27]

* The Schirm/Messmanager .dat format
Initially we used comma-separated-values saved in ASCII plain-text with a
specialized header to record our measurement data.
An example is shown below:
#+name: .dat Example
#+begin_src sh
# Date and Time: 2012-04-12 10:44:07
#
# 1. Column: Time since beginning of measurement., Unit: s
# 2. Column: Temperature of the sample., Unit: Ohm
# 3. Column: Temperature at the Sorb., Unit: K
# 4. Column: Temperature at the 1K-Plate., Unit: K
# 5. Column: Temperature at the He3-Pot., Unit: K
#
# measTime, RSample, TSorb, T1K, THe3
0.0,8666.1,7.002,249.2,7.002
#+end_src
The first line is the starting date and time of the measurement.
The next comment block contains detailed descriptions of each column of data.
The final header line is a list of the short names for the columns.

The variables that were saved during each measurement varied from measurement
to measurement.
Some of the smaller files had only 5 variables, as shown above, whereas some of
the larger ones had up to 20 variables.
This is something to take into account when writing analysis scripts that
should be reusable.

A list of files that needs to be checked is here: [[file:~/Documents/PhD/root/raw-data/dat_files.txt][dat_files.txt]] in the raw_data
folder.

Use the following workflow:
1. Generate a list of all of the column name lines:
   A) For *.dat files ala C. Schirm I always had the "measTime" variable
      #+name: column_names_schirm
      #+begin_src sh
      grep -hr 'measTime' > column_names_schirm.txt
      #+end_src
   B) For the *.csv files from ADwin I always had the "Milliseconds" variable
      #+name: column_names_adwin
      #+begin_src sh
      grep -hr 'Milliseconds' > column_names_adwin.txt
      #+end_src
2. Sort out the unique lines
   #+name: list_unique_lines
   #+begin_src sh
   sort column_names_adwin.txt | uniq > sorted_adwin.txt
   sort column_names_schirm.txt | uniq > sorted_schirm.txt
   #+end_src
   - measTime, ISample, dISample, RSample, dRSample, TSorb, T1K, THe3, TSample,
     BzField
   - measTime, RSample, TSorb, T1K, THe3
   - measTime, ISample, dISample, VBias, RSample, dRSample, TSorb, T1K, THe3,
     TSample, BzField
   - measTime, RSample, TSorb, T1K, THe3, BField
   - measTime, RTSample
   - measTime, TSorb, RRef, VBias, VSample, dVSample, VRef, dVRef, ISample,
     dISample, RSample, dRSample, T1K, THe3
   - measTime, TSorb, RRef, VBias, VSample, dVSample, VRef, dVRef, ISample,
     dISample, RSample, dRSample, T1K, THe3, RTSample, TSample
   - measTime, TSorb, RRef, VBias, VSample, dVSample, VRef, dVRef, ISample,
     dISample, RSample, dRSample, T1K, THe3, TSample
   - measTime, TSorb, T1K, THe3, BxField, RTSample_direct, TSample_direct, VAmp,
     IAmp, LVSens, LISens
   - measTime, TSorb, T1K, THe3, BzField, RTSample_direct, TSample_direct
   - measTime, TSorb, T1K, THe3, BzField, RTSample_direct, TSample_direct, VAmp,
     IAmp, LVSens, LISens
   - measTime, TSorb, T1K, THe3, RSample, BField
   - measTime, TSorb, T1K, THe3, RTSample_direct, TSample_direct, RSample,
     dRSample, VSample, dVSample, ISample, dISample, VBias, BzField
   - measTime, TSorb, T1K, THe3, RTSample_direct, TSample_direct, VAmp, IAmp,
     LVSens, LISens
   - measTime, TSorb, T1K, THe3, RTSample, TSample, RSample, dRSample, VSample,
     dVSample, ISample, dISample, VBias, BzField
   - measTime, TSorb, T1K, THe3, TSample_direct, BzField, VAmp, IAmp, LVSens,
     LISens
   - measTime, TSorb, T1K, THe3, TSample, RTSample, RSample, dRSample, VSample,
     ISample, VBias, dVSamplex, dVSampley, dVSample, dISamplex, dISampley,
     dISample
   - measTime, TSorb, T1K, THe3, TSample, RTSample, VSample, ISample, VBias,
     dVSamplex, dVSampley, dVSample, dISamplex, dISampley, dISample, BzField,
     RSample, dRSample
   - measTime, TSorb, T1K, THe3, TSample, RTSample, VSample, ISample, VBias,
     dVSamplex, dVSampley, dVSample, dISamplex, dISampley, dISample, BzField,
     RSample, dRSample, VAmp, IAmp, LVSens, LISens
   - measTime, TSorb, T1K, THe3, TSample, RTSample, VSample, VRef, dVSamplex,
     dVSampley, dVSample, dVRefx, dVRefy, dVRef, BxField, BzField, winkel,
     ISample, dISample, RSample, dRSample, RRef
   - measTime, TSorb, T1K, THe3, TSample, RTSample, VSample, VRef, dVSamplex,
     dVSampley, dVSample, dVRefx, dVRefy, dVRef, ISample, dISample, RSample,
     dRSample, RRef
   - measTime, TSorb, T1K, THe3, TSample, VBias, VSample, VRef, dVSamplex,
     dVSampley, dVSample, dVRefx, dVRefy, dVRef, ISample, dISample, RSample,
     dRSample, RRef
   - measTime, TSorb, T1K, THe3, TSample, VBias, VSample, VRef, dVSamplex,
     dVSampley, dVSample, dVRefx, dVRefy, dVRef, ISample, dISample, RSample,
     dRSample, RRef, BzField
   - measTime, U1, R1, ISample, BField
   - measTime, U1, R1, ISample, TSample, TSorb, T1K, THe3, BField
   - measTime, UBias, U1, R1, U6, ISample
   - measTime, UBias, U1, R1, U6, ISample, T1K, TSorb, THe3, BField
   - measTime, UBias, U1, R1, U6, ISample, TSample, BField
   - measTime, UBias, U1, R1, U6, ISample, TSample, T1K, TSorb, THe3, BField
   - measTime, UBias, U1, R1, U6, ISample, TSorb, T1K, THe3, TSample
   - measTime, UBias, U1, U2, R1, R2, U3, ISample, TSorb, T1K, THe3, TSample
   - measTime, UBias, U1, U2, R1, R2, U3, ISample, TSorb, T1K, THe3, TSample,
     BField
   - measTime, UBias, U1, U2, R1, R2, U6, ISample, TSorb, T1K, THe3, TSample,
     BField
   - measTime, UBias, U1, U2, R1, U6, ISample, TSorb, T1K, THe3, TSample
   - measTime, UBias, U1, U2, U3, ISample, T1K, TSorb, THe3, BField
   - measTime, UBias, U1, U2, U3, R1, R2, ISample, TSample, TSorb, T1K, THe3,
     BField
   - measTime, UBias, U1, U3, R1, ISample, TSample, TSorb, T1K, THe3, BField
   - measTime, UBias, USample, DUSample, URef, DURef, RSample, DRSample,
     ISample, DISample, GSample, DGSample, motorpos
   - measTime, V1x, V1y, V1, V_B, V_Curr, R1, ISample, TSample, TSorb, T1K,
     THe3, BField
   - measTime, VSample, ISample, dVSamplex, dVSampley, dISamplex, dISampley
     VBias, RTSample
   
[2013-09-13 Fri 16:41]
* The ADWin .csv format with .dat files
The second type of file used was .csv 
- Milliseconds, VSample, ISample, dVSamplex, dVSampley, dISamplex, dISampley,
  VBias, RTSample, RSample
- Milliseconds, VSample, ISample, dVSamplex, dVSampley, dISamplex, dISampley,
  VBias, RTSample
[2013-09-25 Wed 10:44]
* The National Instruments *.tdms format

[2013-07-23 Tue 12:16]
** First one, PlantUML Test
#+name: testuml
#+begin_src plantuml :file testuml.png
title Test UML
object obj1
object obj2  
#+end_src

[2015-12-24 Thu 13:26]
** First file: omri01
#+name: omri01_uml
#+begin_src plantuml :file omri01_tdms.png :exports results
title Omri01
object tdms_file

tdms_file : name = "File"



#+end_src


[2015-12-24 Thu 13:40]
