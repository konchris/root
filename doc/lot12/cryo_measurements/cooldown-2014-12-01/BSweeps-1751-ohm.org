#+TITLE: LOT12 Cool Down 2014-12-01 \\ BSweeps 280 $\Omega$
#+DATE: <2014-12-12 Fri>
#+AUTHOR: Christopher Espy
#+EMAIL: christopher.espy@uni-konstanz.de
#+OPTIONS: ':nil *:t -:t ::t <:nil H:3 \n:nil ^:nil arch:headline author:t
#+OPTIONS: c:nil creator:comment d:(not "LOGBOOK") date:t e:t email:nil f:t
#+OPTIONS: inline:t num:t p:nil pri:nil stat:t tags:t tasks:t tex:t timestamp:t
#+OPTIONS: toc:t todo:t |:t
#+CREATOR: Emacs 24.3.1 (Org mode 8.2.10)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: en
#+SELECT_TAGS: export
#+LATEX_CLASS: koma-article
#+LATEX_CLASS_OPTIONS:
#+LATEX_HEADER: \usepackage[x11names]{xcolor}
#+LATEX_HEADER: \usepackage{colortbl}
#+LATEX_HEADER: \usepackage{tabularx}
#+LATEX_HEADER: \usepackage{tabu}
#+LATEX_HEADER_EXTRA:

\clearpage

* Introduction
This file contains a list of the raw-data files of complete magnet field sweeps
of sample LOT12.
 
[2014-12-12 Fri 11:01]
* File Naming Convention
Files will be named according to the measurement type, which is the first
"field" after the timestamp.
Fields are separated by underscores "_".

The following measurements are performed
- *Cooldown:* A record from all sensors during cooling down from room
  temperature to at least \SI{20}{\kelvin} or below.
- *1K-Pumping:* Recorded when turning on the pump on the 1k pot until the
  temperature there reaches a steady state.
  /Note: Sometimes this data is recorded in with other measurements and not/
  /saved in a dedicated file./
- *Condensing Helium-3:* Recorded when the helium-3 condensation process takes
  place.
  This starts with the initial heating to \SI{30}{\kelvin}, continues while
  waiting the \SI{30}{\minute} at temperatures above \SI{30}{\kelvin}, and
  finishes with the cool down towards base temperatures.
- *Thermalize:* Often these files contain data when waiting for the temperatures
  across all thermometers to stabilize.
- *PID Testing:* Testing to find the PI parameters for the system.
- *BRamp:* Usually a subclass of a BSweep during which the magnet field is
  changed from \SI{0}{\milli\tesla} to another value, or vice versa.
- *BSweep:* This is term is used to describe measurements during with the magnet
  field is changed from some a positive to a negative value or vice versa.
  A good example are the sweeps from \SI{+150}{\milli\tesla} to
  \SI{-150}{\milli\tesla}.
- *TRamp:* A measurement during which the temperature is changed.
- *TSweep:* A measurement during which the temperature is changed and then
  brought back to the starting value.

#+ATTR_LATEX: :align |X|l|l|l| :environment tabularx :width \textwidth
|--------------------+-------------+-----------+------------|
| *Measurement Type* | *Field 1*   | *Field 2* | *Field 3*  |
|--------------------+-------------+-----------+------------|
| Cooldown           | --          | --        | --         |
| Pump-1K            | --          | --        | --         |
| Condense           | --          | --        | --         |
| Thermalize         | --          | --        | --         |
| PID Testing        | --          | --        | --         |
| BRamp              | Temperature | Ramp End  | Sweep Rate |
| BSweep             | Temperature | Sweep End | Sweep Rate |
| TRamp              | --          | --        | --         |
| TSweep             | --          | --        | --         |
|--------------------+-------------+-----------+------------|

[2014-12-12 Fri 11:04]
* File Renaming
Not all files were named using the same naming convention.

#+ATTR_LATEX: :align |l|l|l|
|----------------------------------------------------+-----------------------------------------------+-------|
| Old File Name                                      | New File Name                                 | Notes |
|----------------------------------------------------+-----------------------------------------------+-------|
| 2014-12-10T13-47-12-BSweep-down-950mK--150mT-2_5mT | 2014-12-10T13-47-12_BSweep_950mK_-150mT_2.5mT |       |
| 2014-12-10T20-35-12-BSweep-up-950mK-150mT-2_5mT    | 2014-12-10T20-35-12_BSweep_950mK_+150mT_2.5mT |       |
| 2014-12-11T10-52-21-BSweep-down-1044mK--150mT-5mT  | 2014-12-11T10-52-21_BSweep_1044mK_-150mT_5mT  |       |
| 2014-12-11T11-53-59-BSweep-Up-1044mK-150mT-5mT     | 2014-12-11T11-53-59_BSweep_1044mK_+150mT_5mT  |       |
| 2014-12-11T12-56-56-BSweep-down-1137mK--150mT-5mT  | 2014-12-11T12-56-56_BSweep_1137mK_-150mT_5mT  |       |
| 2014-12-11T16-27-41-BSweep-down-1137mK--150mT-5mT  | 2014-12-11T16-27-41_BSweep_1137mK_-150mT_5mT  |       |
| 2014-12-11T17-28-24-BSweep-up-1137mK-150mT-5mT     | 2014-12-11T17-28-24_BSweep_1137mK_+150mT_5mT  |       |
| 2014-12-11T21-13-18-BSweep-down-1231mK--150mT-5mT  | 2014-12-11T21-13-18_BSweep_1231mK_-150mT_5mT  |       |
| 2014-12-11T22-14-53-BSweep-up-1231mK-150mT-5mT     | 2014-12-11T22-14-53_BSweep_1231mK_+150mT_5mT  |       |
| 2014-12-12T13-02-50-BSweep_1325mK_-150mT_5mT       | 2014-12-12T13-02-50_BSweep_1325mK_-150mT_5mT  |       |
| 2014-12-12T14-04-19-BSweep_1325mK_+150mT_5mT       | 2014-12-12T14-04-19_BSweep_1325mK_+150mT_5mT  |       |
| 2014-12-12T17-28-41-BSweep_856mK_-150mT_5mT        | 2014-12-12T17-28-41_BSweep_856mK_-150mT_5mT   |       |
| 2014-12-12T18-29-27-BSweep_856mK_+150mT_5mT        | 2014-12-12T18-29-27_BSweep_856mK_+150mT_5mT   |       |
| 2014-12-12T22-52-16-BSweep_763mK_-150mT_5mT        | 2014-12-12T22-52-16_BSweep_763mK_-150mT_5mT   |       |
| 2014-12-12T23-53-56-BSweep_763mK_+150mT_5mT        | 2014-12-12T23-53-56_BSweep_763mK_+150mT_5mT   |       |
| 2014-12-13T01-07-39-BSweep_669mK_-150mT_5mT        | 2014-12-13T01-07-39_BSweep_669mK_-150mT_5mT   |       |
| 2014-12-13T02-08-26-BSweep_669mK_+150mT_5mT        | 2014-12-13T02-08-26_BSweep_669mK_+150mT_5mT   |       |
|----------------------------------------------------+-----------------------------------------------+-------|

[2014-12-12 Fri 11:03]
* Complete Magnet Field Sweeps

#+ATTR_LATEX: :align |X|l|l|l|l|l| :environment tabularx :width \textwidth
|----------------------------+-------------+-------------+-----------+------------+-------|
| File Name                  | Temperature | Sweep Start | Sweep End | Sweep Rate | Notes |
|----------------------------+-------------+-------------+-----------+------------+-------|
| 2014-12-10T20-35-15-BSweep |             |             |           |            |       |
|----------------------------+-------------+-------------+-----------+------------+-------|
[2014-12-12 Fri 11:02]
* Analysis Workflow

[2015-01-21 Wed 09:04]
** Create Single Combined file for all usable BSWeeps with smoothed BField channel
1. Generate list of usable bsweeps
2. Decide what channels from each measurement file should be saved (only from
   ADWin device! other devices will not be saved)
3. Decide on file/location to combine and save selected channels from all usable
   bsweeps
4. For each usable bsweep:
   1. split filename into component parts
   2. add a zero ('0') to the beginning of the temperature string if temperature
      is less than 1000 mK
   3. generate the full path of the file to open
   4. generate unique dictionary/legend key based on temperature and bsweep direction
      which serves as the name of the sweep
   5. open the file and load each device into dictionary of data frames
   6. generate a channel for elapsed measurement time in minutes
   7. *generate a smoothed bfield channel to go along with the ADWin data*
   8. create data frame for saving
   9. write save data frame to file
5. plot everything to make sure it's correct

[2015-01-21 Wed 09:28]

** Create lib/<sample name>.py file
This file contains
1. Both small loop side lengths
2. Both large loop side lengths
3. Line width(s)

[2015-01-21 Wed 09:29]
