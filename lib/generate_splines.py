def generate_splines(measurement, key, bounds=(0.0, 0.0),
                     smooth_s=(65536, 65536)):
    """Generate interoplated negative and positive data for FFT

    Parameters
    ----------
    old_df : dict
        The data frame to modify
    bounds : tuple with two floats, optional
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.
    smooth_s : tuple with two ints, optional
        Smoothing factors for the positive and negative branches for the
        univariate spline fitting.
        DEFAULT: (65536, 65536)

    Return
    ------
    fig_test, ax_test : matplotlib figure and axis
        The figure and axis showing the data
    new_measurement : dict
        The interpolated data, ready for use in the fft
    """

    # print(key)

    if 'RSample' in measurement:
        r_key = 'RSample'
        yunit = '[$\Omega$]'

    if 'BField' in measurement:
        b_key = 'BField'
        xunit = '[T]'
    elif 'B' in measurement:
        b_key = 'B'
        xunit = '[T]'
    elif 'BzField' in measurement:
        b_key = 'BzField'
        xunit = '[T]'
    else:
        print('Could not find a valid channel for representing the magnet'
              ' field. Please try something else.')

    # Define the boundaries
    neg_bndry = bounds[0]
    pos_bndry = bounds[1]

    # Define the smoothing
    neg_smooth = smooth_s[0]
    pos_smooth = smooth_s[1]

    # Get the positive and negative dataframes
    temp_pos = measurement[measurement[b_key] > pos_bndry]
    temp_neg = measurement[measurement[b_key] < neg_bndry]

    # Get the x channels
    x_pos = temp_pos[b_key]
    x_neg = temp_neg[b_key]

    # Find next power of two of each channel's length
    idx_pos = nextpow2(len(x_pos))
    idx_neg = nextpow2(len(x_neg))

    # Linearly inerpolate the x channels (we are assuming here an monotonically
    # increasing function, i.e. strictly linear!)
    new_x_pos = np.linspace(np.min(x_pos), np.max(x_pos), idx_pos)
    new_x_neg = np.linspace(np.min(x_neg), np.max(x_neg), idx_neg)

    # Setup the new data frame
    new_measurement = {}
    new_measurement['mods'] = []
    new_measurement['modified'] = True
    new_measurement['negFFT'] = pd.DataFrame({'IntBField': new_x_neg})
    new_measurement['posFFT'] = pd.DataFrame({'IntBField': new_x_pos})

    # Setup the plot
    fig_splinefit, ax_splinefit = plt.subplots(nrows=3, ncols=2)

    if True:
        # Get the y channels
        y_pos = temp_pos[r_key]
        y_neg = temp_neg[r_key]

        # Linearly interpolate the y channels and generate
        # print('Doing linear interpolation of x')
        f_pos = interp1d(x_pos, y_pos, 'linear')
        f_neg = interp1d(x_neg, y_neg, 'linear')

        new_y_pos = f_pos(new_x_pos)
        new_y_neg = f_neg(new_x_neg)

        # Sometimes a nan sneaks in and the rest of the analysis does not
        # work. Catch that here.
        if np.isnan(new_y_pos).any():
            # print(key, 'Nan in new_y_pos')
            new_y_pos = np.nan_to_num(new_y_pos)
            new_y_pos[new_y_pos == 0] = new_y_pos[new_y_pos != 0].min()

        if np.isnan(new_y_neg).any():
            # print(key, 'Nan in new_y_neg')
            new_y_neg = np.nan_to_num(new_y_neg)
            new_y_neg[new_y_neg == 0] = new_y_neg[new_y_neg != 0].max()

        # print(new_x_pos, new_y_pos)

        # Do the univatiate spline thing
        # print('orders', smooth_s)
        # print('Doing univariate spling thing')
        us_pos = UnivariateSpline(new_x_pos, new_y_pos, s=pos_smooth)
        us_neg = UnivariateSpline(new_x_neg, new_y_neg, s=neg_smooth)

        # Generate the background reduced data by subtracting the univariate
        #  spline from the linearly interpolated data
        # print('Calculating reduced background')
        new_y_pos_reduced = new_y_pos - us_pos(new_x_pos)
        new_y_neg_reduced = new_y_neg - us_neg(new_x_neg)

        int_r_key = 'Int_{}'.format(r_key)
        back_r_key = 'Back_{}'.format(r_key)
        red_r_key = 'Red_{}'.format(r_key)
        # print(int_r_key, back_r_key, red_r_key)

        new_measurement['negFFT'][int_r_key] = new_y_neg
        new_measurement['negFFT'][back_r_key] = new_y_neg - \
            new_y_neg_reduced
        new_measurement['negFFT'][red_r_key] = new_y_neg_reduced
        new_measurement['mods'].append('Adding interpolated and background'
                                       '-reduced negative branch of sweep for'
                                       ' FFT.')

        new_measurement['posFFT'][int_r_key] = new_y_pos
        new_measurement['posFFT'][back_r_key] = new_y_pos - \
            new_y_pos_reduced
        new_measurement['posFFT'][red_r_key] = new_y_pos_reduced
        new_measurement['mods'].append('Adding interpolated and background'
                                       '-reduced positive branch of sweep for'
                                       ' FFT.')

        # print('Plotting')
        # Interpolated Data
        ax_splinefit[0][0].plot(new_x_neg[::10], new_y_neg[::10],
                                label=r_key)
        ax_splinefit[0][1].plot(new_x_pos[::10], new_y_pos[::10],
                                label=r_key)

        # Spline Smoothed Data
        ax_splinefit[1][0].plot(new_x_neg[::10],
                                (new_y_neg - new_y_neg_reduced)[::10],
                                label=r_key)
        ax_splinefit[1][1].plot(new_x_pos[::10],
                                (new_y_pos - new_y_pos_reduced)[::10],
                                label=r_key)

        # Reduced data
        ax_splinefit[2][0].plot(new_x_neg[::10], new_y_neg_reduced[::10],
                                label=r_key)
        ax_splinefit[2][1].plot(new_x_pos[::10], new_y_pos_reduced[::10],
                                label=r_key)
        # print('Finished plotting')
 
    ax_splinefit[0][0].set_title('Negative')
    ax_splinefit[0][1].set_title('Positive')

    ax_splinefit[0][0].set_ylabel('Interpolated Data\n$R_s$ {}'
                                  .format(yunit))
    ax_splinefit[1][0].set_ylabel('Spline Smoothed\n$R_s$ {}'
                                  .format(yunit))
    ax_splinefit[2][0].set_ylabel('Bkgnd Reduced\n$R_s$ {}'
                                  .format(yunit))

    ax_splinefit[1][0].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))
    ax_splinefit[1][1].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))

    fig_splinefit.tight_layout()

    return fig_splinefit, ax_splinefit, new_measurement


