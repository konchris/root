import numpy as np
from Constants import PHI_0, ELECTRON_MASS, ELECTRON_CHARGE


def bulk_critical_field(temp, b_c_0, t_c):
    """Calculate the bulk material critical field for a given temperature.

    Parameters
    ----------
    b_c_0 : float
        The zero temperature critical field in millitesla
    temp : float
        The temperature to calculate the critical field for in millikelvin
    t_c : float
        The transition temperature of the sample in millikelvin

    Returns
    -------
    Bc : float
        The critical field in mT

    """
    return b_c_0 * (1 - (temp / t_c)**2)


def coherence_length(b_c):
    r"""Calculate the coherance length of a sample based critical field

    Parameters
    ----------
    b_c : float
        The zero temperature critical field in mT

    Returns
    -------
    xi_0 : float
        The Ginzberg-Landau coherence length in nm

    Notes
    -----
    This calculates the coherence length based on the upper critical field,
    :math: `B_{c2}`, of a type two superconductor:

    .. math:: \xi_{GL} = \sqrt{ \frac{ \Phi_0}{2 \pi B_{c2} } }

    """

    return np.sqrt(PHI_0 / (2 * np.pi * b_c))


def mean_free_path(v_F, L, n, R_RT, t, W):
    """Calculate the elastic mean free path based on room temperature mean
    free path.

    Parameters
    ----------
    v_F : float
        The Fermi velocity of the material in m/s
    L : float
        The length of the sample being measured in m
    n : float
        The matieral's electron charge density in 1 / m^3
    R_RT : float
        The room temperature resistance of the sample in Ohm
    t : float
        The sample thicknes in m
    W : float
        The sample's cross sectional width

    Returns
    -------
    mean_free_path : float
        The sample's mean free path in m

    """
    l = (ELECTRON_MASS * v_F * L) / (n * ELECTRON_CHARGE**2 * R_RT * t * W)
    return l


def my_tanh(x, a=1, b=1, c=0, d=0):
    """Computer hyperbolic tangent element-wise.

    Equivalent to np.sinh(x)/np.cosh(x) or -1j* np.tan(1j*x).

    Parameters
    ----------
    x : array-like
        Input arra.
    a : scalar, optional
        DEFAULT: 1
    b : scalar, optional
        DEFAULT: 1
    c : scalar, optional
        DEFAULT: 0
    d : scalar, optional
        DEFAULT: 0

    Returns
    -------
    y : numpy.ndarray
        The corresponding hyperbolic tangent values.
    """
    return a * np.tanh(b * x + c) + d


def monotonic_background(Tc0, d, xi_gl, B):
    r"""Calculate the critical temperature for thin films using Tinkham's formula

    Parameters
    ----------
    Tc0 : float
        The zero field critical temperature in Kelvin
    d : float
        The wire width in meters
    xi_gl : float
        The Ginzburg-Landau coherence length in meters
    B : numpy.ndarray
        The magnetfield strengths in Tesla

    Returns
    -------
    Tc : numpy.ndarray
        The field dependent critical temperatures in K

    Notes
    -----
    This is equation 7 from reference [1]_.

    .. math::

        T_c \left(B\right) = T_{c0} \left[1 - \frac{\pi^2}{3} \left(
                \frac{d \xi_{GL} B}{\Phi_0} \right)^2 \right]

    References
    ----------
    .. [1] C. Strunk, et al., PRB 57, 10854 (1998)


    """
    phi_0 = PHI_0 * (1E-9)**2 / 1000
    Tc = Tc0 * (1 - (np.pi**2 / 3) * ((d * xi_gl * B) / phi_0)**2)
    return Tc


def lp_oscillations(Tc0, xi_gl, Rm, B, d, n):
    r"""Calculate the critical temperature oscillations due to the Little-Parks
    effect

    Parameters
    ----------
    Tc0 : float
        The zero field critical temperature in Kelvin
    xi_gl : float
        The Ginzburg-Landau coherence length in meters
    Rm : float
        Average of the inner and outer radii of the loop
    B : numpy.ndarray
        The magnetfield strengths in Tesla
    d : float
        The wire width in meters
    n : float
        The fluxoid occupation number of the loop

    Returns
    -------
    Tc : numpy.ndarray
        The field dependent critical temperatures in K

    Notes
    -----
    This is equation 8 from reference [1]_.

    .. math::

        T_c \left(B\right) = T_{c0} \left[1 - \left(\frac{\xi_{GL}}{R_m
            \right)^2 \left[ \left( \frac{\pi R_m^2 B}{\Phi_0} \right)^2
            \left(1 + z^2\right) - 2n\frac{\pi R_m^2 B}{\Phi_0} +
            \frac{n^2}{2z} \ln \left( \frac{1+z}{1-z} \right) \right] \right]

    References
    ----------
    .. [1] C. Strunk, et al., PRB 57, 10854 (1998)


    """
    z = d / (2 * Rm)
    phi_0 = PHI_0 * (1E-9)**2 / 1000
    Tc_lp = (Tc0 * (1 - (xi_gl / Rm)**2 *
                        ((np.pi * Rm**2 * B / phi_0)**2 *
                         (1 + z**2)
                            - 2*n*(np.pi * Rm**2 * B / phi_0)
                            + (n**2 / (2 * z)) *
                            np.log((1 + z)/(1 - z)))))
    return Tc_lp
