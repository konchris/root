import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from scipy.interpolate import interp1d, UnivariateSpline
from scipy.optimize import curve_fit

from MiscFunctions import nextpow2
from SuperconductivityCalculations import my_tanh
from MungingFunctions import savitzky_golay


def _get_channel_names(chan_list):
    """Discover and revtieve relevant channel names for analysis from measurement

    Parameters
    ----------
    chan_list : list
        List of all of the channels recorded in the measurement

    Returns
    -------
    b_key : str
        The channel name for the magnet field measurement
    r_keys : list
        List of the channel name(s) to analyze
    """
    r_keys = []
    if 'dR' in chan_list:
        r_keys.append('dR')
    elif 'R' in chan_list:
        r_keys.append('R')
    elif 'dRSample' in chan_list and 'RSample' in chan_list:
        r_keys.append('dRSample')
        r_keys.append('RSample')
    elif 'dRSample' not in chan_list and 'RSample' in chan_list:
        r_keys.append('RSample')

    if 'B' in chan_list:
        b_key = 'B'
    elif 'BField' in chan_list:
        b_key = 'BField'
    elif 'BzField' in chan_list:
        b_key = 'BzField'
    elif 'xMagnet' in chan_list:
        b_key = 'xMagnet'
    else:
        print('Could not find a valid channel for representing the magnet'
              ' field. Please try something else.')

    return b_key, r_keys


def _crop_dataframes(old_df, b_chan, bounds, fit_bound):
    """Cut a DataFrame into neg, pos and all parts

    Create three cropped new DataFrames: negative branch, positive
    branch, and 'all', i.e. within the outer fit boundaries, if they are
    defined.

    Paramaters
    ----------
    old_df : pandas.DataFrame
        The DataFrame for the measurement with the un-cropped data
    b_chan : str
        The name of the channel containing the magnet field data
    bounds : tuple with two floats
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.
    fit_bound : scalar-like
        The upper boundary of the absolute value of the data to be fit

    Returns
    -------
    neg_df : pandas.DataFrame
        The negative branch data
    all_df : pandas.DataFrame
        The entire sweep, but cropped to the range within the outer fit
        boundaries
    pos_df : pandas.DataFrame
        The postive branch data

    """
    # Define the boundaries
    neg_bndry = bounds[0]
    pos_bndry = bounds[1]

    fit_bound_neg = fit_bound * -1
    fit_bound_pos = fit_bound

    # Get the positive and negative dataframes
    neg_df = old_df[(old_df[b_chan] >= fit_bound_neg) &
                    (old_df[b_chan] <= neg_bndry)]

    all_df = old_df[(old_df[b_chan] >= fit_bound_neg) &
                    (old_df[b_chan] <= fit_bound_pos)]

    pos_df = old_df[(old_df[b_chan] >= pos_bndry) &
                    (old_df[b_chan] <= fit_bound_pos)]

    return neg_df, all_df, pos_df


def _interpolate_magnetfields(x_neg, x_all, x_pos):
    """Generate the interpolated magnet field strengths for all branches

    Parameters
    ----------
    x_neg : pandas.DataSeries
        The negative branch cropped data
    x_all : pandas.DataSeries
        The fit cropped data
    x_pos : pandas.DataSeries
        The positive branch cropped data

    Return
    ------
    new_x_neg : pandas.DataSeries
        The interpolated negative branch magnetfield
    new_x_all : pandas.DataSeries
        The interpolated fit-cropped magnetfield
    new_x_pos : pandas.DataSeries
        The interpolated positivebranch magnetfield

    """
    # Find next power of two of each channel's length
    idx_pos = nextpow2(len(x_pos))
    idx_neg = nextpow2(len(x_neg))
    idx_all = nextpow2(len(x_all))

    # Linearly inerpolate the x channels (we are assuming here an monotonically
    # increasing function, i.e. strictly linear!)
    new_x_neg = np.linspace(np.min(x_neg), np.max(x_neg), idx_neg)
    new_x_all = np.linspace(np.min(x_all), np.max(x_all), idx_all)
    new_x_pos = np.linspace(np.min(x_pos), np.max(x_pos), idx_pos)

    return new_x_neg, new_x_all, new_x_pos


def _poly_fit_and_reduce_resistance(x_chan, y_chan, deg):
    """Polynomial fit to find background and reduce a resistance

    Parameters
    ----------
    x_chan : pandas.Series
        The magnet field channel data
    y_chan : pandas.Series
        The resistance channel data
    deg : int
        The degree of the polynomial with which to fit the data

    Returns
    -------
    back_y_chan : pandas.Series
        The background of the resistance
    red_y_chan : pandas.Series
        The resistance without the background signal

    """
    poly_params = np.polyfit(x_chan, y_chan, deg)

    poly_func = np.poly1d(poly_params)

    back_y_chan = poly_func(x_chan)

    red_y_chan = y_chan - back_y_chan

    return back_y_chan, red_y_chan


def _tanh_fit_and_reduce_resistance(x_chan, y_chan, init_params):
    """Tanh fit to find background and reduce a resistance

    Parameters
    ----------
    x_chan : pandas.Series
        The magnet field channel data
    y_chan : pandas.Series
        The resistance channel data
    init_params : list
        The four initial parameters for the tanh fitx

    Returns
    -------
    back_y_chan : pandas.Series
        The background of the resistance
    red_y_chan : pandas.Series
        The resistance without the background signal

    """
    tanh_params = curve_fit(my_tanh, x_chan, y_chan, p0=init_params,
                            maxfev=10000)

    back_y_chan = my_tanh(x_chan, *tanh_params[0])

    red_y_chan = y_chan - back_y_chan

    return back_y_chan, red_y_chan


def _spline_fit_and_reduce_resistance(x_chan, y_chan, smooth):
    """UnivariateSpline fit and smooth to find background and reduce a
    resistance

    Parameters
    ----------
    x_chan : pandas.Series
        The magnet field channel data
    y_chan : pandas.Series
        The resistance channel data
    smooth : scalar-like
        The smoothing factor

    Returns
    -------
    back_y_chan : pandas.Series
        The background of the resistance
    red_y_chan : pandas.Series
        The resistance without the background signal

    """
    back_y_chan = UnivariateSpline(x_chan, y_chan, s=smooth)(x_chan)

    red_y_chan = y_chan - back_y_chan

    return back_y_chan, red_y_chan


def _sg_fit_and_reduce_resistance(x_chan, y_chan, window):
    """Savitzky-Golay smooth to find background and reduce a resistance

    Parameters
    ----------
    x_chan : pandas.Series
        The magnet field channel data
    y_chan : pandas.Series
        The resistance channel data
    windows : tuple of three scalar-like
        The windowing factor to use for Savitzky-Golay smoothing

    Returns
    -------
    back_y_chan : pandas.Series
        The background of the resistance
    red_y_chan : pandas.Series
        The resistance without the background signal

    """
    back_y_chan = savitzky_golay(y_chan, window, 3)

    red_y_chan = y_chan - back_y_chan

    return back_y_chan, red_y_chan


def generate_x_fits(measurement, key, bounds=(0.0, 0.0),
                    fit_bound=1,
                    deg=None,
                    init_params=None,
                    s=None,
                    sg_win=None
                    ):
    """Generate x fits of the negative, positive and combined data

    Parameters
    ----------
    measurement : dict
        The data frame to modify
    key : str
        The name of the measurement
    bounds : tuple with two floats, optional
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.
    fit_bound : scalar-like, optional
        The upper boundary of the absolute value of the data to be fit
    deg : int, optional
        If this is defined, then a polynomial fit is attempted of the degree
        given by deg
    init_params : array-like, optional
        If this is defined, then a tanh fit is attempted, using the initial
        fit parameters given
    s : scalar-like, optional
        If defined the univariate spline fit with smoothing is attempted, using
        the smoothing parameter given
    sg_win : optional
        Savitzky-Golay smoothing

    Return
    ------
    fig_test, ax_test : matplotlib figure and axis
        The figure and axis showing the data
    new_measurement : dict
        The interpolated data, ready for use in the fft
    """
    # 0: Get relevant resistance, discover bfield
    b_key, r_keys = _get_channel_names(sorted(measurement.keys()))

    # 1: Generate cropped data sets
    neg_df, all_df, pos_df = _crop_dataframes(measurement, b_key, bounds,
                                              fit_bound)

    # 1.5: Get the x channels
    # Get the x channels
    x_neg = neg_df[b_key]
    x_all = all_df[b_key]
    x_pos = pos_df[b_key]

    # 2: Interpolate magnetfield
    new_x_neg, new_x_all, new_x_pos = _interpolate_magnetfields(x_neg, x_all,
                                                                x_pos)

    # 3: Setup FFT DataFrames
    # Setup the new data frame
    new_measurement = {}
    new_measurement['mods'] = []
    new_measurement['modified'] = True
    new_measurement['negFFT'] = pd.DataFrame({'Int_B': new_x_neg})
    new_measurement['allFFT'] = pd.DataFrame({'Int_B': new_x_all})
    new_measurement['posFFT'] = pd.DataFrame({'Int_B': new_x_pos})

    # Setup the plot
    fig_x_fit, ax_x_fit = plt.subplots(nrows=2, ncols=3, sharex='col',
                                       sharey='row')

    for (i, r_key) in enumerate(r_keys):

        if 'dR' in r_key:
            r_name = 'dR'
        elif 'R' in r_key:
            r_name = 'R'

        # 3.5 Get the y channels
        y_neg = neg_df[r_key]
        y_all = all_df[r_key]
        y_pos = pos_df[r_key]

        # 4. Generate Interpolated dR/R
        f_neg = interp1d(x_neg, y_neg, 'linear')
        f_all = interp1d(x_all, y_all, 'linear')
        f_pos = interp1d(x_pos, y_pos, 'linear')

        new_y_neg = f_neg(new_x_neg)
        new_y_all = f_all(new_x_all)
        new_y_pos = f_pos(new_x_pos)

        # Sometimes a nan sneaks in and the rest of the analysis does not
        # work. Catch that here.
        if np.isnan(new_y_neg).any():
            # print(key, 'Nan in new_y_neg')
            new_y_neg = np.nan_to_num(new_y_neg)
            new_y_neg[new_y_neg == 0] = new_y_neg[new_y_neg != 0].max()

        if np.isnan(new_y_all).any():
            # print(key, 'Nan in new_y_all')
            new_y_all = np.nan_to_num(new_y_all)
            new_y_all[new_y_all == 0] = new_y_all[new_y_all != 0].max()

        if np.isnan(new_y_pos).any():
            # print(key, 'Nan in new_y_pos')
            new_y_pos = np.nan_to_num(new_y_pos)
            new_y_pos[new_y_pos == 0] = new_y_pos[new_y_pos != 0].min()

        int_r_key = 'Int_{}'.format(r_name)
        back_r_key = 'Back_{}'.format(r_name)
        red_r_key = 'Red_{}'.format(r_name)

        new_measurement['negFFT'][int_r_key] = new_y_neg
        new_measurement['allFFT'][int_r_key] = new_y_all
        new_measurement['posFFT'][int_r_key] = new_y_pos

        # 5. Do Fit here
        # '{k}; {d:d}-deg\nPositive'.format(k=key, d=deg))
        if deg:
            fit = 'poly'
            fit_fun = _poly_fit_and_reduce_resistance
            v = deg
            v_neg = v
            v_all = v
            v_pos = v
        elif init_params:
            fit = 'tanh'
            fit_fun = _tanh_fit_and_reduce_resistance
            v = init_params
            v_neg = v
            v_all = v
            v_pos = v
        elif s:
            fit = 'US smoothing'
            fit_fun = _spline_fit_and_reduce_resistance
            v = s
            v_neg = v[0]
            v_all = v[2]
            v_pos = v[1]
        elif sg_win:
            fit = 'SG smoothing'
            fit_fun = _sg_fit_and_reduce_resistance
            v = sg_win
            v_neg = v[0]
            v_all = v[2]
            v_pos = v[1]

        plot_title = '{k} {f} fit'.format(k=key, f=fit)

        back_new_y_neg, reduced_new_y_neg = fit_fun(new_x_neg, new_y_neg,
                                                    v_neg)
        back_new_y_all, reduced_new_y_all = fit_fun(new_x_all, new_y_all,
                                                    v_all)
        back_new_y_pos, reduced_new_y_pos = fit_fun(new_x_pos, new_y_pos,
                                                    v_pos)

        back_y_neg, reduced_y_neg = fit_fun(x_neg, y_neg, v_neg)
        back_y_all, reduced_y_all = fit_fun(x_all, y_all, v_all)
        back_y_pos, reduced_y_pos = fit_fun(x_pos, y_pos, v_pos)

        # 6. Add results to DataFrame
        # Background
        new_measurement['negFFT'][back_r_key] = back_new_y_neg
        new_measurement['allFFT'][back_r_key] = back_new_y_all
        new_measurement['posFFT'][back_r_key] = back_new_y_pos

        # Reduced Resistance
        new_measurement['negFFT'][red_r_key] = reduced_new_y_neg
        new_measurement['allFFT'][red_r_key] = reduced_new_y_all
        new_measurement['posFFT'][red_r_key] = reduced_new_y_pos

        # Description
        new_measurement['mods'].append('Adding interpolated and background'
                                       '-reduced negative branch of sweep for'
                                       ' FFT.')
        new_measurement['mods'].append('Adding interpolated and background'
                                       '-reduced sweep for FFT.')
        new_measurement['mods'].append('Adding interpolated and background'
                                       '-reduced positive branch of sweep for'
                                       ' FFT.')

        # 7. Plot
        # Interpolated Data
        ax_x_fit[0][0].plot(new_x_neg[::10], new_y_neg[::10],
                            label=r_key + ' int.')
        ax_x_fit[0][1].plot(new_x_pos[::10], new_y_pos[::10],
                            label=r_key + ' int.')

        ax_x_fit[0][2].plot(new_x_all[::10], new_y_all[::10],
                            label=r_key + ' int.')

        ax_x_fit[0][0].plot(x_neg[::10], y_neg[::10],
                            label=r_key + ' raw')
        ax_x_fit[0][1].plot(x_pos[::10], y_pos[::10],
                            label=r_key + ' raw')

        ax_x_fit[0][2].plot(x_all[::10], y_all[::10],
                            label=r_key + ' raw')

        #  Fit Data
        ax_x_fit[0][0].plot(new_x_neg[::10], back_new_y_neg[::10],
                            label=r_key + ' bckgrnd int.')
        ax_x_fit[0][1].plot(new_x_pos[::10], back_new_y_pos[::10],
                            label=r_key + ' bckgrnd int.')
        ax_x_fit[0][2].plot(new_x_all[::10], back_new_y_all[::10],
                            label=r_key + ' bckgrnd int')

        ax_x_fit[0][0].plot(x_neg[::10], back_y_neg[::10],
                            label=r_key + ' bckgrnd raw')
        ax_x_fit[0][1].plot(x_pos[::10], back_y_pos[::10],
                            label=r_key + ' bckgrnd raw')
        ax_x_fit[0][2].plot(x_all[::10], back_y_all[::10],
                            label=r_key + ' bckgrnd raw')

        # Reduced data
        ax_x_fit[1][0].plot(new_x_neg[::10], reduced_new_y_neg[::10],
                            label=r_key + ' red int.')
        ax_x_fit[1][1].plot(new_x_pos[::10], reduced_new_y_pos[::10],
                            label=r_key + ' red int.')
        ax_x_fit[1][2].plot(new_x_all[::10], reduced_new_y_all[::10],
                            label=r_key + ' red int.')

        ax_x_fit[1][0].plot(x_neg[::10], reduced_y_neg[::10],
                            label=r_key + ' red raw')
        ax_x_fit[1][1].plot(x_pos[::10], reduced_y_pos[::10],
                            label=r_key + ' red raw')
        ax_x_fit[1][2].plot(x_all[::10], reduced_y_all[::10],
                            label=r_key + ' red raw')

    ax_x_fit[0][0].set_title('Negative')
    ax_x_fit[0][1].set_title(plot_title)
    ax_x_fit[0][2].set_title('Whole')

    ax_x_fit[0][2].legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
    ax_x_fit[1][2].legend(loc='center left', bbox_to_anchor=(1.0, 0.5))

    ax_x_fit[0][0].set_ylabel(r'$R_s$ [$\Omega$]')
    ax_x_fit[1][0].set_ylabel('Bkgnd Reduced\n$R_s$ [$\Omega$]')

    ax_x_fit[1][0].set_xlabel(r'Int_B [T]')
    ax_x_fit[1][1].set_xlabel(r'Int_B [T]')
    ax_x_fit[1][2].set_xlabel(r'Int_B [T]')

    ax_x_fit[1][0].set_xlim(new_x_neg.min(), new_x_neg.max())
    ax_x_fit[1][1].set_xlim(new_x_pos.min(), new_x_pos.max())
    ax_x_fit[1][2].set_xlim(new_x_all.min(), new_x_all.max())

    fig_x_fit.subplots_adjust(wspace=0.05, hspace=0.05)

    return fig_x_fit, ax_x_fit, new_measurement
