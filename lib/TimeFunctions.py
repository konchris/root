import numpy as np
import pandas as pd


def calculate_minutes(input_index, start_time=None):
    """Calculate the elapsed time in minutes.

    Using a dataframe's datetime stamp index calculate the elapsed time in
    minutes of the data in minutes, i.e. elapsed time since the first timestamp
    in the index. This returns a timeseries object that should be added to the
    pandas dataframe as an additional column. This function also has the
    possibility of calculating the elapsed time starting from a different
    starting time other than the first timestamp in the index.

    Parameters
    ----------
    input_index : pandas.tseries.index.DatetimeIndex
        The input datetime index from which the elapsed time in minutes shall
        be calculated.
    start_time : pandas.tslib.Timestamp
        An alternative starting time for calculating elapsed time.


    Returns
    -------
    pandas.core.series.Series
        The elapsed time in minutes as a pandas time series object.

    See
    ---
    pandas.DatetimeIndex

    """
    if not start_time:
        start_time = input_index.min()

    time_m = pd.Series((input_index.values - start_time.value
                        ).astype(np.int64)/60E9,
                       index=input_index)
    return time_m


def calculate_hours(input_index, start_time=None):
    """Calculate the elapsed time in hours.

    Using a dataframe's datetime stamp index calculate the elapsed time
    in hours of the data, i.e. elapsed time since the first timestamp in
    the index. This returns a timeseries object that should be added to
    the pandas dataframe as an additional column. This function also has
    the possibility of calculating the elapsed time starting from a
    different starting time other than the first timestamp in the index.

    Parameters
    ----------
    input_index : pandas.tseries.index.DatetimeIndex
    The input datetime index from which the elapsed time in hours shall be
    calculated.
    start_time : pandas.tslib.Timestamp
    An alternative starting time for calculating elapsed time.


    Returns
    -------
    pandas.core.series.Series
    The elapsed time in hours as a pandas time series object.

    See
    ---
    pandas.DatetimeIndex
"""
    if not start_time:
        start_time = input_index.min()

    # print('Start time is {}'.format(start_time))

    time_h = pd.Series((input_index.values - start_time.value
                        ).astype(np.int64)/3600E9,
                       index=input_index)
    return time_h


def recalculate_index(df, start, end, count):
    """Recalculate the timestamps based on the file's start and end times.

    Parameters
    ----------
    df : pandas.DataFrame
        The dataframe whose index should be adjusted

    start : datetime.datetime
        The starting datetimestamp of the file.

    end : datetime.datetime
        The ending datetimestamp of the file.

    count : int
        The number of datapoints in the channel.

    Returns
    -------
    pandas.DataFrame
        The dataframe with the new index

    """

    meas_dur = end - start

    frequency = '{:.0f}L'.format(meas_dur.total_seconds() / count * 1000)

    # print(frequency)

    new_index = pd.date_range(start=start, periods=count, freq=frequency)

    df['new_index'] = new_index

    return df.set_index('new_index')
