def generate_tanh_fits(measurement, key, bounds=(0.0, 0.0),
                       init_params = ([1, 1, 1, 1], [1, 1, 1, 1])):
    """Generate tanh fits of the negative and positive data

    Parameters
    ----------
    measurement : dict
        The data frame to modify
    key : str
        The name of the measurement
    bounds : tuple with two floats, optional
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.
    init_params : tuple of two four-element lists
        The initial parameters for performing the tanh fit.
        DEFAULT: ([1, 1, 1, 1], [1, 1, 1, 1])

    Return
    ------
    fig_test, ax_test : matplotlib figure and axis
        The figure and axis showing the data
    new_measurement : dict
        The interpolated data, ready for use in the fft
    """

    reses = []
    for chan in measurement:
        if 'RSample' in chan:
            reses.append(chan)
        elif chan == 'R':
            reses.append(chan)
        elif chan == 'dR':
            reses.append(chan)
        # elif chan == '$R/R_N$':
        #     reses.append(chan)
        # elif chan == '$dR/dR_N$':
        #     reses.append(chan)

    if '$\Phi / \Phi_0$' in measurement:
        b_key = '$\Phi / \Phi_0$'
        xunit = ''
    elif 'BField' in measurement:
        b_key = 'BField'
        xunit = '[mT]'
    elif 'BzField' in measurement:
        b_key = 'BzField'
        xunit = '[mT]'
    else:
        print('Could not find a valid channel for representing the magnet'
              ' field. Please try something else.')

    yunit = r'[$\Omega$]'

    # Define the boundaries
    neg_bndry = bounds[0]
    pos_bndry = bounds[1]

    # Get the positive and negative dataframes
    temp_pos = measurement[measurement[b_key] > pos_bndry]
    temp_neg = measurement[measurement[b_key] < neg_bndry]

    # Get the x channels
    x_pos = temp_pos[b_key]
    x_neg = temp_neg[b_key]

    # Find next power of two of each channel's length
    idx_pos = nextpow2(len(x_pos))
    idx_neg = nextpow2(len(x_neg))

    # Generate a new index array for each channel
    # k_pos = np.arange(0, idx_pos)
    # k_neg = np.arange(0, idx_neg)

    # Linearly inerpolate the x channels (we are assuming here an monotonically
    # increasing function, i.e. strictly linear!)
    new_x_pos = np.linspace(np.min(x_pos), np.max(x_pos), idx_pos)
    new_x_neg = np.linspace(np.min(x_neg), np.max(x_neg), idx_neg)

    # Setup the new data frame
    new_measurement = {}
    new_measurement['mods'] = []
    new_measurement['modified'] = True
    new_measurement['negFFT'] = pd.DataFrame({'IntPhi0': new_x_neg})
    new_measurement['posFFT'] = pd.DataFrame({'IntPhi0': new_x_pos})

    # Setup the plot
    fig_tanhfit, ax_tanhfit = plt.subplots(nrows=3, ncols=2)

    for (i, r_key) in enumerate(reses):
        # Get the y channels
        y_pos = temp_pos[r_key]
        y_neg = temp_neg[r_key]

        # Linearly interpolate the y channels and generate
        # print('Doing linear interpolation of x')
        f_pos = interp1d(x_pos, y_pos, 'linear')
        f_neg = interp1d(x_neg, y_neg, 'linear')

        new_y_pos = f_pos(new_x_pos)
        new_y_neg = f_neg(new_x_neg)

        # Sometimes a nan sneaks in and the rest of the analysis does not
        # work. Catch that here.
        if np.isnan(new_y_pos).any():
            print(key, 'Nan in new_y_pos')
            new_y_pos = np.nan_to_num(new_y_pos)
            new_y_pos[new_y_pos == 0] = new_y_pos[new_y_pos != 0].min()

        if np.isnan(new_y_neg).any():
            print(key, 'Nan in new_y_neg')
            new_y_neg = np.nan_to_num(new_y_neg)
            new_y_neg[new_y_neg == 0] = new_y_neg[new_y_neg != 0].min()

        # Do the tanh fit
        # print('Doing tanh fit')
        params_pos = curve_fit(my_tanh, new_x_pos, new_y_pos, p0=init_params,
                               maxfev=1000)
        params_neg = curve_fit(my_tanh, new_x_neg, new_y_neg, p0=init_params,
                               maxfev=1000)

        print('Pos:', params_pos[0])
        print('Neg:', params_neg[0])

        y_tanh_pos = my_tanh(new_x_pos, *params_pos[0])
        y_tanh_neg = my_tanh(new_x_neg, *params_neg[0])

        # Generate the background reduced data by subtracting the univariate
        #  spline from the linearly interpolated data
        # print('Calculating reduced background')
        new_y_pos_reduced = new_y_pos - y_tanh_pos
        new_y_neg_reduced = new_y_neg - y_tanh_neg

        int_r_key = 'Int{}'.format(r_key)
        back_r_key = 'Back{}'.format(r_key)
        red_r_key = 'Red{}'.format(r_key)
        # print(int_r_key, back_r_key, red_r_key)

        new_measurement['negFFT'][int_r_key] = new_y_neg
        new_measurement['negFFT'][back_r_key] = new_y_neg - \
            new_y_neg_reduced
        new_measurement['negFFT'][red_r_key] = new_y_neg_reduced
        new_measurement['mods'].append('Adding interpolated and background'
                                       '-reduced negative branch of sweep for'
                                       ' FFT.')

        new_measurement['posFFT'][int_r_key] = new_y_pos
        new_measurement['posFFT'][back_r_key] = new_y_pos - \
            new_y_pos_reduced
        new_measurement['posFFT'][red_r_key] = new_y_pos_reduced
        new_measurement['mods'].append('Adding interpolated and background'
                                       '-reduced positive branch of sweep for'
                                       ' FFT.')

        print('Plotting')
        # Interpolated Data
        ax_tanhfit[0][0].plot(new_x_neg[::10], new_y_neg[::10],
                              label=r_key)
        ax_tanhfit[0][1].plot(new_x_pos[::10], new_y_pos[::10],
                              label=r_key)

        # Spline Smoothed Data
        ax_tanhfit[1][0].plot(new_x_neg[::10],
                              (new_y_neg - new_y_neg_reduced)[::10],
                              label=r_key)
        ax_tanhfit[1][1].plot(new_x_pos[::10],
                              (new_y_pos - new_y_pos_reduced)[::10],
                              label=r_key)

        # Reduced data
        ax_tanhfit[2][0].plot(new_x_neg[::10], new_y_neg_reduced[::10],
                              label=r_key)
        ax_tanhfit[2][1].plot(new_x_pos[::10], new_y_pos_reduced[::10],
                              label=r_key)
        print('Finished plotting')

    ax_tanhfit[0][0].set_title('Negative')
    ax_tanhfit[0][1].set_title('Positive')

    ax_tanhfit[0][0].set_ylabel('Interpolated Data\n$R_s$ {}'
                                .format(yunit))
    ax_tanhfit[1][0].set_ylabel('Spline Smoothed\n$R_s$ {}'
                                .format(yunit))
    ax_tanhfit[2][0].set_ylabel('Bkgnd Reduced\n$R_s$ {}'
                                .format(yunit))

    ax_tanhfit[1][0].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))
    ax_tanhfit[1][1].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))

    fig_tanhfit.tight_layout()

    return fig_tanhfit, ax_tanhfit, new_measurement


