import numpy as np

def concatenate(data):
    segments={}
    for i in data.keys():
        segments[i]={'values': [], 'unit':[]}
        #segments[i]={}
        #segments[i]['values']=[]
        #segments[i]['unit']=[]
    segments['label']=[]
    segments['filename']=[]   
    
    for key in data.iterkeys():           
        try:            
            segments[key]['values'].append(np.hstack([lst for lst in data[key]['values']]))        
            #segments[key]['unit'].append(data[key]['unit'][0])
        except:            
            print('concatenate failed @ key:   %s'%(key))
            #pass
    #segments['label'].append(data['label'][0].split()[:-2])
    segments['label'].append(data['label'][0])
    segments['filename'].append(data['filename'][0])
    return segments

def segment_custom_slice(data, indexlist, segment_key='BField', labels=['up', 'down']):
    """slice a dataset at the indices given in indexlist (e.g. [0, 100, 300]),
       the function returns a dictionary dataset with columns of the sliced data
       
       in the above axample the function returns two slices [0:100] and [100:300]"""
    segments={}
    #segments=dict.fromkeys(data, {'values':[], 'unit':[]})
    for i in data.keys():
        segments[i]={}
        segments[i]['values']=[]
        segments[i]['unit']=[]
    segments['label']=[]
    segments['filename']=[]

    try:
        filterindex=list(indexlist)        
    except:
        print('invalid index in list')
        return False

    for i in range(len(filterindex)-1):
        #step=1        
        #while ((indices[i+step]-indices[i])<=window and (indices+step+1 >= len(indices))):
        #    step+=1
        if 'label' in data.keys():
            try:
                segments['label'].append(str(data['label'][0])+' '+str(segment_key)+' '+str(labels[len(segments['label'])%2]))
                #segments['label'].append(labels[len(segments['label'])%2])
            except:
                pass
        if 'filename' in data.keys():
            try:
                segments['filename'].append(str(data['filename'][0]))
            except:
                pass
        for key in data.iterkeys():
            try:
                segments[key]['values'].append(data[key]['values'][0][filterindex[i]:filterindex[i+1]])
                segments[key]['unit'].append(data[key]['unit'][0])
            except:
                pass
    print('data segmentation....ok: %s segments'%str(len(segments[segment_key]['values'])))
    return segments

def segment_sweep(data, segment_key, window, labels=['up', 'down'], crop_points=1):
    segments={}
    #segments=dict.fromkeys(data, {'values':[], 'unit':[]})
    for i in data.keys():
        segments[i]={}
        segments[i]['values']=[]
        segments[i]['unit']=[]
    segments['label']=[]
    segments['filename']=[]
    
    datavec=data[segment_key]['values'][0]
    local_MinMax(datavec, window)
    min_indices, max_indices = local_MinMax(datavec, window)
    #print(min_indices, max_indices)
    #indices=np.sort(min_indices[0].tolist()+max_indices[0].tolist())
    indices=np.sort(np.hstack((min_indices[0], max_indices[0])))
    #indexfit=np.sort(min_fits+max_fits)
    filterindex=[0]
    filterindex.extend(indices[np.where(np.diff(indices)>= window)])
    filterindex.extend([-1])
    
    for i in range(len(filterindex)-1):
        #step=1        
        #while ((indices[i+step]-indices[i])<=window and (indices+step+1 >= len(indices))):
        #    step+=1
        if 1:
            if 'label' in data.keys():
                try:
                    segments['label'].append(str(data['label'][0])+' '+str(segment_key)+' '+str(labels[len(segments['label'])%2]))
                    #segments['label'].append(labels[len(segments['label'])%2])
                except:
                    pass
            if 'filename' in data.keys():
                try:
                    segments['filename'].append(str(data['filename'][0]))
                except:
                    pass
            for key in data.iterkeys():
                try:
                    segments[key]['values'].append(data[key]['values'][0][filterindex[i]:filterindex[i+1]])
                    segments[key]['unit'].append(data[key]['unit'][0])
                except:
                    pass
        else:
            pass
    for key in data.iterkeys():
        try:
            segments[key]['values'][-1]=np.append(segments[key]['values'][-1], data[key]['values'][0][-1])            
        except:
            pass
    print('data segmentation....ok: %s segments'%str(len(segments[segment_key]['values'])))
    return segments


def segment_histo(histdata, window=1.0, minG=None, maxG=None, refG=0.1, calib=False):
    """helptext

    % window    depth of adjecent extrema
                window should be approximately maxG - minG
    """
    #prepare new dictionary
    #segments= dict.fromkeys(data, {'values':[], 'unit':[]})
    data=histdata.copy()
    segments={}
    for i in data.keys():
        segments[i]={}
        segments[i]['values']=[]
        segments[i]['unit']=[]
    segments['label']=['start']
    segments['filename']=[]
    segments['ttd_calib']=[]
    segments['rel_time']={'values':[], 'unit':[]}
    segments['rel_pos']={'values':[], 'unit':[]}
    labels=['close', 'open']

    #reduce data to valid range between minG and maxG
    if minG==None: minG= -np.Inf
    if maxG==None: maxG= np.Inf
    G= np.ma.masked_outside(data['G']['values'][0], minG, maxG, copy=True)
    mask=G.mask.copy()
    for key in data.iterkeys():
        if type(data[key])== type({}) and data[key].has_key('values'):
            data[key]['values'][0]= np.ma.masked_where(mask, data[key]['values'][0], copy=True).compressed()    
    G=G.compressed()        
    
    #get indices and values of both the local minima and maxima
    maxtab, mintab = peakdet(G, window)
    indexlist= [0]+sorted(mintab.T[0].tolist()+maxtab.T[0].tolist())+[len(G)] #[-1]
    print(len(indexlist))
    #loop over all steps in indexlist and create new data array
    for i in range(len(indexlist)-1):
        if not i==0:
            segments['label'].append(labels[i%2]+' '+str(int(i/2)))               
        if 'filename' in data.keys():
            try:
                segments['filename'].append(str(data['filename'][0]))
            except:
                pass
        for key in data.iterkeys():
            try:
                segments[key]['unit'].append(data[key]['unit'][0])
                vals=data[key]['values'][0]
                segments[key]['values'].append(vals[indexlist[i]:indexlist[i+1]])                
            except:
                pass
        #find reference position and create relative time, motorpos etc.
        Gseg=G[indexlist[i]:indexlist[i+1]]
        Gseg[np.where(np.isnan(Gseg))]=0.0
        refind = np.argmin(np.abs(Gseg-refG))
        relpos = np.arange(-refind, len(Gseg)-refind, 1.0)*(-1)**(1+i%2)
        segments['rel_pos']['values'].append(relpos)
        segments['rel_pos']['unit'].append(r'relative position, $a.U.$')
        
    print('data segmentation....ok: %s segments'%str(len(segments['G']['values'])))
    return segments

def local_MinMax(data, window):
    """Find the local minima within fits, and return them and their indices.

    Returns a list of indices at which the minima were found, and a list of the
    minima, sorted in order of increasing minimum.  The keyword argument window
    determines how close two local minima are allowed to be to one another.  If
    two local minima are found closer together than that, then the lowest of
    them is taken as the real minimum.  window=1 will return all local minima"""

    from scipy.ndimage.filters import minimum_filter as min_filter
    from scipy.ndimage.filters import maximum_filter as max_filter

    fits=data
    minfits = min_filter(fits, size=window, mode='wrap')
    maxfits = max_filter(fits, size=window, mode='wrap')
    
    min_indices=np.nonzero(fits==minfits)
    max_indices=np.nonzero(fits==maxfits)
    min_bool=fits==minfits
    max_bool=fits==maxfits
    
    min_indices=[i[min_bool] for i in np.indices(fits.shape)]
    max_indices=[i[max_bool] for i in np.indices(fits.shape)]

    #min_indices= uniquify(min_indices)
    #max_indices= uniquify(max_indices)
    #min_fits = uniquify([ fit for fit in minima ])
    #max_fits = uniquify([ fit for fit in maxima ])
    return min_indices, max_indices


def uniquify(seq, idfun=None): 
    """ remove douplicate elements from 'seq' (list), order preserving """
    if idfun is None:
        def idfun(x): return x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        # in old Python versions:
        # if seen.has_key(marker)
        # but in new ones:
        if marker in seen: continue
        seen[marker] = 1
        result.append(item)
    return result

def peakdet(v, delta, x = None):
    """
    Converted from MATLAB script at http://billauer.co.il/peakdet.html
    
    Returns two arrays
    
    function [maxtab, mintab]=peakdet(v, delta, x)
    %PEAKDET Detect peaks in a vector
    %        [MAXTAB, MINTAB] = PEAKDET(V, DELTA) finds the local
    %        maxima and minima ("peaks") in the vector V.
    %        MAXTAB and MINTAB consists of two columns. Column 1
    %        contains indices in V, and column 2 the found values.
    %      
    %        With [MAXTAB, MINTAB] = PEAKDET(V, DELTA, X) the indices
    %        in MAXTAB and MINTAB are replaced with the corresponding
    %        X-values.
    %
    %        A point is considered a maximum peak if it has the maximal
    %        value, and was preceded (to the left) by a value lower by
    %        DELTA.
    
    % Eli Billauer, 3.4.05 (Explicitly not copyrighted).
    % This function is released to the public domain; Any use is allowed.
    
    """
    import sys    
    maxtab = []
    mintab = []       
    if x is None:
        x = np.arange(len(v))    
    v = np.asarray(v)    
    if len(v) != len(x):
        sys.exit('Input vectors v and x must have same length')    
    if not np.isscalar(delta):
        sys.exit('Input argument delta must be a scalar')    
    if delta <= 0:
        sys.exit('Input argument delta must be positive')    
    mn, mx = np.Inf, -np.Inf
    mnpos, mxpos = np.NaN, np.NaN    
    lookformax = True    
    for i in np.arange(len(v)):
        this = v[i]
        if this > mx:
            mx = this
            mxpos = x[i]
        if this < mn:
            mn = this
            mnpos = x[i]        
        if lookformax:
            if this < mx-delta:
                maxtab.append((mxpos, mx))
                mn = this
                mnpos = x[i]
                lookformax = False
        else:
            if this > mn+delta:
                mintab.append((mnpos, mn))
                mx = this
                mxpos = x[i]
                lookformax = True
    return np.array(maxtab), np.array(mintab)

def segment_step(data, segment_key, no_steps, deviation=0, crop_points=0):
    segments={}
    #segments=dict.fromkeys(data, {'values':[], 'unit':[]})
    for i in data.keys():
        segments[i]={}
        segments[i]['values']=[]
        segments[i]['unit']=[]
    segments['label']=[]
    segments['filename']=[]
    
    datavec=data[segment_key]['values'][0]
    start=min(datavec)
    stop=max(datavec)
    bounds=np.linspace(start, stop, num=no_steps+1, endpoint=True, retstep=False)    
    for step in bounds:        
        index=np.where(np.logical_and(datavec>(step-deviation), datavec<(step+deviation)))        
        #print(index)
        if 'label' in data.keys():
            try:
                segments['label'].append(str(data['label'][0])+' %s= %s'%(str(segment_key), str(round(step, 4))))
            except:
                pass
        if 'filename' in data.keys():
            try:
                segments['filename'].append(str(data['filename'][0]))
            except:
                pass
        for key in data.iterkeys():
            try:
                segments[key]['values'].append(data[key]['values'][0][index][crop_points:-crop_points])
                segments[key]['unit'].append(data[key]['unit'][0])
            except:
                pass        
    print('dataset slices successfully')
    return segments

def segment_custom_step(data, segment_key, steplist, deviation=0, crop_points=0):
    segments={}
    #segments=dict.fromkeys(data, {'values':[], 'unit':[]})
    for i in data.keys():
        segments[i]={}
        segments[i]['values']=[]
        segments[i]['unit']=[]
    segments['label']=[]
    segments['filename']=[]
    
    datavec=data[segment_key]['values'][0]
      
    for step in steplist:        
        index=np.where(np.logical_and(datavec>(step-deviation), datavec<(step+deviation)))        
        #print(index)
        if 'label' in data.keys():
            try:
                segments['label'].append(str(data['label'][0])+' %s= %s'%(str(segment_key), str(round(step, 4))))
            except:
                pass
        if 'filename' in data.keys():
            try:
                segments['filename'].append(str(data['filename'][0]))
            except:
                pass
        for key in data.iterkeys():
            try:
                segments[key]['values'].append(data[key]['values'][0][index][crop_points:-crop_points])
                segments[key]['unit'].append(data[key]['unit'][0])
            except:
                pass        
    print('dataset slices successfully')
    return segments

def segment_interval(data, segment_key, interval, deviation=0, crop_points=0):
    segments={}
    #segments=dict.fromkeys(data, {'values':[], 'unit':[]})
    for i in data.keys():
        segments[i]={}
        segments[i]['values']=[]
        segments[i]['unit']=[]
    segments['label']=[]
    segments['filename']=[]
    
    datavec=data[segment_key]['values'][0]
    range=np.ptp(datavec)
    no_steps=int(range/interval)
    start=min(datavec)
    stop=max(datavec)
    bounds=np.linspace(start, stop, num=no_steps+1, endpoint=True, retstep=False)    
    for step in bounds:        
        index=np.where(np.logical_and(datavec>(step-deviation), datavec<(step+deviation)))        
        #print(index)
        if 'label' in data.keys():
            try:
                segments['label'].append(str(data['label'][0])+' %s= %s'%(str(segment_key), str(round(step, 4))))
            except:
                pass
        if 'filename' in data.keys():
            try:
                segments['filename'].append(str(data['filename'][0]))
            except:
                pass
        for key in data.iterkeys():
            try:
                segments[key]['values'].append(data[key]['values'][0][index][crop_points:-crop_points])
                segments[key]['unit'].append(data[key]['unit'][0])
            except:
                pass        
    print('dataset slices successfully')
    return segments


def segment_pick_by_index(data, indexlist):
    segments={}    
    for i in data.keys():        
        if 'values' and 'unit' in data[i]:
            segments[i]={}
            segments[i]['values']=[]
            segments[i]['unit']=[]
            for index in indexlist:
                try:
                    segments[i]['values'].append(data[i]['values'][index])                
                    segments[i]['unit'].append(data[i]['unit'][index])
                except:
                    pass
        else:
            segments[i]=[]           
            for index in indexlist:
                try:
                    segments[i].append(data[i][index])
                except:
                    pass
    return segments


def movaver(data, window_len):
    """a moving averager function"""
    wd = window_len
    d = data
    weightings = np.repeat(1.0, wd) / wd
    return np.convolve(d, weightings)[wd-1:-(wd-1)]

def Smooth(data, window_len=11, window='hanning'):
        """Smooth the data using a window with requested size.

        This method is based on the convolution of a scaled window with the
        signal.  The signal is prepared by introducing reflected copies of the
        signal (with the window size) in both ends so that transient parts are
        minimized in the begining and end part of the output signal.

        Parameters
        ----------
        data : ndarray
            The 1D numpy array to be smoothed
        window_len : int, optional
            The dimension of the smoothing window; should be an odd integer
        window : str, optional
            The type of window from 'flat', 'hanning', 'hamming', 'bartlett',
            'blackman' flat window will produce a moving average smoothing.

        See also
        --------
            smooth, numpy.hanning, numpy.hamming, numpy.bartlett,
            numpy.blackman, numpy.convolve, scipy.signal.lfilter


        Notes
        -----
            1) This was modified from http://wiki.scipy.org/Cookbook/SignalSmooth.
            2) length(output) != length(input), to correct this:
               return y[(window_len/2-1):-(window_len/2)] instead of just y.

        """

        if window_len<3:
            return

        if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
            raise ValueError, "Window is on of 'flat', 'hanning', 'hamming',"+\
                              "'bartlett', 'blackman'"

        xData = data

        s = np.r_[xData[window_len-1:0:-1], xData, xData[-1:-window_len:-1]]

        if window == 'flat': #moving average
            w = np.ones(window_len,'d')
        else:
            w = eval('np.'+window+'(window_len)')

        y = np.convolve(w/w.sum(), s, mode = 'valid')

        return y[window_len/2:-window_len/2+1]
