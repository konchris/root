
def symmetriepunkt(self,iterationen=1000,testbereich=0.8,xStart=None,yStart=None,Tfaktor=0.01,KickOff=0.0,ReturnError=False,verbose=False):
        '''Liefert die Koordinaten des Symmetriepunktes einer punktsymmetrischen
        Funktion als Tupel (x,y), (berechnet mit Monte Carlo-Algorithmus)

        iterationen: Anzahl der Iterationen (simuliertes Ausheilen bzw. Monte Carlo)
        testbereich: Innerer Bruchteil der Kurvenpunkte die zum Symmetrietest verwendet werden.
        xStart,yStart: Startwert fuer den Symmetriepunkt. Bei None wird die Mitte zwischen den
            Extrema gewaehlt.
        TFaktor: Temperatur in Einheiten des Fehlers (Standardabweichung zur gespiegelten Kurve).
        KickOff: Wahrscheinlichkeit, dass eine Zufallsaenderung bedinngungslos uebernommen wird.
            Fuer schwierige Faelle, damit lokale Minima verlassen werden koennen.
        ReturnError: Zusaetzlich zum Tupel der Koordinaten wird auch der Fehler zurueckgegeben: x,y,err
        verbose: Anzahl Iterationen nach der der Fehler ausgegeben wird. Bei False keine Ausgabe

        Folgendes Beispiel macht die Kurve a symmetrisch:
            a-=a.symmetriepunkt()
        '''
        def stdabw(xs,ys):
            Auswahl=(self.x>txmin) & (self.x<txmax)
            partx=self.x[Auswahl]
            party=self.y[Auswahl]
            spartx=xs-partx
            sparty=splder(spartx[::-1].copy(), spl, der=0)
            sparty=ys-sparty[::-1]
            stdabw=sum((party-sparty)**2)/len(party)
            return stdabw
        xmin=self.x.min()
        xmax=self.x.max()
        ymin=self.y.min()
        ymax=self.y.max()
        dx=xmax-xmin
        dy=ymax-ymin
        xNew=xNow=((xmin+xmax)*.5) if xStart==False else xStart
        yNew=yNow=((ymin+ymax)*.5) if yStart==False else yStart
        rand=.5*(1.-testbereich)
        txmin=xmin+dx*rand
        txmax=xmax-dx*rand
        tymin=ymin+dy*rand
        tymax=ymax-dy*rand
        spl=self.spline(k=3, imin=None, imax=None, xmin=None, xmax=None, newx=None, ableitung=None)
        ErrRef=0.01*dy
        Strengthx,Strengthy=0.001*dx,0.001*dy
        ErrNow=ErrBest=stdabw(xNow,yNow)
        rand1=numpy.random.rand
        rand2=numpy.random.uniform

        for n in xrange(iterationen):
            ErrNew=stdabw(xNew,yNew)
            #Strengthx=0.5*(Strengthx+ErrBest*dx/dy)
            #Strengthy=0.5*(Strengthy+ErrBest*dx/dy)
            if (ErrNew<ErrNow): xNow,yNow,ErrNow=xNew,yNew,ErrNew
            else:
                if math.exp(-ErrNew/ErrRef)>rand1(): xNow,yNow,ErrNow=xNew,yNew,ErrNew
                elif rand1()<KickOff: xNow,yNow,ErrNow=xNew,yNew,ErrNew
            if ErrNow<ErrBest: xBest,yBest,ErrBest=xNow,yNow,ErrNow
            xNew=xNow+Strengthx*rand2(-1.,1.)
            yNew=yNow+Strengthy*rand2(-1.,1.)
            if verbose:
                if n%verbose==0:
                    print str(n)+': ErrorNew,ErrorNow=',ErrNew,ErrNow
                    #if plotxy is None:
                    #else: plotxy.append(float(n), math.log(ErrNow,10))
            ErrRef=0.5*(ErrRef+ErrBest*Tfaktor)
        return (.5*xNow,.5*yNow,ErrBest) if ReturnError else (xNow,yNow)

    def symmetriepunkt_simpel(self,iterationen=20,pruefpunkte=8,zentralbereich=0.6):
        '''Liefert die Koordinaten des Symmetriepunktes einer punktsymmetrischen
        Funktion als Tupel (x,y).
        Ist eigentlich inzwischen ueberfluessig, seit die Monte Carlo-Methode fertig ist.

        iterationen: Anzahl der Schritte um den Symmetrieoffset in entweder x- oder
            y-Richtung auszugleichen (den jeweils Groesseren)
        pruefpunkte: legt fest aus wie vielen zufaellig ausgewaehlten x bzw.
            y-Werten pro Iterationsschritt der Offset berechnet wird
        zentralbereich: gibt an welcher Bruchteil an Funktionswerten um die Mitte
            herum ueberhaupt verwendet werden soll.

        Folgendes Beispiel macht die Kurve a symmetrisch:
            a-=a.symmetriepunkt()

        '''
        xmin=self.x.min()
        xmax=self.x.max()
        ymin=self.y.min()
        ymax=self.y.max()
        dx=0.5*zentralbereich*(xmax-xmin)
        dy=0.5*zentralbereich*(ymax-ymin)
        offsx=0.5*(xmax+xmin)
        offsy=0.5*(ymax+ymin)
        for i in xrange(iterationen):
            mx=my=0.0
            for i in range(pruefpunkte):
                rndx=numpy.random.rand()*dx
                rndy=numpy.random.rand()*dy
                try:
ax=self.getval(x=rndx+offsx)+self.getval(x=-rndx+offsx)-2*offsy
ay=self.getval(y=rndy+offsy)+self.getval(y=-rndy+offsy)-2*offsx
                    my+=ax
                    mx+=ay
                except:
                    pass
            if my/dy>mx/dx:
                offsy+=.5*my/pruefpunkte
            else:
                offsx+=.5*mx/pruefpunkte
        return offsx,offsy

    def symmetriepunkt_alt(self,iterationen=10,pruefraster=(25,11),eingrenzung=(0.3,0.73,0.64,0.55),testpunkte=20,testbereich=0.8,xstart=None,ystart=None):
        '''Liefert die Koordinaten des Symmetriepunktes einer punktsymmetrischen
        Funktion als Tupel (x,y).
        Ist eigentlich inzwischen ueberfluessig, seit die Monte Carlo-Methode fertig ist.

        iterationen: Anzahl der Iterationen
        pruefraster: Anzahl der Rasterpunkte in x und y-Richtung. Kann eine Liste
            sein fuer die einzelnen Iterationsschrittweiten. Der letzte Wert wird notfalls
            wiederholt.
        eingrenzung: Groesse des Rasterfeldes als Bruchteil. Kann eine Liste
            sein fuer die einzelnen Iterationsschrittweiten. Der letzte Wert wird notfalls
            wiederholt.
        testpunkte: Anzahl der Testpunkte fuer einen vorgeschlagenen Symmetriepunkt
        testbereich: Innerer Bruchteil der Kurvenpunkte die zum Symmetrietest verwendet werden.
        xstart,ystart: Startwerte fuer die Symmetriesuche. Standardwerte sind die Mittelpunkte

        Folgendes Beispiel macht die Kurve a symmetrisch:
            a-=a.symmetriepunkt()
        '''
        if type(pruefraster) not in (list,tuple): pruefraster=[pruefraster]
        if type(eingrenzung) not in (list,tuple): eingrenzung=[pruefraster]
        x,y=self.x,self.y
        xmin0=x.min()
        xmax0=x.max()
        ymin0=y.min()
        ymax0=y.max()
        symx=0.5*(xmax0+xmin0) if xstart is None else xstart
        symy=0.5*(ymax0+ymin0) if ystart is None else ystart
        xmin=(xmin0-symx)*testbereich/2.+symx
        xmax=(xmax0-symx)*testbereich/2.+symx
        ymin=(ymin0-symy)*testbereich/2.+symy
        ymax=(ymax0-symy)*testbereich/2.+symy
        l=len(self)
        asymmax=1E100
        testxmax=testymax=None
        erwartungsbereich=1
        for i in xrange(iterationen):
erwartungsbereich=erwartungsbereich*eingrenzung[min(i,len(eingrenzung)-1)]
            raster=pruefraster[min(i,len(pruefraster)-1)]
            #aview=numpy.ones((raster,raster),dtype='d')
            #xview=numpy.ones(raster,dtype='d')
            #yview=numpy.ones(raster,dtype='d')
            for ix in range(raster):
                for iy in range(raster):
testx=symx+(2*(ix/float(raster-1)-.5))*erwartungsbereich*(xmax-xmin)
testy=symy+(2*(iy/float(raster-1)-.5))*erwartungsbereich*(ymax-ymin)
                    asym=0.0
                    for j in range(testpunkte):
                        ir=-1
                        while ir==-1:
                            ir=numpy.random.randint(l)
                            if not (xmin<=x[ir]<=xmax and ymin<=y[ir]<=ymax)\
                                and (xmin0<=((2*symx-x[ir])<=xmax0) and (ymin0<=(2*symy-y[ir])<=ymax0)):
                                    ir=-1
asym+=math.sqrt(((2*testy-y[ir]-y)**2+(2*testx-x[ir]-x)**2).min())
                    asym/=float(testpunkte)
                    #aview[ix][iy]=asym
                    #xview[ix]=testx
                    #yview[iy]=testy
                    if asym<asymmax:
                        asymmax=asym
                        testxmax,testymax=testx,testy
            #print ""; print " "*8+'| ',
            #for ix in range(raster): print '%7.4f'%(xview[ix]),
            #print '-'*100
            #for iy in range(raster):
            #    print '%7.4f'%(yview[iy])+' |',
            #    for ix in range(raster): print '%7.4f'%(aview[ix][iy]),
            #    print ""
            symx=testxmax
            symy=testymax
        return symx,symy 
