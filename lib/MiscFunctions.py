import os
import numpy as np
import pandas as pd
import re

import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
import seaborn as sns
from scipy.interpolate import interp1d, UnivariateSpline
from scipy.optimize import curve_fit

from SuperconductivityCalculations import my_tanh
from MungingFunctions import savitzky_golay

HOME = os.path.expanduser('~')
DATA_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'data')
RESULTS_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'results')

DEVICES = ['ADWin', 'ITC503', 'Lakeshore', 'IPS']
SWEEPS = ['bsweep', 'tsweep', 'ivsweep', 'bramp', 'new_bsweep']

HOME = os.path.expanduser('~')
DATA_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'data')
RAW_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'raw-data')
RESULTS_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'results')


def save_figure(fig, fname, sample='', sample_run='', dpi=180):
    """Quickly save a figure.

    This saves the figure 'fig' in the 'RESULTS_DIR' base directory under
    filename 'fname'.
    Optionally one can provide a sample name and measurement run name if the
    file should be saved in a subdirectory of the results directory.

    Parameters
    ----------
    fig : matplotlib.figure.Figure
        The figure object to be saved.
    fname : str
        The basename of the file WITH extension (e.g. .png, .jpd, etc...)
    sample : str, optional
        The name of the sample
    sample_run : str, optional
        The name of the measurement run
    dpi : int
        The desired resolution of the image to be saved

    """
    full_fig_path = os.path.join(RESULTS_DIR, sample, 'cryo_measurements',
                                 sample_run)

    if not os.path.exists(full_fig_path):
        os.makedirs(full_fig_path)

    full_fig_path = os.path.join(full_fig_path, fname)

    fig.savefig(full_fig_path, bbox_inches='tight', dpi=dpi)


def interpolate_bfield(df_ips, df_adwin):
    """Interpolate the magnetfield strength for the ADwin data from ips data

    This assumes the magnet field sweep was just in one direction.
    Using the data saved via GPIB directly from the IPS, a channel is
    interpolated (using np.linspace) with the BField that matches the ADWin
    data length.

    Parameters
    ----------
    df_ips : pandas.DataFrame
        The data frame containing the data recorded from the IPS.
    df_adwin : pandas.DataFrame
        The data frame containing the data recorded by ADWin.

        Returns
    -------
    pandas.TimeSeries
        The smoothed magnet field file.

    """

    # Calculate the derivative of the IPS Magnetfield data for the purpose of
    # finding when the sweep started and stopped (i.e. dB > 0).
    Bdiff = np.hstack((np.array([0]), np.diff(df_ips['Magnetfield'])))
    dB = pd.TimeSeries(data=Bdiff, index=df_ips.index)

    # Get the indices of the start and end times,
    start_i = dB.index.get_loc(dB.index[dB != 0][0]) - 1
    end_i = dB.index.get_loc(dB.index[dB != 0][::-1][0])

    # Get the start and end timestamps and minutes
    start_t = dB.index[start_i]
    end_t = dB.index[end_i]

    # Get the start and end magnet field strengths in mT
    start_b = np.round(df_ips.Magnetfield[start_t], 6) * 1000
    end_b = np.round(df_ips.Magnetfield[end_t], 6) * 1000

    # Get the indices in the adwin index
    start_i = df_adwin.index.get_loc(start_t) - 1
    end_i = df_adwin.index.get_loc(end_t) + 0

    # Generate the start and end arrays, i.e. the parts of the measurement
    # before and after the magnet field sweep is running
    start_array = np.zeros((start_i,)) + start_b
    end_array = np.zeros((df_adwin.dR.count() - end_i,)) + end_b

    # Generay the sweep array
    mid_array = np.linspace(start_b, end_b, end_i - start_i, False)

    # Combine the generated arrays to one big array
    B_array = np.hstack((start_array, mid_array, end_array))
    # Create the time series in the adwin data frame
    return pd.TimeSeries(data=B_array, index=df_adwin.index)


def load_sweep_data(sweep_type, sample_name, sample_run):
    """Load the data from the files listed in the file_list file_list_filename

    Returns
    -------
    df : dict
        A dictionary where the top level is the measurement key (i.e.
        temperature and maybe sweep direction), followed by dictionaries
        sorted by device name, then each device's data in a pandas DataFrame.
        df = {}
        df['0327mK up 01'] = {}
        df['0327mK up 01']['ADWin'] = pandas.DataFrame()
        df['0327mK up 01']['filename'] =
                '2015-03-26T17-09-31_BSweep_327mK_+300mT_1mT'
        df['0327mK up 01']['modified'] = False
    df_extra : pandas.DataFrame
        This contains
        1. The estimated critical fields (positive and negative)
        2. The coherence lengths (based on critical field)
        3. The resistance at the critical field
        4. The resistance in normal conducting regime

    """
    assert sweep_type in SWEEPS, (sweep_type + ' is not a valid sweep. Valid '
                                  'sweep types are: ' + ', '.join(SWEEPS))

    # Generate the filename of the file that has the list of the good data
    # files
    file_list_filename = '{}_files.csv'.format(sweep_type)
    full_path = os.path.join(DATA_DIR, sample_name, 'cryo_measurements',
                             sample_run, file_list_filename)

    # Sometimes value errors were raised. I cannot remember anymore why.
    try:
        file_lists = pd.DataFrame.from_csv(full_path)
    except ValueError:
        print('ValueError raised for {}'.format(file_list_filename))

    # Count the number of sweeps at a specific temperature
    i = 1

    # Initialize the empty data dictionary
    df = {}

    # Go through each file in the list and load its data
    for fname in file_lists['file name']:

        full_path = os.path.join(DATA_DIR, sample_name, 'cryo_measurements',
                                 sample_run, fname + '.h5')

        # Split up the filename into basic components that provide information
        # about the measurement
        if sweep_type in ['bsweep', 'bramp']:
            (datetime_stamp, meas_type_unused, meas_temp, meas_end,
             meas_rate) = re.split(r'[_.]', fname)

            # Convert the tempreature into millikelvin. Allows for easier
            # sorting later
            meas_temp = int(meas_temp.rstrip('mK'))

            # Figure out the direction of the magnet field sweep
            if int(meas_end.rstrip('mT')) < 0:
                meas_direction = 'down'
            elif int(meas_end.rstrip('mT')) > 0:
                meas_direction = 'up'

            # Generate the key for the measurement.
            # Pad temperatures less than one kelvin with zeros (this allows for
            #    ascending sorting)
            key = '{t:04}mK {d} {n:02}'.format(t=meas_temp, d=meas_direction,
                                               n=i)

            # If that key has already been generated and used for data that has
            # been loaded, increment the measurement counter.
            if key in df.keys():
                i += 1
                key = '{t:04}mK {d} {n:02}'.format(t=meas_temp,
                                                   d=meas_direction,
                                                   n=i)
            elif i > 1:
                # i -= 1
                key = '{t:04}mK {d} {n:02}'.format(t=meas_temp,
                                                   d=meas_direction,
                                                   n=i)

        elif sweep_type == 'tsweep':
            key = re.split(r'[_.]', fname)[1]

            df_extra = None

        # If the final key is not present in the dataset, initialize the key
        if key not in df.keys():
            df[key] = {}
            df[key]['filename'] = fname
            df[key]['modified'] = False

        for device in DEVICES:

            # Create the HDFStore object
            hdf_store = pd.HDFStore(full_path)

            # Generate the key under which the device's data was stored
            hdf_key = '/proc01/{}'.format(device)

            if hdf_key in hdf_store.keys():

                if device not in df[key].keys():
                    # One measurement did not have the ITC data in it, which
                    # caused a KeyError
                    try:
                        df[key][device] = hdf_store[hdf_key]
                    except KeyError:
                        pass

            hdf_store.close()

    extras_path = os.path.join(DATA_DIR, sample_name, 'cryo_measurements',
                               sample_run, 'critical_fields_{}s.csv'
                               .format(sweep_type))

    try:
        df_extra = pd.read_csv(extras_path)
    except OSError:
        print('Could not load {f}'.format(f=extras_path))
        df_extra = pd.DataFrame()

    return (df, df_extra)


def save_data(df, sample_name, sample_run):
    """Save the data from the dataset back into the files.

    Parameters
    ----------
    df : dict
        A dictionary where the top level is the measurement key (i.e.
        temperature and maybe sweep direction), followed by dictionaries
        sorted by device name, then each device's data in a pandas DataFrame.
        df = {}
        df['0327mK up 01'] = {}
        df['0327mK up 01']['ADWin'] = pandas.DataFrame()
        df['0327mK up 01']['filename'] =
                '2015-03-26T17-09-31_BSweep_327mK_+300mT_1mT'

    """
    # Generate the filename of the file that has the list of the good data
    # files
    for key in df.keys():
        if df[key]['modified']:
            filename = df[key]['filename'] + '.h5'
            full_path = os.path.join(DATA_DIR, sample_name, sample_run,
                                     filename)
            print('Saving {}'.format(filename))

            for device in DEVICES:

                if device in df[key].keys():

                    # Create the HDFStore object
                    hdf_store = pd.HDFStore(full_path)

                    # Generate the key under which the device's data was stored
                    hdf_key = '/proc01/{}'.format(device)

                    try:
                        hdf_store[hdf_key] = df[key][device]
                    except KeyError:
                        pass

                    hdf_store.close()


def nextpow2(i):
    """Find next exponential power of two.

    Parameters
    ----------
    i : int, float
        The number for which one wants to find the next power of two

    Returns
    -------
    n : float
        The next highest power of two

    """
    n = 1
    while n < i:
        n *= 2

    return n


def remove_valley2(measurement, key, bounds=(0.0, 0.0)):
    """Split BSweep into positive and negative parts, removing middle.

    This assumes that the key $\Phi / \Phi_0$ is present in the measurement,
    if not it will fall back to BField

    Parameters
    ----------
    old_df : dict
        The data frame to modify
    bounds : tuple with two floats
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.

    Return
    ------
    fig_test, ax_test : matplotlib figure and axis
        The figure and axis showing the data
    """
    if 'B' in measurement:
        b_key = 'B'
        xunit = '[T]'
    elif '$\Phi / \Phi_0$' in measurement:
        b_key = '$\Phi / \Phi_0$'
        xunit = ''
    elif 'BField' in measurement:
        b_key = 'BField'
        xunit = '[mT]'
    elif 'BzField' in measurement:
        b_key = 'BzField'
        xunit = '[mT]'
    else:
        print('Could not find a valid channel for representing the magnet'
              ' field. Please try something else.')
        return

    yunit = r'[$\Omega$]'
    if 'dR' in measurement:
        r_key = 'dR'
    elif 'dRSample' in measurement:
        r_key = 'dRSample'
    elif 'R' in measurement:
        r_key = 'R'
    elif 'RSample' in measurement:
        r_key = 'RSample'
    else:
        print('Could not find a valid channel for representing the resistance.'
              ' Please try something else.')
        return

    neg_bndry = bounds[0]
    print(neg_bndry)
    pos_bndry = bounds[1]
    print(pos_bndry)

    temp_pos = measurement[measurement[b_key] > pos_bndry]
    temp_neg = measurement[measurement[b_key] < neg_bndry]

    y_pos = temp_pos[r_key]

    y_neg = temp_neg[r_key]

    lim_pos = y_pos[np.abs(y_pos - pos_bndry).argmin()]
    lim_neg = y_neg[np.abs(y_neg - neg_bndry).argmin()]

    lim = (lim_pos + lim_neg) / 2

    x_pos = temp_pos[b_key]
    x_neg = temp_neg[b_key]

    fig_test, ax_test = plt.subplots()

    ax_test.plot(measurement[b_key][::10], measurement[r_key][::10],
                 label='complete', ls='--', color=sns.xkcd_rgb['grey'],
                 lw=0.75)
    ax_test.plot(x_neg[::10], y_neg[::10], label='negative')
    ax_test.plot(x_pos[::10], y_pos[::10], label='positive')

    ax_test.axvline(neg_bndry, color='red')
    ax_test.axvline(pos_bndry, color='red')
    ax_test.set_ylim(lim - 1, lim + 10)
    ax_test.set_xlim(neg_bndry - 10, pos_bndry + 10)

    minor_locator = AutoMinorLocator(10)
    ax_test.xaxis.set_minor_locator(minor_locator)
    plt.grid(True, 'both')
    plt.grid(True, 'minor', ls='--', lw=1)

    ax_test.set_title('Cut of BSweep: {}'.format(key))
    ax_test.set_xlabel('{k} {u}'.format(k=b_key, u=xunit))
    ax_test.set_ylabel('{k} {u}'.format(k=r_key, u=yunit))

    fig_test.tight_layout()

    return fig_test, ax_test


def generate_splines(measurement, key, bounds=(0.0, 0.0),
                     smooth_s=(65536, 65536)):
    """Generate interoplated negative and positive data for FFT

    Parameters
    ----------
    old_df : dict
        The data frame to modify
    bounds : tuple with two floats, optional
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.
    smooth_s : tuple with two ints, optional
        Smoothing factors for the positive and negative branches for the
        univariate spline fitting.
        DEFAULT: (65536, 65536)

    Return
    ------
    fig_test, ax_test : matplotlib figure and axis
        The figure and axis showing the data
    new_measurement : dict
        The interpolated data, ready for use in the fft
    """

    # print(key)

    if 'RSample' in measurement:
        r_key = 'RSample'
        yunit = '[$\Omega$]'

    if 'BField' in measurement:
        b_key = 'BField'
        xunit = '[T]'
    elif 'B' in measurement:
        b_key = 'B'
        xunit = '[T]'
    elif 'BzField' in measurement:
        b_key = 'BzField'
        xunit = '[T]'
    else:
        print('Could not find a valid channel for representing the magnet'
              ' field. Please try something else.')

    # Define the boundaries
    neg_bndry = bounds[0]
    pos_bndry = bounds[1]

    # Define the smoothing
    neg_smooth = smooth_s[0]
    pos_smooth = smooth_s[1]

    # Get the positive and negative dataframes
    temp_pos = measurement[measurement[b_key] > pos_bndry]
    temp_neg = measurement[measurement[b_key] < neg_bndry]

    # Get the x channels
    x_pos = temp_pos[b_key]
    x_neg = temp_neg[b_key]

    # Find next power of two of each channel's length
    idx_pos = nextpow2(len(x_pos))
    idx_neg = nextpow2(len(x_neg))

    # Linearly inerpolate the x channels (we are assuming here an monotonically
    # increasing function, i.e. strictly linear!)
    new_x_pos = np.linspace(np.min(x_pos), np.max(x_pos), idx_pos)
    new_x_neg = np.linspace(np.min(x_neg), np.max(x_neg), idx_neg)

    # Setup the new data frame
    new_measurement = {}
    new_measurement['mods'] = []
    new_measurement['modified'] = True
    new_measurement['negFFT'] = pd.DataFrame({'IntBField': new_x_neg})
    new_measurement['posFFT'] = pd.DataFrame({'IntBField': new_x_pos})

    # Setup the plot
    fig_splinefit, ax_splinefit = plt.subplots(nrows=3, ncols=2)

    if True:
        # Get the y channels
        y_pos = temp_pos[r_key]
        y_neg = temp_neg[r_key]

        # Linearly interpolate the y channels and generate
        # print('Doing linear interpolation of x')
        f_pos = interp1d(x_pos, y_pos, 'linear')
        f_neg = interp1d(x_neg, y_neg, 'linear')

        new_y_pos = f_pos(new_x_pos)
        new_y_neg = f_neg(new_x_neg)

        # Sometimes a nan sneaks in and the rest of the analysis does not
        # work. Catch that here.
        if np.isnan(new_y_pos).any():
            # print(key, 'Nan in new_y_pos')
            new_y_pos = np.nan_to_num(new_y_pos)
            new_y_pos[new_y_pos == 0] = new_y_pos[new_y_pos != 0].min()

        if np.isnan(new_y_neg).any():
            # print(key, 'Nan in new_y_neg')
            new_y_neg = np.nan_to_num(new_y_neg)
            new_y_neg[new_y_neg == 0] = new_y_neg[new_y_neg != 0].max()

        # print(new_x_pos, new_y_pos)

        # Do the univatiate spline thing
        # print('orders', smooth_s)
        # print('Doing univariate spling thing')
        us_pos = UnivariateSpline(new_x_pos, new_y_pos, s=pos_smooth)
        us_neg = UnivariateSpline(new_x_neg, new_y_neg, s=neg_smooth)

        # Generate the background reduced data by subtracting the univariate
        #  spline from the linearly interpolated data
        # print('Calculating reduced background')
        new_y_pos_reduced = new_y_pos - us_pos(new_x_pos)
        new_y_neg_reduced = new_y_neg - us_neg(new_x_neg)

        int_r_key = 'Int_{}'.format(r_key)
        back_r_key = 'Back_{}'.format(r_key)
        red_r_key = 'Red_{}'.format(r_key)
        # print(int_r_key, back_r_key, red_r_key)

        new_measurement['negFFT'][int_r_key] = new_y_neg
        new_measurement['negFFT'][back_r_key] = new_y_neg - \
            new_y_neg_reduced
        new_measurement['negFFT'][red_r_key] = new_y_neg_reduced
        new_measurement['mods'].append('Adding interpolated and background'
                                       '-reduced negative branch of sweep for'
                                       ' FFT.')

        new_measurement['posFFT'][int_r_key] = new_y_pos
        new_measurement['posFFT'][back_r_key] = new_y_pos - \
            new_y_pos_reduced
        new_measurement['posFFT'][red_r_key] = new_y_pos_reduced
        new_measurement['mods'].append('Adding interpolated and background'
                                       '-reduced positive branch of sweep for'
                                       ' FFT.')

        # print('Plotting')
        # Interpolated Data
        ax_splinefit[0][0].plot(new_x_neg[::10], new_y_neg[::10],
                                label=r_key)
        ax_splinefit[0][1].plot(new_x_pos[::10], new_y_pos[::10],
                                label=r_key)

        # Spline Smoothed Data
        ax_splinefit[1][0].plot(new_x_neg[::10],
                                (new_y_neg - new_y_neg_reduced)[::10],
                                label=r_key)
        ax_splinefit[1][1].plot(new_x_pos[::10],
                                (new_y_pos - new_y_pos_reduced)[::10],
                                label=r_key)

        # Reduced data
        ax_splinefit[2][0].plot(new_x_neg[::10], new_y_neg_reduced[::10],
                                label=r_key)
        ax_splinefit[2][1].plot(new_x_pos[::10], new_y_pos_reduced[::10],
                                label=r_key)
        # print('Finished plotting')
 
    ax_splinefit[0][0].set_title('Negative')
    ax_splinefit[0][1].set_title('Positive')

    ax_splinefit[0][0].set_ylabel('Interpolated Data\n$R_s$ {}'
                                  .format(yunit))
    ax_splinefit[1][0].set_ylabel('Spline Smoothed\n$R_s$ {}'
                                  .format(yunit))
    ax_splinefit[2][0].set_ylabel('Bkgnd Reduced\n$R_s$ {}'
                                  .format(yunit))

    ax_splinefit[1][0].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))
    ax_splinefit[1][1].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))

    fig_splinefit.tight_layout()

    return fig_splinefit, ax_splinefit, new_measurement


def generate_tanh_fits(measurement, key, bounds=(0.0, 0.0),
                       init_params = ([1, 1, 1, 1], [1, 1, 1, 1])):
    """Generate tanh fits of the negative and positive data

    Parameters
    ----------
    measurement : dict
        The data frame to modify
    key : str
        The name of the measurement
    bounds : tuple with two floats, optional
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.
    init_params : tuple of two four-element lists
        The initial parameters for performing the tanh fit.
        DEFAULT: ([1, 1, 1, 1], [1, 1, 1, 1])

    Return
    ------
    fig_test, ax_test : matplotlib figure and axis
        The figure and axis showing the data
    new_measurement : dict
        The interpolated data, ready for use in the fft
    """

    reses = []
    for chan in measurement:
        if 'RSample' in chan:
            reses.append(chan)
        elif chan == 'R':
            reses.append(chan)
        elif chan == 'dR':
            reses.append(chan)
        # elif chan == '$R/R_N$':
        #     reses.append(chan)
        # elif chan == '$dR/dR_N$':
        #     reses.append(chan)

    if '$\Phi / \Phi_0$' in measurement:
        b_key = '$\Phi / \Phi_0$'
        xunit = ''
    elif 'BField' in measurement:
        b_key = 'BField'
        xunit = '[mT]'
    elif 'BzField' in measurement:
        b_key = 'BzField'
        xunit = '[mT]'
    else:
        print('Could not find a valid channel for representing the magnet'
              ' field. Please try something else.')

    yunit = r'[$\Omega$]'

    # Define the boundaries
    neg_bndry = bounds[0]
    pos_bndry = bounds[1]

    # Get the positive and negative dataframes
    temp_pos = measurement[measurement[b_key] > pos_bndry]
    temp_neg = measurement[measurement[b_key] < neg_bndry]

    # Get the x channels
    x_pos = temp_pos[b_key]
    x_neg = temp_neg[b_key]

    # Find next power of two of each channel's length
    idx_pos = nextpow2(len(x_pos))
    idx_neg = nextpow2(len(x_neg))

    # Generate a new index array for each channel
    # k_pos = np.arange(0, idx_pos)
    # k_neg = np.arange(0, idx_neg)

    # Linearly inerpolate the x channels (we are assuming here an monotonically
    # increasing function, i.e. strictly linear!)
    new_x_pos = np.linspace(np.min(x_pos), np.max(x_pos), idx_pos)
    new_x_neg = np.linspace(np.min(x_neg), np.max(x_neg), idx_neg)

    # Setup the new data frame
    new_measurement = {}
    new_measurement['mods'] = []
    new_measurement['modified'] = True
    new_measurement['negFFT'] = pd.DataFrame({'IntPhi0': new_x_neg})
    new_measurement['posFFT'] = pd.DataFrame({'IntPhi0': new_x_pos})

    # Setup the plot
    fig_tanhfit, ax_tanhfit = plt.subplots(nrows=3, ncols=2)

    for (i, r_key) in enumerate(reses):
        # Get the y channels
        y_pos = temp_pos[r_key]
        y_neg = temp_neg[r_key]

        # Linearly interpolate the y channels and generate
        # print('Doing linear interpolation of x')
        f_pos = interp1d(x_pos, y_pos, 'linear')
        f_neg = interp1d(x_neg, y_neg, 'linear')

        new_y_pos = f_pos(new_x_pos)
        new_y_neg = f_neg(new_x_neg)

        # Sometimes a nan sneaks in and the rest of the analysis does not
        # work. Catch that here.
        if np.isnan(new_y_pos).any():
            print(key, 'Nan in new_y_pos')
            new_y_pos = np.nan_to_num(new_y_pos)
            new_y_pos[new_y_pos == 0] = new_y_pos[new_y_pos != 0].min()

        if np.isnan(new_y_neg).any():
            print(key, 'Nan in new_y_neg')
            new_y_neg = np.nan_to_num(new_y_neg)
            new_y_neg[new_y_neg == 0] = new_y_neg[new_y_neg != 0].min()

        # Do the tanh fit
        # print('Doing tanh fit')
        params_pos = curve_fit(my_tanh, new_x_pos, new_y_pos, p0=init_params,
                               maxfev=1000)
        params_neg = curve_fit(my_tanh, new_x_neg, new_y_neg, p0=init_params,
                               maxfev=1000)

        print('Pos:', params_pos[0])
        print('Neg:', params_neg[0])

        y_tanh_pos = my_tanh(new_x_pos, *params_pos[0])
        y_tanh_neg = my_tanh(new_x_neg, *params_neg[0])

        # Generate the background reduced data by subtracting the univariate
        #  spline from the linearly interpolated data
        # print('Calculating reduced background')
        new_y_pos_reduced = new_y_pos - y_tanh_pos
        new_y_neg_reduced = new_y_neg - y_tanh_neg

        int_r_key = 'Int{}'.format(r_key)
        back_r_key = 'Back{}'.format(r_key)
        red_r_key = 'Red{}'.format(r_key)
        # print(int_r_key, back_r_key, red_r_key)

        new_measurement['negFFT'][int_r_key] = new_y_neg
        new_measurement['negFFT'][back_r_key] = new_y_neg - \
            new_y_neg_reduced
        new_measurement['negFFT'][red_r_key] = new_y_neg_reduced
        new_measurement['mods'].append('Adding interpolated and background'
                                       '-reduced negative branch of sweep for'
                                       ' FFT.')

        new_measurement['posFFT'][int_r_key] = new_y_pos
        new_measurement['posFFT'][back_r_key] = new_y_pos - \
            new_y_pos_reduced
        new_measurement['posFFT'][red_r_key] = new_y_pos_reduced
        new_measurement['mods'].append('Adding interpolated and background'
                                       '-reduced positive branch of sweep for'
                                       ' FFT.')

        print('Plotting')
        # Interpolated Data
        ax_tanhfit[0][0].plot(new_x_neg[::10], new_y_neg[::10],
                              label=r_key)
        ax_tanhfit[0][1].plot(new_x_pos[::10], new_y_pos[::10],
                              label=r_key)

        # Spline Smoothed Data
        ax_tanhfit[1][0].plot(new_x_neg[::10],
                              (new_y_neg - new_y_neg_reduced)[::10],
                              label=r_key)
        ax_tanhfit[1][1].plot(new_x_pos[::10],
                              (new_y_pos - new_y_pos_reduced)[::10],
                              label=r_key)

        # Reduced data
        ax_tanhfit[2][0].plot(new_x_neg[::10], new_y_neg_reduced[::10],
                              label=r_key)
        ax_tanhfit[2][1].plot(new_x_pos[::10], new_y_pos_reduced[::10],
                              label=r_key)
        print('Finished plotting')

    ax_tanhfit[0][0].set_title('Negative')
    ax_tanhfit[0][1].set_title('Positive')

    ax_tanhfit[0][0].set_ylabel('Interpolated Data\n$R_s$ {}'
                                .format(yunit))
    ax_tanhfit[1][0].set_ylabel('Spline Smoothed\n$R_s$ {}'
                                .format(yunit))
    ax_tanhfit[2][0].set_ylabel('Bkgnd Reduced\n$R_s$ {}'
                                .format(yunit))

    ax_tanhfit[1][0].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))
    ax_tanhfit[1][1].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))

    fig_tanhfit.tight_layout()

    return fig_tanhfit, ax_tanhfit, new_measurement


def nearest_value(data, value):
    """Find the index and value in an array nearest a given value.

    Parameters
    ----------
    data : array-like
        An array like object through which to search
    value : scalar
        The value you with to find

    Returns
    -------
    idx : int
        The index of the value nearest the given value
    val : scalar
        The value nearest the given value
    """

    idx = np.abs(data - value).argmin()

    return idx, data[idx]


def generate_poly_fits2(measurement, key, bounds=(0.0, 0.0), deg=1):
    """Generate poly fits of the negative and positive data

    Parameters
    ----------
    measurement : dict
        The data frame to modify
    key : str
        The name of the measurement
    bounds : tuple with two floats, optional
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.
    deg : int
        The degree of the polynomial that should be used to fit the data

    Return
    ------
    fig_test, ax_test : matplotlib figure and axis
        The figure and axis showing the data
    new_measurement : dict
        The interpolated data, ready for use in the fft
    """

    reses = []
    for chan in measurement:
        if 'dR/dR_N' in chan:
            reses.append(chan)
        # elif chan == 'R':
        #    reses.append(chan)
        # elif chan == 'dR':
        #    reses.append(chan)
        # elif chan == '$R/R_N$':
        #     reses.append(chan)
        # elif chan == '$dR/dR_N$':
        #     reses.append(chan)

    if '$\Phi / \Phi_0$' in measurement:
        b_key = '$\Phi / \Phi_0$'
        xunit = ''
    elif 'BField' in measurement:
        b_key = 'BField'
        xunit = '[mT]'
    elif 'BzField' in measurement:
        b_key = 'BzField'
        xunit = '[mT]'
    else:
        print('Could not find a valid channel for representing the magnet'
              ' field. Please try something else.')

    yunit = r'[$\Omega$]'

    # Define the boundaries
    neg_bndry = bounds[0]
    pos_bndry = bounds[1]

    # Get the positive and negative dataframes
    temp_pos = measurement[measurement[b_key] > pos_bndry]
    temp_neg = measurement[measurement[b_key] < neg_bndry]

    # Get the x channels
    x_pos = temp_pos[b_key]
    x_neg = temp_neg[b_key]

    # Find next power of two of each channel's length
    idx_pos = nextpow2(len(x_pos))
    idx_neg = nextpow2(len(x_neg))

    # Generate a new index array for each channel
    # k_pos = np.arange(0, idx_pos)
    # k_neg = np.arange(0, idx_neg)

    # Linearly inerpolate the x channels (we are assuming here an monotonically
    # increasing function, i.e. strictly linear!)
    new_x_pos = np.linspace(np.min(x_pos), np.max(x_pos), idx_pos)
    new_x_neg = np.linspace(np.min(x_neg), np.max(x_neg), idx_neg)

    # Setup the new data frame
    new_measurement = {}
    new_measurement['mods'] = []
    new_measurement['modified'] = True
    new_measurement['negFFT'] = pd.DataFrame({'IntBField': new_x_neg})
    new_measurement['posFFT'] = pd.DataFrame({'IntBField': new_x_pos})

    # Setup the plot
    fig_poly_fit, ax_poly_fit = plt.subplots(nrows=2, ncols=2)

    for (i, r_key) in enumerate(reses):
        # Get the y channels
        y_pos = temp_pos[r_key]
        y_neg = temp_neg[r_key]

        # Linearly interpolate the y channels and generate
        # print('Doing linear interpolation of x')
        f_pos = interp1d(x_pos, y_pos, 'linear')
        f_neg = interp1d(x_neg, y_neg, 'linear')

        new_y_pos = f_pos(new_x_pos)
        new_y_neg = f_neg(new_x_neg)

        # Sometimes a nan sneaks in and the rest of the analysis does not
        # work. Catch that here.
        if np.isnan(new_y_pos).any():
            print(key, 'Nan in new_y_pos')
            new_y_pos = np.nan_to_num(new_y_pos)
            new_y_pos[new_y_pos == 0] = new_y_pos[new_y_pos != 0].min()

        if np.isnan(new_y_neg).any():
            print(key, 'Nan in new_y_neg')
            new_y_neg = np.nan_to_num(new_y_neg)
            new_y_neg[new_y_neg == 0] = new_y_neg[new_y_neg != 0].max()

        # Do the polynomial fit
        print('Doing polynomial fit')
        # pos_poly_params = np.polyfit(x_pos, y_pos, deg)
        # neg_poly_params = np.polyfit(x_neg, y_neg, deg)

        # pos_poly_func = np.poly1d(pos_poly_params)
        # neg_poly_func = np.poly1d(neg_poly_params)

        new_pos_poly_params = np.polyfit(new_x_pos, new_y_pos, deg)
        new_neg_poly_params = np.polyfit(new_x_neg, new_y_neg, deg)

        new_pos_poly_func = np.poly1d(new_pos_poly_params)
        new_neg_poly_func = np.poly1d(new_neg_poly_params)

        # Generate the fit data, i.e. the background
        # y_pos_poly_fit = pos_poly_func(x_pos)
        # y_neg_poly_fit = neg_poly_func(x_neg)

        new_y_pos_poly_fit = new_pos_poly_func(new_x_pos)
        new_y_neg_poly_fit = new_neg_poly_func(new_x_neg)

        # Generate the background reduced data by subtracting the polynomial
        #  fit from the linearly interpolated data
        # y_pos_reduced = y_pos - y_pos_poly_fit
        # y_neg_reduced = y_neg - y_neg_poly_fit

        new_y_pos_reduced = new_y_pos - new_y_pos_poly_fit
        new_y_neg_reduced = new_y_neg - new_y_neg_poly_fit

        if 'dR/dR_N' in r_key:
            r_name = 'dRn'
        elif 'dR' in r_key:
            r_name = 'dR'
        elif 'R' in r_key:
            r_name = 'R'

        int_r_key = 'Int_{}'.format(r_name)
        back_r_key = 'Back_{}'.format(r_name)
        red_r_key = 'Red_{}'.format(r_name)
        # print(int_r_key, back_r_key, red_r_key)

        new_measurement['negFFT'][int_r_key] = new_y_neg
        new_measurement['negFFT'][back_r_key] = new_y_neg - \
            new_y_neg_reduced
        new_measurement['negFFT'][red_r_key] = new_y_neg_reduced
        new_measurement['mods'].append('Adding interpolated and background'
                                       '-reduced negative branch of sweep for'
                                       ' FFT.')

        new_measurement['posFFT'][int_r_key] = new_y_pos
        new_measurement['posFFT'][back_r_key] = new_y_pos - \
            new_y_pos_reduced
        new_measurement['posFFT'][red_r_key] = new_y_pos_reduced
        new_measurement['mods'].append('Adding interpolated and background'
                                       '-reduced positive branch of sweep for'
                                       ' FFT.')

        print('Plotting')
        # Interpolated Data
        ax_poly_fit[0][0].plot(new_x_neg[::10], new_y_neg[::10],
                               label=r_key + ' int.')
        ax_poly_fit[0][1].plot(new_x_pos[::10], new_y_pos[::10],
                               label=r_key + ' int.')

        #  Fit Smoothed Data
        ax_poly_fit[0][0].plot(new_x_neg[::10], new_y_neg_poly_fit[::10],
                               label=r_key + ' bckgrnd')
        ax_poly_fit[0][1].plot(new_x_pos[::10], new_y_pos_poly_fit[::10],
                               label=r_key + ' bckgrnd')

        # Reduced data
        ax_poly_fit[1][0].plot(new_x_neg[::10], new_y_neg_reduced[::10],
                               label=r_key + ' red')
        ax_poly_fit[1][1].plot(new_x_pos[::10], new_y_pos_reduced[::10],
                               label=r_key + ' red')
        print('Finished plotting')

    ax_poly_fit[0][0].set_title('Negative {:d}-deg'.format(deg))
    ax_poly_fit[0][1].set_title('Positive {:d}-deg'.format(deg))

    ax_poly_fit[0][1].legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
    ax_poly_fit[1][1].legend(loc='center left', bbox_to_anchor=(1.0, 0.5))

    # ax_poly_fit[0][0].set_ylabel('Interpolated Data\n$R_s$ {}'
    #                            .format(yunit))
    ax_poly_fit[0][0].set_ylabel('$R_s$ {}'.format(yunit))
    ax_poly_fit[1][0].set_ylabel('Bkgnd Reduced\n$R_s$ {}'
                                 .format(yunit))

    ax_poly_fit[1][0].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))
    ax_poly_fit[1][1].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))

    fig_poly_fit.tight_layout()

    return fig_poly_fit, ax_poly_fit, new_measurement


def envelope(x, y, averagewin=15):
    """Find fit envelope.

    From Torsten Pietsch
    """
    from scipy import signal
    from scipy.interpolate import interp1d
    envlp = np.abs(signal.hilbert(y, N=None, axis=-1))
    smooth_enevelope = np.smooth(envlp, window_len=averagewin,
                                 window='hanning')
    fit_envelope = interp1d(x, smooth_enevelope, kind='linear')
    return fit_envelope


def remove_valley(measurement, key, bounds=(0.0, 0.0)):
    """Split BSweep into positive and negative parts, removing middle.

    This assumes that the key $\Phi / \Phi_0$ is present in the measurement,
    if not it will fall back to BField

    Parameters
    ----------
    old_df : dict
        The data frame to modify
    bounds : tuple with two floats
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.

    Return
    ------
    fig_test, ax_test : matplotlib figure and axis
        The figure and axis showing the data
    """
    # Get the name and units of the x channel to use. Priority is given to 'B'
    if 'B' in measurement:
        b_key = 'B'
        xunit = '[T]'
    elif 'xMagnet' in measurement:
        b_key = 'xMagnet'
        xunit = '[T]'
    elif 'BField' in measurement:
        b_key = 'BField'
        xunit = '[mT]'
    elif 'BzField' in measurement:
        b_key = 'BzField'
        xunit = '[T]'
    else:
        print('Could not find a valid channel for representing the magnet'
              ' field. Please try something else.')
        return

    # Get the name and units of the y channel to use. Priority is given to 'dR'
    yunit = r'[$\Omega$]'
    if 'dR' in measurement:
        r_key = 'dR'
    elif 'dRSample' in measurement:
        r_key = 'dRSample'
    elif 'R' in measurement:
        r_key = 'R'
    elif 'RSample' in measurement:
        r_key = 'RSample'
    else:
        print('Could not find a valid channel for representing the resistance.'
              ' Please try something else.')
        return

    # Get the upper boundary for the negative branch
    neg_bndry = bounds[0]
    # Get the lower boundary for the positive branch
    pos_bndry = bounds[1]

    # Set the offset for the positive and negative x-axis plotting limits
    xlim = 0.02  # T
    # Set the lower offset of the y-axis plotting limit
    ylim_low = 10  # Ohm
    # Set the upper offset of the y-axis plotting limit
    ylim_high = 50  # Ohm

    # Grab the negative branch data
    temp_neg = measurement[measurement[b_key] < neg_bndry]
    # Grab the positive branch data
    temp_pos = measurement[measurement[b_key] > pos_bndry]

    # Get the negative and positive y-channels
    y_neg = temp_neg[r_key]
    y_pos = temp_pos[r_key]

    # Set the y-axis plotting limits
    lim_neg = y_neg[np.abs(y_neg - neg_bndry).argmin()]
    lim_pos = y_pos[np.abs(y_pos - pos_bndry).argmin()]

    y_neg_min = y_neg.argmin()
    y_pos_min = y_pos.argmin()

    lim = (lim_pos + lim_neg) / 2

    if lim - ylim_low > y_pos.min():
        ylim_low = lim - y_pos.min() + 1
    if lim - ylim_low > y_neg.min():
        ylim_low = lim - y_neg.min() + 1

    # Get the x-axis channels
    x_pos = temp_pos[b_key]
    x_neg = temp_neg[b_key]

    fig_test, ax_test = plt.subplots()

    # Plot the data
    # Plot the complete data in grey and dashed in the background for reference
    ax_test.plot(measurement[b_key][::10], measurement[r_key][::10],
                 label='complete', ls='--', color=sns.xkcd_rgb['grey'],
                 lw=0.75)
    # Plot the negative branch
    ax_test.plot(x_neg[::10], y_neg[::10], label='negative')
    # Plot the positive branch
    ax_test.plot(x_pos[::10], y_pos[::10], label='positive')

    # Put red lines where the upper negative and lower positive boundaries are
    ax_test.axvline(neg_bndry, color='red')
    ax_test.axvline(pos_bndry, color='red')

    # Set the plotting axes' limits
    ax_test.set_ylim(lim - ylim_low, lim + ylim_high)
    ax_test.set_xlim(neg_bndry - xlim, pos_bndry + xlim)

    minor_locator = AutoMinorLocator(5)
    ax_test.xaxis.set_minor_locator(minor_locator)
    plt.grid(True, 'both')
    plt.grid(True, 'minor', ls='--', lw=1)

    # Find and show if there is a value even lower than the selected boundary
    if y_pos.min() < y_neg.min():
        ax_test.axvline(x_pos[y_pos_min], color=sns.xkcd_rgb['pale red'], lw=1)
        ax_test.text(x_pos[y_pos_min] + 0.02, y_pos.min() + 0.5,
                     x_pos[y_pos_min])
    elif y_pos.min() > y_neg.min():
        ax_test.text(x_neg[y_neg_min] + 0.02, y_neg.min() + 0.5,
                     x_neg[y_neg_min])
        ax_test.axvline(x_neg[y_neg_min], color=sns.xkcd_rgb['pale red'], lw=1)

    ax_test.set_title('Cut of BSweep: {}'.format(key))
    ax_test.set_xlabel('{k} {u}'.format(k=b_key, u=xunit))
    ax_test.set_ylabel('{k} {u}'.format(k=r_key, u=yunit))

    fig_test.tight_layout()

    return fig_test, ax_test


def plot_bound_data(plot_df, meas_name, bounds, fit_bounds):
    """Plot data within the boundaries

    Parameters
    ----------
    plot_df : pandas.DataFrame
        The DataFrame with the data to be plotted
    meas_name : string
        The name or key of the measurement
    bounds : tuple
        Tuple of floats with the inner data boundaries (i.e. closest to zero)
    fit_bounds : scalar-like
        The outer boundary fo the data

    Returns
    -------
    plot_fig, plot_ax : plotted figure for viewing and saving

    """

    if 'B' in plot_df:
        b_key = 'B'
        xunit = '[T]'
    elif 'xMagnet' in plot_df:
        b_key = 'xMagnet'
        xunit = '[T]'
    elif 'BzField' in plot_df:
        b_key = 'BzField'
        xunit = '[T]'

    if 'dR' in plot_df:
        r_key = 'dR'
        yunit = '[$\Omega$]'
    elif 'dRSample' in plot_df:
        r_key = 'dRSample'
        yunit = '[$\Omega$]'

    fig01, ax01 = plt.subplots(ncols=2, sharex=False, sharey=True)

    # Get the upper negative branch boundary
    neg_boundary = bounds[0]
    # Get the lower positive branch boundary
    pos_boundary = bounds[1]

    # Get the lower negative branch boundary
    fit_boundary_neg = fit_bounds * -1
    # Get the upper positive branch boundary
    fit_boundary_pos = fit_bounds

    # print(meas_name,
    #       '\n\tneg:', fit_boundary_neg, neg_boundary,
    #       '\tpos:', pos_boundary, fit_boundary_pos)

    # Grab the data inside the outer boundaries
    temp_df = plot_df[(plot_df[b_key] > fit_boundary_neg) &
                      (plot_df[b_key] < fit_boundary_pos)]

    # Grab the negative branch data
    x_neg = temp_df[b_key][temp_df[b_key] < neg_boundary]
    y_neg = temp_df[r_key][temp_df[b_key] < neg_boundary]

    # Grab the positive branch data
    x_pos = temp_df[b_key][temp_df[b_key] > pos_boundary]
    y_pos = temp_df[r_key][temp_df[b_key] > pos_boundary]

    # Plot both branches, each in its own subplot
    ax01[0].plot(x_neg[::10], y_neg[::10])
    ax01[1].plot(x_pos[::10], y_pos[::10])

    # Automatically scale the view to the data
    ax01[0].autoscale_view(True, True, True)
    ax01[1].autoscale_view(True, True, True)

    # Create the minor grid inside the plot
    minor_locator01 = AutoMinorLocator(5)
    minor_locator02 = AutoMinorLocator(5)

    ax01[0].xaxis.set_minor_locator(minor_locator01)
    ax01[1].xaxis.set_minor_locator(minor_locator02)

    ax01[0].grid(True, 'minor', ls='--', lw=1)
    ax01[1].grid(True, 'minor', ls='--', lw=1)

    # Add the titles and labels
    ax01[0].set_title('Neg BSweeps {}'.format(meas_name))
    ax01[1].set_title('Pos BSweeps {}'.format(meas_name))

    ax01[0].set_xlabel('{k} {u}'.format(k=b_key, u=xunit))
    ax01[1].set_xlabel('{k} {u}'.format(k=b_key, u=xunit))

    ax01[0].set_ylabel('{k} {u}'.format(k=r_key, u=yunit))

    fig01.tight_layout()

    return fig01, ax01


def generate_poly_fits(measurement, key, fit_bound=(-10, 10),
                       bounds=(0.0, 0.0), deg=1):
    """Generate poly fits of the negative and positive data

    Parameters
    ----------
    measurement : dict
        The data frame to modify
    key : str
        The name of the measurement
    fit_bound : tuple with two scalars, optional
        The upper boundary of the absolute value of the data to be fit
    bounds : tuple with two floats, optional
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.
    deg : int
        The degree of the polynomial that should be used to fit the data

    Return
    ------
    fig_test, ax_test : matplotlib figure and axis
        The figure and axis showing the data
    new_measurement : dict
        The interpolated data, ready for use in the fft
    """

    lim = 100

    if 'dR' in measurement:
        r_key = 'dR'
        yunit = r'[$\Omega$]'

    if 'B' in measurement:
        b_key = 'B'
        xunit = '[mT]'
    elif 'BField' in measurement:
        b_key = 'BField'
        xunit = '[mT]'
    elif 'BzField' in measurement:
        b_key = 'BzField'
        xunit = '[mT]'
    elif r'$\Phi / \Phi_0$' in measurement:
        b_key = r'$\Phi / \Phi_0$'
        xunit = ''
    else:
        print('Could not find a valid channel for representing the magnet'
              ' field. Please try something else.')

    # Define the boundaries
    neg_bndry = bounds[0]
    pos_bndry = bounds[1]

    fit_bound_neg = fit_bound * -1
    fit_bound_pos = fit_bound

    # Get the positive and negative dataframes
    temp_neg = measurement[(measurement[b_key] >= fit_bound_neg) &
                           (measurement[b_key] <= neg_bndry)]
    temp_pos = measurement[(measurement[b_key] >= pos_bndry) &
                           (measurement[b_key] <= fit_bound_pos)]

    # Get the x channels
    x_pos = temp_pos[b_key]
    x_neg = temp_neg[b_key]
    x = measurement[b_key][(measurement[b_key] >= fit_bound_neg) &
                           (measurement[b_key] <= fit_bound_pos)]

    # Find next power of two of each channel's length
    idx_pos = nextpow2(len(x_pos))
    idx_neg = nextpow2(len(x_neg))
    idx = nextpow2(len(x))

    # Linearly inerpolate the x channels (we are assuming here an monotonically
    # increasing function, i.e. strictly linear!)
    new_x_pos = np.linspace(np.min(x_pos), np.max(x_pos), idx_pos)
    new_x_neg = np.linspace(np.min(x_neg), np.max(x_neg), idx_neg)
    new_x = np.linspace(np.min(x), np.max(x), idx)

    # Setup the new data frame
    new_measurement = {}
    new_measurement['mods'] = []
    new_measurement['modified'] = True
    new_measurement['negFFT'] = pd.DataFrame({'IntBField': new_x_neg})
    new_measurement['FFT'] = pd.DataFrame({'IntBField': new_x})
    new_measurement['posFFT'] = pd.DataFrame({'IntBField': new_x_pos})

    # Setup the plot
    fig_poly_fit, ax_poly_fit = plt.subplots(nrows=2, ncols=3, sharex='col',
                                             sharey='row')

    # Get the y channels
    y_pos = temp_pos[r_key]
    y = measurement[r_key][(measurement[b_key] >= fit_bound_neg) &
                           (measurement[b_key] <= fit_bound_pos)]
    y_neg = temp_neg[r_key]

    # Linearly interpolate the y channels and generate
    # print('Doing linear interpolation of x')
    f_pos = interp1d(x_pos, y_pos, 'linear')
    f = interp1d(x, y, 'linear')
    f_neg = interp1d(x_neg, y_neg, 'linear')

    new_y_pos = f_pos(new_x_pos)
    new_y = f(new_x)
    new_y_neg = f_neg(new_x_neg)

    # Sometimes a nan sneaks in and the rest of the analysis does not
    # work. Catch that here.
    if np.isnan(new_y_pos).any():
        # print(key, 'Nan in new_y_pos')
        new_y_pos = np.nan_to_num(new_y_pos)
        new_y_pos[new_y_pos == 0] = new_y_pos[new_y_pos != 0].min()

    if np.isnan(new_y_neg).any():
        # print(key, 'Nan in new_y_neg')
        new_y_neg = np.nan_to_num(new_y_neg)
        new_y_neg[new_y_neg == 0] = new_y_neg[new_y_neg != 0].max()

    if np.isnan(new_y).any():
        # print(key, 'Nan in new_y')
        new_y = np.nan_to_num(new_y)
        new_y[new_y == 0] = new_y[new_y != 0].max()

    # Do the polynomial fit
    # print('Doing polynomial fit')
    pos_poly_params = np.polyfit(x_pos, y_pos, deg)
    neg_poly_params = np.polyfit(x_neg, y_neg, deg)
    poly_params = np.polyfit(x, y, deg)

    pos_poly_func = np.poly1d(pos_poly_params)
    neg_poly_func = np.poly1d(neg_poly_params)
    poly_func = np.poly1d(poly_params)

    new_pos_poly_params = np.polyfit(new_x_pos, new_y_pos, deg)
    new_neg_poly_params = np.polyfit(new_x_neg, new_y_neg, deg)
    new_poly_params = np.polyfit(new_x, new_y, deg)

    new_pos_poly_func = np.poly1d(new_pos_poly_params)
    new_neg_poly_func = np.poly1d(new_neg_poly_params)
    new_poly_func = np.poly1d(new_poly_params)

    # Generate the fit data, i.e. the background
    y_pos_poly_fit = pos_poly_func(x_pos)
    y_neg_poly_fit = neg_poly_func(x_neg)
    y_poly_fit = poly_func(x)

    new_y_pos_poly_fit = new_pos_poly_func(new_x_pos)
    new_y_neg_poly_fit = new_neg_poly_func(new_x_neg)
    new_y_poly_fit = new_poly_func(new_x)

    # Generate the background reduced data by subtracting the polynomial
    #  fit from the linearly interpolated data
    y_pos_reduced = y_pos - y_pos_poly_fit
    y_neg_reduced = y_neg - y_neg_poly_fit
    y_reduced = y - y_poly_fit

    new_y_pos_reduced = new_y_pos - new_y_pos_poly_fit
    new_y_neg_reduced = new_y_neg - new_y_neg_poly_fit
    new_y_reduced = new_y - new_y_poly_fit

    if 'dR/dR_N' in r_key:
        r_name = 'dRn'
    elif 'dR' in r_key:
        r_name = 'dR'
    elif 'R' in r_key:
        r_name = 'R'

    int_r_key = 'Int_{}'.format(r_name)
    back_r_key = 'Back_{}'.format(r_name)
    red_r_key = 'Red_{}'.format(r_name)
    # print(int_r_key, back_r_key, red_r_key)

    new_measurement['negFFT'][int_r_key] = new_y_neg
    new_measurement['negFFT'][back_r_key] = new_y_neg - \
        new_y_neg_reduced
    new_measurement['negFFT'][red_r_key] = new_y_neg_reduced
    new_measurement['mods'].append('Adding interpolated and background'
                                   '-reduced negative branch of sweep for'
                                   ' FFT.')

    new_measurement['FFT'][int_r_key] = new_y
    new_measurement['FFT'][back_r_key] = new_y - \
        new_y_reduced
    new_measurement['FFT'][red_r_key] = new_y_reduced
    new_measurement['mods'].append('Adding interpolated and background'
                                   '-reduced sweep for FFT.')

    new_measurement['posFFT'][int_r_key] = new_y_pos
    new_measurement['posFFT'][back_r_key] = new_y_pos - \
        new_y_pos_reduced
    new_measurement['posFFT'][red_r_key] = new_y_pos_reduced
    new_measurement['mods'].append('Adding interpolated and background'
                                   '-reduced positive branch of sweep for'
                                   ' FFT.')

    # print('Plotting')
    # Interpolated Data
    ax_poly_fit[0][0].plot(new_x_neg[::10], new_y_neg[::10],
                           label=r_key + ' int.')
    ax_poly_fit[0][1].plot(new_x_pos[::10], new_y_pos[::10],
                           label=r_key + ' int.')

    ax_poly_fit[0][2].plot(new_x[::10], new_y[::10],
                           label=r_key + ' int.')

    ax_poly_fit[0][0].plot(x_neg[::10], y_neg[::10],
                           label=r_key + ' raw')
    ax_poly_fit[0][1].plot(x_pos[::10], y_pos[::10],
                           label=r_key + ' raw')

    ax_poly_fit[0][2].plot(x[::10], y[::10],
                           label=r_key + ' raw')

    #  Fit Data
    ax_poly_fit[0][0].plot(new_x_neg[::10], new_y_neg_poly_fit[::10],
                           label=r_key + ' bckgrnd int.')
    ax_poly_fit[0][1].plot(new_x_pos[::10], new_y_pos_poly_fit[::10],
                           label=r_key + ' bckgrnd int.')

    ax_poly_fit[0][2].plot(new_x[::10], new_y_poly_fit[::10],
                           label=r_key + ' bckgrnd raw')

    ax_poly_fit[0][0].plot(x_neg[::10], y_neg_poly_fit[::10],
                           label=r_key + ' bckgrnd raw')
    ax_poly_fit[0][1].plot(x_pos[::10], y_pos_poly_fit[::10],
                           label=r_key + ' bckgrnd raw')

    ax_poly_fit[0][2].plot(x[::10], y_poly_fit[::10],
                           label=r_key + ' bckgrnd raw')

    # Full Range Data

    # Reduced data
    ax_poly_fit[1][0].plot(new_x_neg[np.abs(new_y_neg_reduced) <= lim][::10],
                           new_y_neg_reduced[np.abs(new_y_neg_reduced) <= lim][::10],
                           label=r_key + ' red int.')
    ax_poly_fit[1][1].plot(new_x_pos[np.abs(new_y_pos_reduced) <= lim][::10],
                           new_y_pos_reduced[np.abs(new_y_pos_reduced) <= lim][::10],
                           label=r_key + ' red int.')

    ax_poly_fit[1][2].plot(new_x[np.abs(new_y_reduced) <= lim][::10],
                           new_y_reduced[np.abs(new_y_reduced) <= lim][::10],
                           label=r_key + ' red int.')

    ax_poly_fit[1][0].plot(x_neg[np.abs(y_neg_reduced) <= lim][::10],
                           y_neg_reduced[np.abs(y_neg_reduced) <= lim][::10],
                           label=r_key + ' red raw')
    ax_poly_fit[1][1].plot(x_pos[np.abs(y_pos_reduced) <= lim][::10],
                           y_pos_reduced[np.abs(y_pos_reduced) <= lim][::10],
                           label=r_key + ' red raw')

    ax_poly_fit[1][2].plot(x[np.abs(y_reduced) <= lim][::10],
                           y_reduced[np.abs(y_reduced) <= lim][::10],
                           label=r_key + ' red raw')
    # print('Finished plotting')

    ax_poly_fit[0][0].set_title('Negative')
    ax_poly_fit[0][1].set_title('{k}; {d:d}-deg\nPositive'.format(k=key,
                                                                  d=deg))
    ax_poly_fit[0][2].set_title('Whole')

    ax_poly_fit[0][2].legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
    ax_poly_fit[1][2].legend(loc='center left', bbox_to_anchor=(1.0, 0.5))

    ax_poly_fit[0][0].set_ylabel('$R_s$ {}'.format(yunit))
    ax_poly_fit[1][0].set_ylabel('Bkgnd Reduced\n$R_s$ {}'
                                 .format(yunit))

    ax_poly_fit[1][0].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))
    ax_poly_fit[1][1].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))
    ax_poly_fit[1][2].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))

    ax_poly_fit[1][0].set_xlim(new_x_neg.min(), new_x_neg.max())
    ax_poly_fit[1][1].set_xlim(new_x_pos.min(), new_x_pos.max())
    ax_poly_fit[1][2].set_xlim(new_x.min(), new_x.max())

    fig_poly_fit.subplots_adjust(wspace=0.05, hspace=0.05)

    return fig_poly_fit, ax_poly_fit, new_measurement


def generate_sg_fits(measurement, key, bounds=(0.0, 0.0), sg_wins=(1, 1, 1)):
    """Generate tanh fits of the negative and positive data

    Parameters
    ----------
    measurement : dict
        The data frame to modify
    key : str
        The name of the measurement
    bounds : tuple with two floats, optional
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.
    init_params : tuple of two four-element lists
        The initial parameters for performing the tanh fit.
        DEFAULT: ([1, 1, 1, 1], [1, 1, 1, 1])

    Return
    ------
    fig_test, ax_test : matplotlib figure and axis
        The figure and axis showing the data
    new_measurement : dict
        The interpolated data, ready for use in the fft
    """
    #print(key)

    if 'dR' in measurement:
        r_key = 'dR'

    yunit = r'[$\Omega$]'

    if 'B' in measurement:
        b_key = 'B'
        xunit = '[mT]'
    elif '$\Phi / \Phi_0$' in measurement:
        b_key = '$\Phi / \Phi_0$'
        xunit = ''
    elif 'BField' in measurement:
        b_key = 'BField'
        xunit = '[mT]'
    elif 'BzField' in measurement:
        b_key = 'BzField'
        xunit = '[mT]'
    else:
        print('Could not find a valid channel for representing the magnet'
              ' field. Please try something else.')

    if 'Phi_0' in b_key:
        b_name = 'Int_n'
    elif 'Phi' in b_key:
        b_name = 'IntPhi'
    elif 'B' in b_key:
        b_name = 'IntBField'

    # Define the boundaries
    neg_bndry = bounds[0]
    pos_bndry = bounds[1]

    # Get the positive and negative dataframes
    temp_neg = measurement[measurement['B'] < neg_bndry]
    temp_pos = measurement[measurement['B'] > pos_bndry]

    # Get the x channels
    x_neg = temp_neg[b_key]
    x_pos = temp_pos[b_key]
    x = measurement[b_key][(measurement['B'] < neg_bndry) |
                           (measurement['B'] > pos_bndry)]

    # Find next power of two of each channel's length
    idx_neg = nextpow2(len(x_neg))
    idx_pos = nextpow2(len(x_pos))
    idx = nextpow2(len(x))

    # Linearly inerpolate the x channels (we are assuming here an monotonically
    # increasing function, i.e. strictly linear!)
    new_x_neg = np.linspace(np.min(x_neg), np.max(x_neg), idx_neg)
    new_x_pos = np.linspace(np.min(x_pos), np.max(x_pos), idx_pos)
    new_x = np.linspace(np.min(x), np.max(x), idx)

    # Setup the new data frame
    new_measurement = {}
    new_measurement['mods'] = []
    new_measurement['modified'] = True
    new_measurement['negFFT'] = pd.DataFrame({b_name: new_x_neg})
    new_measurement['posFFT'] = pd.DataFrame({b_name: new_x_pos})
    new_measurement['FFT'] = pd.DataFrame({b_name: new_x})

    # Get the y channels
    y_neg = temp_neg[r_key]
    y_pos = temp_pos[r_key]
    y = measurement[r_key][(measurement['B'] < neg_bndry) |
                           (measurement['B'] > pos_bndry)]

    # Linearly interpolate the y channels and generate
    # print('Doing linear interpolation of x')
    f_neg = interp1d(x_neg, y_neg, 'linear')
    f_pos = interp1d(x_pos, y_pos, 'linear')
    f = interp1d(x, y, 'linear')

    new_y_neg = f_neg(new_x_neg)
    new_y_pos = f_pos(new_x_pos)
    new_y = f(new_x)

    # Sometimes a nan sneaks in and the rest of the analysis does not
    # work. Catch that here.
    if np.isnan(new_y_neg).any():
        #print(key, 'Nan in new_y_neg')
        new_y_neg = np.nan_to_num(new_y_neg)
        new_y_neg[new_y_neg == 0] = new_y_neg[new_y_neg != 0].max()

    if np.isnan(new_y_pos).any():
        #print(key, 'Nan in new_y_pos')
        new_y_pos = np.nan_to_num(new_y_pos)
        new_y_pos[new_y_pos == 0] = new_y_pos[new_y_pos != 0].min()

    if np.isnan(new_y).any():
        #print(key, 'Nan in new_y')
        new_y = np.nan_to_num(new_y)
        new_y[new_y == 0] = new_y[new_y != 0].max()

    # Do the SG smoothing
    # print('Doing SG Smoothing')
    (win_neg, win_pos, win) = sg_wins
    order = 3

    new_ys_neg = savitzky_golay(new_y_neg, win_neg, order)
    new_ys_pos = savitzky_golay(new_y_pos, win_pos, order)
    # new_ys = savitzky_golay(new_y, win, order)

    # Generate the background reduced data by subtracting the univariate
    #  spline from the linearly interpolated data
    # print('Calculating reduced background')
    new_y_neg_reduced = new_y_neg - new_ys_neg
    new_y_pos_reduced = new_y_pos - new_ys_pos
    # new_y_reduced = new_y - new_ys

    if 'dR/dR_N' in r_key:
        r_name = 'dRn'
    elif 'dR' in r_key:
        r_name = 'dR'
    elif 'R' in r_key:
        r_name = 'R'

    int_r_key = 'Int{}'.format(r_name)
    back_r_key = 'Back{}'.format(r_name)
    red_r_key = 'Red{}'.format(r_name)
    # print(int_r_key, back_r_key, red_r_key)

    new_measurement['negFFT'][int_r_key] = new_y_neg
    new_measurement['negFFT'][back_r_key] = new_y_neg - \
        new_y_neg_reduced
    new_measurement['negFFT'][red_r_key] = new_y_neg_reduced
    new_measurement['mods'].append('Adding interpolated and background'
                                   '-reduced negative branch of sweep for'
                                   ' FFT.')

    new_measurement['posFFT'][int_r_key] = new_y_pos
    new_measurement['posFFT'][back_r_key] = new_y_pos - \
        new_y_pos_reduced
    new_measurement['posFFT'][red_r_key] = new_y_pos_reduced
    new_measurement['mods'].append('Adding interpolated and background'
                                   '-reduced positive branch of sweep for'
                                   ' FFT.')

    # Setup the plot
    fig_sgfit, ax_sgfit = plt.subplots(nrows=2, ncols=2, sharex='col', sharey='row')

    #print('Plotting')
    # Interpolated Data
    ax_sgfit[0][0].plot(new_x_neg[::10], new_y_neg[::10],
                        label=r_key)
    ax_sgfit[0][1].plot(new_x_pos[::10], new_y_pos[::10],
                        label=r_key)
    # ax_sgfit[0][2].plot(new_x, new_y, label=r_key)

    # Spline Smoothed Data
    ax_sgfit[0][0].plot(new_x_neg[::10],
                        (new_y_neg - new_y_neg_reduced)[::10],
                        color=sns.xkcd_rgb['medium green'],
                        label='win = {w}\norder = {o}\nlen = {l}'
                        .format(w=win_neg, o=order, l=new_y_neg.size))
    ax_sgfit[0][1].plot(new_x_pos[::10],
                        (new_y_pos - new_y_pos_reduced)[::10],
                        color=sns.xkcd_rgb['medium green'],
                        label='win = {w}\norder = {o}\nlen = {l}'
                        .format(w=win_pos, o=order, l=new_y_neg.size))

    ax_sgfit[0][0].legend(loc='best')
    ax_sgfit[0][1].legend(loc='best')

    # Reduced data
    ax_sgfit[1][0].plot(new_x_neg[::10], new_y_neg_reduced[::10],
                        label=r_key)
    ax_sgfit[1][1].plot(new_x_pos[::10], new_y_pos_reduced[::10],
                        label=r_key)

    #print('Finished plotting')

    ax_sgfit[0][0].set_title('Negative')
    ax_sgfit[0][1].set_title('Positive')

    ax_sgfit[0][0].set_ylabel('Interpolated Data\n$R_s$ {}'
                              .format(yunit))
    ax_sgfit[1][0].set_ylabel('Bkgnd Reduced\n$R_s$ {}'
                              .format(yunit))

    ax_sgfit[1][0].set_xlabel(r'{k} {u}'.format(k=r_key, u=xunit))
    ax_sgfit[1][1].set_xlabel(r'{k} {u}'.format(k=r_key, u=xunit))

    fig_sgfit.tight_layout()

    return fig_sgfit, ax_sgfit, new_measurement
