import numpy as np
import sys

from Constants import PHI_0


def peakdet(a, delta, index=None):
    """Find local extrema of a noisy singal

    blah

    Parameters
    ----------
    a : numpy.ndarray, pandas.TimeSeries
        The array which is to be examined for peaks
    delta : float
        The threshold above (below) which the value has to be, to be considered
        a peak (valley).
    index : numpy.ndarray, pandas.TimeSeries.index (optional)
        The original array's x-axis, or TimeSeries/DataFrame's index

    Returns
    -------
    peaks : numpy.ndarray, pandas.TimeSeries
        An array containing the
    valleys : numpy.ndarray, pandas.TimeSeries

    References
    ----------
    [*] https://gist.github.com/sixtenbe/1178136
    [1] https://gist.github.com/endolith/250860
    [2] http://billauer.co.il/peakdet.html

    """

    if index is None:
        index = np.arange(len(a))

    a = np.asarray(a)

    if len(a) != len(index):
        sys.exit('Input vectors a and index must have the same length.')

    if not np.isscalar(delta):
        sys.exit('Input argument delta must be a scalar')

    if delta <= 0:
        sys.exit('Input argument delta must be positive')

    min_, max_ = np.Inf, -np.Inf

    lookformax = True

    it = np.nditer(a, flags=['f_index'])

    while not it.finished:

        val = it[0]

        if val > max_:
            max_ = val

        if val < min_:
            min_ = val

        if lookformax:
            if val < max_ - delta:
                pass
        it.iternext()


def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    """Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.

    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute
        (default = 0 means only smoothing)

    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).

    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.

    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()

    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    import numpy as np
    from math import factorial

    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size - 1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window,
                                                           half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs(y[1:half_window+1][::-1] - y[0])
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve(m[::-1], y, mode='valid')


def normalize_resistances(old_df, factor=None, dfactor=None, offset=0,
                              doffset=0, overwrite=False):
    """Normalize a resistance channel, removing the offset, if any

    Parameters
    ----------
    old_df : dict
        The data frame to modify
    factor : float
        The normalization factor for absolute resistances
    factor : float
        The normalization factor for differential resistances
    offset : float, optional
        The absolute offet of the absolute resistance
    offset : float, optional
        The absolute offet of the differential resistance
    overwrite : boolean, optional
        A switch weather to overwrite the data in a dataframe that was already
       processed

    Returns
    -------
    old_df : dict
        The old data frame with the newly calculated and added channels

    """
    # Remove the offset from the normalization factors
    if factor:
        fac = factor - offset
    else:
        fac = None

    if dfactor:
        dfac = dfactor - doffset
    else:
        dfac = None

    for key in old_df.keys():
        temp_df = gen_temp_df(old_df[key])

        if 'R' in temp_df.keys():
            rkey = 'R'
        elif 'RSample' in temp_df.keys():
            rkey = 'RSample'
        else:
            rkey = None

        if 'dR' in temp_df.keys():
            drkey = 'dR'
        elif 'dRSample' in temp_df.keys():
            drkey = 'dRSample'
        else:
            drkey = None

        # Absolute resistance
        if ('$R/R_N$' not in old_df[key][res_dev] or overwrite) and \
                rkey in old_df[key][res_dev]:

            # if offset:
            #     # Remove the offset
            #     old_df[key][res_dev][rkey] -= offset
            #     old_df[key]['mods'].append('Removing R offset of {:.2f} Ohm'
            #                                .format(offset) + ' , i.e. cable'
            #                                ' resistance (2-point).')

            # Normalize the resistance
            if fac:
                old_df[key][res_dev]['$R/R_N$'] = old_df[key][res_dev][rkey]\
                    / fac
                old_df[key]['mods'].append('Normalizing resistance to '
                                           '{f1:.2f} Ohm - {o:.2f} Ohm = {f2:.2f}'
                                           .format(f1=factor, o=offset, f2=fac) +
                                           ' Ohm')

                # Change the modified flag to true
                old_df[key]['modified'] = True

        # Differential resistance
        if ('$dR/dR_N$' not in old_df[key][res_dev] or overwrite) and \
                drkey in old_df[key][res_dev]:

            # if doffset:
            #     # Remove the offset
            #     old_df[key][res_dev][drkey] -= doffset
            #     old_df[key]['mods'].append('Removing dR offset of {:.2f} Ohm'
            #                                .format(doffset) + ' , i.e. cable'
            #                                ' resistance (2-point).')

            # Normalize the resistance
            if dfac:
                old_df[key][res_dev]['$dR/dR_N$'] = (old_df[key][res_dev][drkey]
                    - doffset) / dfac
                old_df[key]['mods'].append('Normalizing differential resistance'
                                           ' to {f1:.2f} Ohm - {o:.2f} Ohm = {f2:.2f} Ohm'
                                           .format(f1=dfactor, o=doffset, f2=dfac))

                # Change the modified flag to true
                old_df[key]['modified'] = True

    return old_df


def calculate_flux_quanta(old_df, res_dev, radius, factor=1, overwrite=False):
    """Calculate the flux and number of flux quanta through a small ring

    Parameters
    ----------
    old_df : dict
        The data frame to modify
    res_dev : string
        The name of the device with which the sample resistance was recoreded
    radius : float
        The designed radius of the small rings in nm
    factor : float, optional
        Factor to convert T to mT
    overwrite : boolean, optional
        A switch weather to overwrite the data in a dataframe that was already
       processed

    Returns
    -------
    old_df : dict
        The data frame with the newly calculated and added channels

    """
    for key in sorted(old_df.keys()):
        if '$\Phi$' not in old_df[key][res_dev] or overwrite:

            if 'B' in old_df[key][res_dev].keys():
                b_key = 'B'
            elif 'BField' in old_df[key][res_dev].keys():
                b_key = 'BField'
            elif 'BzField' in old_df[key][res_dev].keys():
                b_key = 'BzField'
            elif 'IntBField' in old_df[key][res_dev].keys():
                b_key = 'IntBField'
            elif 'IntB' in old_df[key][res_dev].keys():
                b_key = 'IntB'
            elif 'Int_B' in old_df[key][res_dev].keys():
                b_key = 'Int_B'
            # Omri01 has xMagnet /and/ yMagnet, but yMagnet is garbage!
            elif 'xMagnet' in old_df[key][res_dev].keys():
                b_key = 'xMagnet'
            else:
                print('No known bfield key is in the dataset.')
                return

            # Calculate the flux though one small loop
            old_df[key][res_dev][r'$\Phi$'] = old_df[key][res_dev][b_key] *\
                factor * radius ** 2
            old_df[key]['mods'].append(r'Calculating and adding $\Phi$.'
                                       'r = {:.3f} nm'.format(radius))

            # Calculate the number of flux quanta through one small loop
            old_df[key][res_dev][r'$\frac{\Phi}{\Phi_0}$'] = \
                old_df[key][res_dev][r'$\Phi$'] / PHI_0
            old_df[key]['mods'].append(r'Calculating and adding'
                                       r' $\frac{\Phi}{\Phi_0}$.')
            old_df[key]['modified'] = True

    return old_df

def moving_average(a, n=3):
    """Return the unweighted moving average of an array.

    This calculates the unweighted moving average of an array and returns an
    array of the same length.

    Parameters
    ----------
    a : array-like
        The array to perform the calculation on
    n : integer
        The number of values on either side of any one value to take into
        account when calculating the average

    """

    it = np.nditer(a, flags=['f_index'])

    mas = []

    N = n+1

    a_size = a.shape[0]

    while not it.finished:
        # Get the index and calculate first and last indices for slice to
        # average over
        i = it.index
        k = i-n
        l = i+N

        # The slice for averaging is cenetered around the index. However, negative indices don't work
        # and return an empty slice. For this reason the lower window bound is either the index itself
        # or the window size given by the user
        if k <= 0:
            k = 0

        if l >= a_size:
            l = a_size-1

        avg_array = a[k:l]

        # Get the average of the slice

        avg = np.average(avg_array)

        mas.append(avg)
        it.iternext()

    mas = np.array(mas)
    
    return mas
