RES_DEVICE = 'all'

NORMAL_RESISTANCE = 6271.9
RESISTANCE_OFFSET = 307.6

RADIUS = 400
FACTOR = 1000 # Convertion factor for converting measurement into mT

BOUNDARIES = {
    '1067mK down 01': (-0.009693, 0.009047),
    '1071mK up 01': (-0.009693, 0.007754),
    '1132mK down 01': (-0.007108, 0.006462),
    '1137mK up 01': (-0.007754, 0.005816),
    '1147mK down 01': (-0.006462, 0.006462),
    '1156mK up 01': (-0.007108, 0.005170),
    }

SMOOTHING = {'1156mK up 01': (2**16, 2**16, 2**16),
             '1147mK down 01': (2**16, 2**16, 2**16),
             '1137mK up 01': (2**16, 2**16, 2**16),
             '1132mK down 01': (2**16, 2**16, 2**16),
             '1071mK up 01': (2**16, 2**16, 2**16),
             '1067mK down 01': (2**16, 2**16, 2**16)}
