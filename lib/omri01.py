BOUNDARIES = {'0300mK up 01': (-0, 0),
              '0350mK down 01': (0.0, 0.0),
              '0450mK down 01': (0.0, 0.0),
              '0500mK down 01': (0.0, 0.0),
              '0500mK up 01': (0.0, 0.0),
              '0550mK down 01': (0.0, 0.0),
              '0550mK down 02': (0.0, 0.0),
              '0550mK up 01': (0.0, 0.0),
              '0600mK down 01': (0.0, 0.0),
              '0650mK down 01': (0.0, 0.0),
              '0700mK up 01': (-0.0, -0.0)}

CUTOFFS = {'0300mK up 01': 1210,
           '0350mK down 01': None,
           '0450mK down 01': None,
           '0500mK down 01': 42.5,
           '0500mK up 01': None,
           '0550mK down 01': None,
           '0550mK down 02': None,
           '0550mK up 01': None,
           '0600mK down 01': 41,
           '0650mK down 01': None,
           '0700mK up 01': None,
           }

FIT_BOUNDARIES = {
    '0300mK up 01': 0.1270,
    '0350mK down 01': 0.1158,
    '0450mK down 01': 0.1092,
    '0500mK down 01': 0.0827,
    '0500mK up 01': 0.0463,
    '0550mK down 01': 0.0993,
    '0550mK down 02': 0.0910,
    '0550mK up 01': 0.0910,
    '0600mK down 01': 0.0827,
    '0650mK down 01': 0.1323,
    '0700mK up 01': 0.0993,
    }

RES_DEVICE = 'ADWin'

NORMAL_dRESISTANCE = 1825.130
dRESISTANCE_OFFSET = 0

NORMAL_RESISTANCE = 1741.4966
RESISTANCE_OFFSET = 0

RADIUS = 250
FACTOR = 1000 # Convertion factor for converting measurement into mT
