import datetime

NORMAL_dRESISTANCE = 23.33
dRESISTANCE_OFFSET = 0.007

RES_DEVICE = 'ADWin'

RADIUS = 324.07
FACTOR = 1000

#TC_JUMP = datetime.datetime(2014, 12, 10, 9, 37, 52)

# average magnet field offset in T
B_OFFSET = 0

B_OFFSETS = {
    '0327mK down 01': -0.00075,
    '0328mK up 01': -0.001,
    '0490mK down 01': -0.0005,
    '0490mK up 01': 0.0,
    '0491mK down 01': 0.0,
    '1001mK down 01': 0.0,
    '1001mK up 01': 0.0,
    '1249mK up 01': -0.0005,
    '1251mK down 01': -0.00075,
    '1290mK down 01': -0.00125,
    '1295mK down 01': -0.00075,
    '1302mK down 01': -0.0009,
    '1302mK up 01': 0.0,
    '1307mK down 01': -0.0015,
    '1308mK up 01': 0,
    '1311mK down 01': -0.0009,
    '1315mK down 01': -0.0011,
    '1315mK down 02': -0.0011,
    '1317mK down 01': -0.0011,
    '1320mK up 01': -0.0001,
    '1350mK up 01': -0.0001,
    '1359mK down 01': -0.001,
    }

BOUNDARIES = {
    '1480mK down 01': (0, 0),
    '1480mK down 02': (0, 0),
    '1485mK down 01': (0, 0),
    '1490mK down 01': (0, 0),
    '1495mK down 01': (0, 0),
    '1500mK down 01': (0, 0),
    '2540mK down 01': (0, 0),
    '11.62Ohm up 01': (-100, 100)
    }

FIT_BOUNDARIES = {
    '0320mK down 01': 0.18,
    '0320mK down 02': 0.18,
    '0320mK up 01': 0.18,
    '0320mK up 02': 0.18,
    '0400mK down 01': 0.18,
    '0500mK down 01': 0.18,
    '0500mK down 02': 0.18,
    '0750mK down 01': 0.18,
    '1000mK down 01': 0.18,
    '1000mK down 02': 0.18,
    '1000mK up 01': 0.18,
    '1260mK down 01': 0.18,
    '1260mK up 01': 0.18,
    '1300mK down 01': 0.18,
    '1305mK down 01': 0.18,
    '1315mK down 01': 0.18,
    '1315mK up 01': 0.18,
    '1320mK down 01': 0.18,
    '1325mK down 01': 0.18,
    '1330mK down 01': 0.18,
    '1335mK down 01': 0.18,
    '1335mK down 02': 0.18,
    '1345mK down 01': 0.18,
    '1360mK down 01': 0.18,
    '1395mK down 01': 0.18,
    '1400mK down 01': 0.18,
    '1405mK down 01': 0.18,
    '1410mK down 01': 0.18,
    '1415mK down 01': 0.18,
    '1420mK down 01': 0.18,
    '1425mK down 01': 0.18,
    '1430mK down 01': 0.18,
    '1435mK down 01': 0.18,
    '1440mK down 01': 0.18,
    '1445mK down 01': 0.18,
    '1450mK down 01': 0.18,
    '1455mK down 01': 0.18,
    '1460mK down 01': 0.18,
    '1465mK down 01': 0.18,
    '1470mK down 01': 0.18,
    '1470mK down 02': 0.18,
    '1475mK down 01': 0.18,
    '1480mK down 01': 0.18,
    '1480mK down 02': 0.18,
    '1485mK down 01': 0.18,
    '1490mK down 01': 0.18,
    '1495mK down 01': 0.18,
    '1500mK down 01': 0.18,
    '2540mK down 01': 0.18,
    '11.62Ohm up 01': 0.06
    }
