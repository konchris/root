# coding: utf-8
"""The Helper Module for setting up sample-specific generic IPython Notebooks

This module combines elements that are the same across all IPython Notebooks
for this sample.

"""
import os
import re
import pandas as pd
import h5py

from ChannelModel import EXPRT_CHANS

HOME = os.path.expanduser('~')
DATA_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'data')
RAW_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'raw-data')
RESULTS_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'results')
DEVICES = ['all', 'posFFT', 'negFFT', 'ADWin', 'ITC503', 'Lakeshore', 'IPS',
           'allFFT']
SWEEPS = ['bsweep', 'tsweep', 'ivsweep', 'bramp', 'new_bsweep']
EXPORTS = [c.split('/')[-1] for c in EXPRT_CHANS]


def load_sweep_data(sweep_type, sample_dir, sample_run, root='measurement'):
    """Load the data from the files listed in the file_list file_list_filename

    Parameters
    ----------
    sweep_type : {'bsweep', 'tsweep', 'ivsweep', 'bramp'}
        The type of the sweep measurement of the data you want to load.
    sample_dir : str
        A string containing the sample directory, in some cases the network
        subdirectory and the 'cryo_measurements' directory
    sample_run : str
        A string containing the start date of the sample run.
    root : str, optional
        The name of the data set to load from the file(s).
        The raw data is in 'proc01' and further processed data saved in roots
        with increasing numbers.
        DEFAULT: measurement

    Returns
    -------
    df : dict
        A dictionary where the top level is the measurement key (i.e.
        temperature and maybe sweep direction), followed by dictionaries
        sorted by device name, then each device's data in a pandas DataFrame.
        df = {}
        df['0327 mK up 01'] = {}
        df['0327 mK up 01']['ADWin'] = pandas.DataFrame()
        df['0327 mK up 01']['filename'] =
                '2015-03-26T17-09-31_BSweep_327mK_+300mT_1mT'
        df['0327 mK up 01']['modified'] = False
        df['0327 mK up 01']['mods'] = ['Adding normalized resistance']

    """
    assert sweep_type in SWEEPS, (sweep_type + ' is not a valid sweep. Valid '
                                  'sweep types are: ' + ', '.join(SWEEPS))

    # Generate the filename of the file that has the list of the good data
    # files
    file_list_filename = '{}_files.csv'.format(sweep_type)
    full_path = os.path.join(DATA_DIR, sample_dir, sample_run,
                             file_list_filename)

    assert os.path.exists(full_path), '{fp} does not exist.'.format(fp=full_path)

    # Sometimes value errors were raised. I cannot remember anymore why.
    try:
        file_lists = pd.DataFrame.from_csv(full_path)
    except ValueError:
        print('ValueError raised for {}'.format(file_list_filename))

    # Initialize the empty data dictionary
    df = {}

    # Go through each file in the list and load its data
    for fname in file_lists['file name']:
        # print('Loading:', fname)

        # Generate the unique dataframe key for the data based on the
        # filename
        if sweep_type in ['bsweep', 'bramp', 'new_bsweep']:
            try:
                (datetime_stamp, meas_type_unused, meas_temp, meas_end,
                 meas_rate) = re.split(r'[_.]', fname)
            except ValueError:
                (datetime_stamp, meas_type_unused, meas_temp, meas_end,
                 meas_rate01, meas_rate02) = re.split(r'[_.]', fname)
                meas_rate = '.'.join((meas_rate01, meas_rate02))

            # Average temperature of measurement
            try:
                meas_temp = int(meas_temp.rstrip('mK'))
                meas_unit = 'mK'
            except ValueError:
                meas_temp = float(meas_temp.rstrip('mOh')) / 1000
                meas_unit = 'Ohm'

            # Figure out the direction of the magnet field sweep
            if int(meas_end.rstrip('mT')) < 0:
                meas_direction = 'down '
            elif int(meas_end.rstrip('mT')) > 0:
                meas_direction = 'up '

            if sweep_type == 'bramp':
                meas_direction = ''

            # Generate the dataframe key: count how many instances of a
            # temperature already are present, then add one
            j = 1
            for x in df.keys():
                # if TTTTUU in a key present in df, then add one to j
                if '{t:04}{u} {d}'.format(t=meas_temp, u=meas_unit,
                                          d=meas_direction) in x:
                        # print('\t1:', device)
                        j += 1

            key = '{t:04}{u} {d}{n:02}'.format(t=meas_temp,
                                               u=meas_unit,
                                               d=meas_direction,
                                               n=j)

            # print('\nFinal key is:', key)
        elif sweep_type == 'tsweep':

            # Split the filename up into components and generate key
            keys = re.split(r'[_.]', fname)
            # datetime_stamp = keys[0]
            meas_type = keys[1]
            if 'Base' in meas_type:
                meas_temp = ' base'
            elif 'Warm' in meas_type:
                meas_temp = ' RT'
            elif 'Condense' in meas_type or 'Cooldown' in meas_type:
                meas_temp = ''
            else:
                meas_temp = ' ' + keys[2]

            j = 1

            for x in df.keys():
                # print('\t' 'x:', x,
                #       '{m}{t}:', '{m}{t}'.format(m=meas_type,
                #                                  t=meas_temp),
                #       '{m}{t}'.format(m=meas_type, t=meas_temp) in x)
                if '{m}{t}'.format(m=meas_type, t=meas_temp, n=j) in x:
                    # print('\t2', device, j)
                    j += 1

            key = '{m}{t} {n:02}'.format(m=meas_type, t=meas_temp,
                                         n=j)
            # print('\t', key in df)

        elif sweep_type == 'ivsweep':
            keys = re.split(r'[_.]', fname)

            meas_type = keys[1]
            meas_temp = int(keys[2].rstrip('mK'))

            j = 1

            key_attempt = '{t:04}mK {n:02}'.format(t=meas_temp, n=j)
            ## print(key_attempt)

            while key_attempt in df:
                j += 1
                key_attempt = '{t:04}mK {n:02}'.format(t=meas_temp, n=j)

            ## print(key_attempt)

            key = key_attempt

            # print('\tThe key:', key)

        full_path = os.path.join(DATA_DIR, sample_dir, sample_run,
                                 fname + '.h5')

        # Create the HDFStore object
        #hdf_store = pd.HDFStore(full_path)
        hdf5_file_object = h5py.File(full_path, 'a')

        for exprt_chan in EXPORTS:

            # Generate the hdf_key under which the device's data was stored
            hdf_key = '/{r}/{c}'.format(r=root, c=exprt_chan)

            # print('HDF key is', hdf_key, hdf_key in hdf5_file_object)

            if hdf_key in hdf5_file_object:
                # print('\thdf_key:', hdf_key)

                # Get the descriptions of the data processing modifications
                # performed on the data, if any

                try:
                    mods = hdf5_file_object[hdf_key].attrs.mods
                    # for k01 in hdf5_file_object.attrs: print(k01)
                except AttributeError:
                    mods = []

                # If the file's dataframe key is not present in the dataframe,
                # then initialize the dataframe structure.
                if key not in df:
                    # print('\t', 'Adding {} to the DataFrame.'.format(key))
                    df[key] = {}
                    df[key]['filename'] = fname
                    df[key]['modified'] = False
                    df[key]['mods'] = mods

                # If the device is not in the dataframe keys then the data for
                # that device has not been loaded yet.
                # print('\t', key)
                if exprt_chan not in df[key].keys():
                    temp_df = {}
                    # df[key][device] = pd.DataFrame(hdf5_file_object[hdf_key])
                    # print('\t3', device, key)
                    # try:
                    # for chan in hdf5_file_object[hdf_key].keys():
                    temp_df[exprt_chan] = hdf5_file_object[hdf_key][::]
                    # print(key, hdf_key, exprt_chan)
                    df[key][exprt_chan] = pd.DataFrame(temp_df)
                    # except KeyError:
                    #     pass
                else:
                    print('\tWARNING: {ec} is already present in df[{k}]'
                          .format(ec=exprt_chan, k=key))

        # print('\tfilename:', fname)
        # print('\tkey:', key)
        # print('\tdatetime_stamp', datetime_stamp)
        hdf5_file_object.close()
        # print(list(df[key].keys()))

    # for key in sorted(df.keys()):
    #     print(key)
    #     print('\t', sorted(df[key].keys()))

    return df


def delete_data(df, sample_dir, sample_run, root='proc03'):
    """Delete a branch out of the h5 file

    Parameters
    ----------
    df : dict
        A dictionary where the top level is the measurement key (i.e.
        temperature and maybe sweep direction), followed by dictionaries
        sorted by device name, then each device's data in a pandas DataFrame.
        df = {}
        df['0327mK up 01'] = {}
        df['0327mK up 01']['ADWin'] = pandas.DataFrame()
        df['0327mK up 01']['filename'] =
                '2015-03-26T17-09-31_BSweep_327mK_+300mT_1mT'
    sample_dir : str
    sample_run : str
    root : str
        The name of the root folder in the HDF5 file structure to place the
        data.
        DEFAULT 'proc01'
    overwrite : boolean
        If you want to overwrite a processed dataset in the file. This will
        overwrite all devices' data in the file!
        DEFAULT False

    """

    for key in df.keys():
        filename = df[key]['filename'] + '.h5'
        full_path = os.path.join(DATA_DIR, sample_dir, sample_run,
                                 filename)

        hdf_store = pd.HDFStore(full_path)

        hdf_key = '/{}'.format(root)

        print(hdf_key, 'is in df:', hdf_key in hdf_store)

        if hdf_key in hdf_store:
            del hdf_store[hdf_key]
            # suc = True
        else:
            # print('Sorry, cannot find {k} in {f}'.format(k=hdf_key,
            #                                              f=filename))
            # suc = False
            pass

        # if suc:
        #     print('Successfully deleted {k} from {f}'.format(k=hdf_key,
        #                                                      f=filename))
        # else:
        #     print('Sorry, somethign went wrong while trying to delete'
        #           ' {k} from {f}'.format(k=hdf_key, f=filename))

        hdf_store.close()


def save_data(df, sample_dir, sample_run, root='proc01', overwrite=False):
    """Save the data from the dataset back into the files.

    Parameters
    ----------
    df : dict
        A dictionary where the top level is the measurement key (i.e.
        temperature and maybe sweep direction), followed by dictionaries
        sorted by device name, then each device's data in a pandas DataFrame.
        df = {}
        df['0327mK up 01'] = {}
        df['0327mK up 01']['ADWin'] = pandas.DataFrame()
        df['0327mK up 01']['filename'] =
                '2015-03-26T17-09-31_BSweep_327mK_+300mT_1mT'
    sample_dir : str
    sample_run : str
    root : str
        The name of the root folder in the HDF5 file structure to place the
        data.
        DEFAULT 'proc01'
    overwrite : boolean
        If you want to overwrite a processed dataset in the file. This will
        overwrite all devices' data in the file!
        DEFAULT False

    """
    # Generate the filename of the file that has the list of the good data
    # files
    for key in df.keys():
        if df[key]['modified']:
            filename = df[key]['filename'] + '.h5'
            full_path = os.path.join(DATA_DIR, sample_dir, sample_run,
                                     filename)
            # print('Saving {}'.format(filename))

            for device in DEVICES:

                if device in df[key]:

                    # Create the HDFStore object
                    # hdf_store = pd.HDFStore(full_path)
                    hdf5_file_object = h5py.File(full_path, 'a')

                    # Generate the key under which the device's data was stored
                    hdf_key = '/{r}/{d}'.format(r=root, d=device)

                    # If the hdf key is in the file
                    # if hdf_key in hdf_store and not overwrite:
                    if hdf_key in hdf5_file_object and not overwrite:
                        print('The key {k} already exists in the file {f}'
                              .format(k=hdf_key, f=os.path.basename(filename)))
                        print('***{k} not saved to {f}!***'
                              .format(k=hdf_key, f=os.path.basename(filename)))
                    # If the hdf key is not in the file, write the data to it
                    else:
                        try:
                            # del hdf_store[hdf_key]
                            del hdf5_file_object[hdf_key]
                            print('Deleting old {k} data from {f}.'
                                  .format(k=hdf_key,
                                          f=os.path.basename(filename)))
                        except KeyError:
                            pass
                        print('Writing {k} data to {f}.'
                              .format(k=hdf_key,
                                      f=os.path.basename(filename)))
                        print(hdf_key, key, device)
                        # hdf_store.append(hdf_key, df[key][device])

                        for chan in df[key][device]:
                            # print('\t', chan)
                            chan_key = '{hk}/{c}'.format(hk=hdf_key, c=chan)
                            chan_data = df[key][device][chan].values
                            hdf5_file_object.create_dataset(chan_key, data=chan_data)

                        # Add the modifications as attributes
                        # hdf_store.get_storer(hdf_key).attrs.mods = \
                        #     df[key]['mods']
                        hdf5_file_object[hdf_key].attrs.mods = df[key]['mods']
                        # mods = hdf_store.get_storer(hdf_key).attrs.mods
                        # for mod in mods:
                        #     print('\t', mod)

                    try:
                        del hdf5_file_object['/fft/FFT']
                        print('Deleted old FFT data')
                    except KeyError:
                        pass

                    hdf5_file_object.flush()
                    hdf5_file_object.close()
                    # hdf_store.close()
