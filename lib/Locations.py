import os

HOME = os.path.expanduser('~')
DATA_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'data')
RAW_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'raw-data')
RESULTS_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'results')
DOCS_DIR = os.path.join(HOME, 'Documents', 'PhD', 'root', 'doc')
PGF_DIR = os.path.join(HOME, 'Documents', 'PhD', 'Thesis', 'PGF')
