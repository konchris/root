def generate_poly_fits(measurement, key, fit_bound=(-10, 10),
                       bounds=(0.0, 0.0), deg=1):
    """Generate poly fits of the negative and positive data

    Parameters
    ----------
    measurement : dict
        The data frame to modify
    key : str
        The name of the measurement
    fit_bound : tuple with two scalars, optional
        The upper boundary of the absolute value of the data to be fit
    bounds : tuple with two floats, optional
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.
    deg : int
        The degree of the polynomial that should be used to fit the data

    Return
    ------
    fig_test, ax_test : matplotlib figure and axis
        The figure and axis showing the data
    new_measurement : dict
        The interpolated data, ready for use in the fft
    """

    lim = 100

    if 'dR' in measurement:
        r_key = 'dR'
        yunit = r'[$\Omega$]'

    if 'B' in measurement:
        b_key = 'B'
        xunit = '[mT]'
    elif 'BField' in measurement:
        b_key = 'BField'
        xunit = '[mT]'
    elif 'BzField' in measurement:
        b_key = 'BzField'
        xunit = '[mT]'
    elif r'$\Phi / \Phi_0$' in measurement:
        b_key = r'$\Phi / \Phi_0$'
        xunit = ''
    else:
        print('Could not find a valid channel for representing the magnet'
              ' field. Please try something else.')

    # Define the boundaries
    neg_bndry = bounds[0]
    pos_bndry = bounds[1]

    fit_bound_neg = fit_bound * -1
    fit_bound_pos = fit_bound

    # Get the positive and negative dataframes
    temp_neg = measurement[(measurement[b_key] >= fit_bound_neg) &
                           (measurement[b_key] <= neg_bndry)]
    temp_pos = measurement[(measurement[b_key] >= pos_bndry) &
                           (measurement[b_key] <= fit_bound_pos)]

    # Get the x channels
    x_pos = temp_pos[b_key]
    x_neg = temp_neg[b_key]
    x = measurement[b_key][(measurement[b_key] >= fit_bound_neg) &
                           (measurement[b_key] <= fit_bound_pos)]

    # Find next power of two of each channel's length
    idx_pos = nextpow2(len(x_pos))
    idx_neg = nextpow2(len(x_neg))
    idx = nextpow2(len(x))

    # Linearly inerpolate the x channels (we are assuming here an monotonically
    # increasing function, i.e. strictly linear!)
    new_x_pos = np.linspace(np.min(x_pos), np.max(x_pos), idx_pos)
    new_x_neg = np.linspace(np.min(x_neg), np.max(x_neg), idx_neg)
    new_x = np.linspace(np.min(x), np.max(x), idx)

    # Setup the new data frame
    new_measurement = {}
    new_measurement['mods'] = []
    new_measurement['modified'] = True
    new_measurement['negFFT'] = pd.DataFrame({'IntBField': new_x_neg})
    new_measurement['FFT'] = pd.DataFrame({'IntBField': new_x})
    new_measurement['posFFT'] = pd.DataFrame({'IntBField': new_x_pos})

    # Setup the plot
    fig_poly_fit, ax_poly_fit = plt.subplots(nrows=2, ncols=3, sharex='col',
                                             sharey='row')

    # Get the y channels
    y_pos = temp_pos[r_key]
    y = measurement[r_key][(measurement[b_key] >= fit_bound_neg) &
                           (measurement[b_key] <= fit_bound_pos)]
    y_neg = temp_neg[r_key]

    # Linearly interpolate the y channels and generate
    # print('Doing linear interpolation of x')
    f_pos = interp1d(x_pos, y_pos, 'linear')
    f = interp1d(x, y, 'linear')
    f_neg = interp1d(x_neg, y_neg, 'linear')

    new_y_pos = f_pos(new_x_pos)
    new_y = f(new_x)
    new_y_neg = f_neg(new_x_neg)

    # Sometimes a nan sneaks in and the rest of the analysis does not
    # work. Catch that here.
    if np.isnan(new_y_pos).any():
        # print(key, 'Nan in new_y_pos')
        new_y_pos = np.nan_to_num(new_y_pos)
        new_y_pos[new_y_pos == 0] = new_y_pos[new_y_pos != 0].min()

    if np.isnan(new_y_neg).any():
        # print(key, 'Nan in new_y_neg')
        new_y_neg = np.nan_to_num(new_y_neg)
        new_y_neg[new_y_neg == 0] = new_y_neg[new_y_neg != 0].max()

    if np.isnan(new_y).any():
        # print(key, 'Nan in new_y')
        new_y = np.nan_to_num(new_y)
        new_y[new_y == 0] = new_y[new_y != 0].max()

    # Do the polynomial fit
    # print('Doing polynomial fit')
    pos_poly_params = np.polyfit(x_pos, y_pos, deg)
    neg_poly_params = np.polyfit(x_neg, y_neg, deg)
    poly_params = np.polyfit(x, y, deg)

    pos_poly_func = np.poly1d(pos_poly_params)
    neg_poly_func = np.poly1d(neg_poly_params)
    poly_func = np.poly1d(poly_params)

    new_pos_poly_params = np.polyfit(new_x_pos, new_y_pos, deg)
    new_neg_poly_params = np.polyfit(new_x_neg, new_y_neg, deg)
    new_poly_params = np.polyfit(new_x, new_y, deg)

    new_pos_poly_func = np.poly1d(new_pos_poly_params)
    new_neg_poly_func = np.poly1d(new_neg_poly_params)
    new_poly_func = np.poly1d(new_poly_params)

    # Generate the fit data, i.e. the background
    y_pos_poly_fit = pos_poly_func(x_pos)
    y_neg_poly_fit = neg_poly_func(x_neg)
    y_poly_fit = poly_func(x)

    new_y_pos_poly_fit = new_pos_poly_func(new_x_pos)
    new_y_neg_poly_fit = new_neg_poly_func(new_x_neg)
    new_y_poly_fit = new_poly_func(new_x)

    # Generate the background reduced data by subtracting the polynomial
    #  fit from the linearly interpolated data
    y_pos_reduced = y_pos - y_pos_poly_fit
    y_neg_reduced = y_neg - y_neg_poly_fit
    y_reduced = y - y_poly_fit

    new_y_pos_reduced = new_y_pos - new_y_pos_poly_fit
    new_y_neg_reduced = new_y_neg - new_y_neg_poly_fit
    new_y_reduced = new_y - new_y_poly_fit

    if 'dR/dR_N' in r_key:
        r_name = 'dRn'
    elif 'dR' in r_key:
        r_name = 'dR'
    elif 'R' in r_key:
        r_name = 'R'

    int_r_key = 'Int_{}'.format(r_name)
    back_r_key = 'Back_{}'.format(r_name)
    red_r_key = 'Red_{}'.format(r_name)
    # print(int_r_key, back_r_key, red_r_key)

    new_measurement['negFFT'][int_r_key] = new_y_neg
    new_measurement['negFFT'][back_r_key] = new_y_neg - \
        new_y_neg_reduced
    new_measurement['negFFT'][red_r_key] = new_y_neg_reduced
    new_measurement['mods'].append('Adding interpolated and background'
                                   '-reduced negative branch of sweep for'
                                   ' FFT.')

    new_measurement['FFT'][int_r_key] = new_y
    new_measurement['FFT'][back_r_key] = new_y - \
        new_y_reduced
    new_measurement['FFT'][red_r_key] = new_y_reduced
    new_measurement['mods'].append('Adding interpolated and background'
                                   '-reduced sweep for FFT.')

    new_measurement['posFFT'][int_r_key] = new_y_pos
    new_measurement['posFFT'][back_r_key] = new_y_pos - \
        new_y_pos_reduced
    new_measurement['posFFT'][red_r_key] = new_y_pos_reduced
    new_measurement['mods'].append('Adding interpolated and background'
                                   '-reduced positive branch of sweep for'
                                   ' FFT.')

    # print('Plotting')
    # Interpolated Data
    ax_poly_fit[0][0].plot(new_x_neg[::10], new_y_neg[::10],
                           label=r_key + ' int.')
    ax_poly_fit[0][1].plot(new_x_pos[::10], new_y_pos[::10],
                           label=r_key + ' int.')

    ax_poly_fit[0][2].plot(new_x[::10], new_y[::10],
                           label=r_key + ' int.')

    ax_poly_fit[0][0].plot(x_neg[::10], y_neg[::10],
                           label=r_key + ' raw')
    ax_poly_fit[0][1].plot(x_pos[::10], y_pos[::10],
                           label=r_key + ' raw')

    ax_poly_fit[0][2].plot(x[::10], y[::10],
                           label=r_key + ' raw')

    #  Fit Data
    ax_poly_fit[0][0].plot(new_x_neg[::10], new_y_neg_poly_fit[::10],
                           label=r_key + ' bckgrnd int.')
    ax_poly_fit[0][1].plot(new_x_pos[::10], new_y_pos_poly_fit[::10],
                           label=r_key + ' bckgrnd int.')

    ax_poly_fit[0][2].plot(new_x[::10], new_y_poly_fit[::10],
                           label=r_key + ' bckgrnd raw')

    ax_poly_fit[0][0].plot(x_neg[::10], y_neg_poly_fit[::10],
                           label=r_key + ' bckgrnd raw')
    ax_poly_fit[0][1].plot(x_pos[::10], y_pos_poly_fit[::10],
                           label=r_key + ' bckgrnd raw')

    ax_poly_fit[0][2].plot(x[::10], y_poly_fit[::10],
                           label=r_key + ' bckgrnd raw')

    # Full Range Data

    # Reduced data
    ax_poly_fit[1][0].plot(new_x_neg[np.abs(new_y_neg_reduced) <= lim][::10],
                           new_y_neg_reduced[np.abs(new_y_neg_reduced) <= lim][::10],
                           label=r_key + ' red int.')
    ax_poly_fit[1][1].plot(new_x_pos[np.abs(new_y_pos_reduced) <= lim][::10],
                           new_y_pos_reduced[np.abs(new_y_pos_reduced) <= lim][::10],
                           label=r_key + ' red int.')

    ax_poly_fit[1][2].plot(new_x[np.abs(new_y_reduced) <= lim][::10],
                           new_y_reduced[np.abs(new_y_reduced) <= lim][::10],
                           label=r_key + ' red int.')

    ax_poly_fit[1][0].plot(x_neg[np.abs(y_neg_reduced) <= lim][::10],
                           y_neg_reduced[np.abs(y_neg_reduced) <= lim][::10],
                           label=r_key + ' red raw')
    ax_poly_fit[1][1].plot(x_pos[np.abs(y_pos_reduced) <= lim][::10],
                           y_pos_reduced[np.abs(y_pos_reduced) <= lim][::10],
                           label=r_key + ' red raw')

    ax_poly_fit[1][2].plot(x[np.abs(y_reduced) <= lim][::10],
                           y_reduced[np.abs(y_reduced) <= lim][::10],
                           label=r_key + ' red raw')
    # print('Finished plotting')

    ax_poly_fit[0][0].set_title('Negative')
    ax_poly_fit[0][1].set_title('{k}; {d:d}-deg\nPositive'.format(k=key,
                                                                  d=deg))
    ax_poly_fit[0][2].set_title('Whole')

    ax_poly_fit[0][2].legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
    ax_poly_fit[1][2].legend(loc='center left', bbox_to_anchor=(1.0, 0.5))

    ax_poly_fit[0][0].set_ylabel('$R_s$ {}'.format(yunit))
    ax_poly_fit[1][0].set_ylabel('Bkgnd Reduced\n$R_s$ {}'
                                 .format(yunit))

    ax_poly_fit[1][0].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))
    ax_poly_fit[1][1].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))
    ax_poly_fit[1][2].set_xlabel(r'$\Phi/\Phi_0$ {}'.format(xunit))

    ax_poly_fit[1][0].set_xlim(new_x_neg.min(), new_x_neg.max())
    ax_poly_fit[1][1].set_xlim(new_x_pos.min(), new_x_pos.max())
    ax_poly_fit[1][2].set_xlim(new_x.min(), new_x.max())

    fig_poly_fit.subplots_adjust(wspace=0.05, hspace=0.05)

    return fig_poly_fit, ax_poly_fit, new_measurement


