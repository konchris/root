def generate_sg_fits(measurement, key, bounds=(0.0, 0.0), sg_wins=(1, 1, 1)):
    """Generate tanh fits of the negative and positive data

    Parameters
    ----------
    measurement : dict
        The data frame to modify
    key : str
        The name of the measurement
    bounds : tuple with two floats, optional
        The upper boundary of the negative side of the sweep and the lower
        boundary of the positive side of the sweep.
    init_params : tuple of two four-element lists
        The initial parameters for performing the tanh fit.
        DEFAULT: ([1, 1, 1, 1], [1, 1, 1, 1])

    Return
    ------
    fig_test, ax_test : matplotlib figure and axis
        The figure and axis showing the data
    new_measurement : dict
        The interpolated data, ready for use in the fft
    """
    #print(key)

    if 'dR' in measurement:
        r_key = 'dR'

    yunit = r'[$\Omega$]'

    if 'B' in measurement:
        b_key = 'B'
        xunit = '[mT]'
    elif '$\Phi / \Phi_0$' in measurement:
        b_key = '$\Phi / \Phi_0$'
        xunit = ''
    elif 'BField' in measurement:
        b_key = 'BField'
        xunit = '[mT]'
    elif 'BzField' in measurement:
        b_key = 'BzField'
        xunit = '[mT]'
    else:
        print('Could not find a valid channel for representing the magnet'
              ' field. Please try something else.')

    if 'Phi_0' in b_key:
        b_name = 'Int_n'
    elif 'Phi' in b_key:
        b_name = 'IntPhi'
    elif 'B' in b_key:
        b_name = 'IntBField'

    # Define the boundaries
    neg_bndry = bounds[0]
    pos_bndry = bounds[1]

    # Get the positive and negative dataframes
    temp_neg = measurement[measurement['B'] < neg_bndry]
    temp_pos = measurement[measurement['B'] > pos_bndry]

    # Get the x channels
    x_neg = temp_neg[b_key]
    x_pos = temp_pos[b_key]
    x = measurement[b_key][(measurement['B'] < neg_bndry) |
                           (measurement['B'] > pos_bndry)]

    # Find next power of two of each channel's length
    idx_neg = nextpow2(len(x_neg))
    idx_pos = nextpow2(len(x_pos))
    idx = nextpow2(len(x))

    # Linearly inerpolate the x channels (we are assuming here an monotonically
    # increasing function, i.e. strictly linear!)
    new_x_neg = np.linspace(np.min(x_neg), np.max(x_neg), idx_neg)
    new_x_pos = np.linspace(np.min(x_pos), np.max(x_pos), idx_pos)
    new_x = np.linspace(np.min(x), np.max(x), idx)

    # Setup the new data frame
    new_measurement = {}
    new_measurement['mods'] = []
    new_measurement['modified'] = True
    new_measurement['negFFT'] = pd.DataFrame({b_name: new_x_neg})
    new_measurement['posFFT'] = pd.DataFrame({b_name: new_x_pos})
    new_measurement['FFT'] = pd.DataFrame({b_name: new_x})

    # Get the y channels
    y_neg = temp_neg[r_key]
    y_pos = temp_pos[r_key]
    y = measurement[r_key][(measurement['B'] < neg_bndry) |
                           (measurement['B'] > pos_bndry)]

    # Linearly interpolate the y channels and generate
    # print('Doing linear interpolation of x')
    f_neg = interp1d(x_neg, y_neg, 'linear')
    f_pos = interp1d(x_pos, y_pos, 'linear')
    f = interp1d(x, y, 'linear')

    new_y_neg = f_neg(new_x_neg)
    new_y_pos = f_pos(new_x_pos)
    new_y = f(new_x)

    # Sometimes a nan sneaks in and the rest of the analysis does not
    # work. Catch that here.
    if np.isnan(new_y_neg).any():
        #print(key, 'Nan in new_y_neg')
        new_y_neg = np.nan_to_num(new_y_neg)
        new_y_neg[new_y_neg == 0] = new_y_neg[new_y_neg != 0].max()

    if np.isnan(new_y_pos).any():
        #print(key, 'Nan in new_y_pos')
        new_y_pos = np.nan_to_num(new_y_pos)
        new_y_pos[new_y_pos == 0] = new_y_pos[new_y_pos != 0].min()

    if np.isnan(new_y).any():
        #print(key, 'Nan in new_y')
        new_y = np.nan_to_num(new_y)
        new_y[new_y == 0] = new_y[new_y != 0].max()

    # Do the SG smoothing
    # print('Doing SG Smoothing')
    (win_neg, win_pos, win) = sg_wins
    order = 3

    new_ys_neg = savitzky_golay(new_y_neg, win_neg, order)
    new_ys_pos = savitzky_golay(new_y_pos, win_pos, order)
    # new_ys = savitzky_golay(new_y, win, order)

    # Generate the background reduced data by subtracting the univariate
    #  spline from the linearly interpolated data
    # print('Calculating reduced background')
    new_y_neg_reduced = new_y_neg - new_ys_neg
    new_y_pos_reduced = new_y_pos - new_ys_pos
    # new_y_reduced = new_y - new_ys

    if 'dR/dR_N' in r_key:
        r_name = 'dRn'
    elif 'dR' in r_key:
        r_name = 'dR'
    elif 'R' in r_key:
        r_name = 'R'

    int_r_key = 'Int{}'.format(r_name)
    back_r_key = 'Back{}'.format(r_name)
    red_r_key = 'Red{}'.format(r_name)
    # print(int_r_key, back_r_key, red_r_key)

    new_measurement['negFFT'][int_r_key] = new_y_neg
    new_measurement['negFFT'][back_r_key] = new_y_neg - \
        new_y_neg_reduced
    new_measurement['negFFT'][red_r_key] = new_y_neg_reduced
    new_measurement['mods'].append('Adding interpolated and background'
                                   '-reduced negative branch of sweep for'
                                   ' FFT.')

    new_measurement['posFFT'][int_r_key] = new_y_pos
    new_measurement['posFFT'][back_r_key] = new_y_pos - \
        new_y_pos_reduced
    new_measurement['posFFT'][red_r_key] = new_y_pos_reduced
    new_measurement['mods'].append('Adding interpolated and background'
                                   '-reduced positive branch of sweep for'
                                   ' FFT.')

    # Setup the plot
    fig_sgfit, ax_sgfit = plt.subplots(nrows=2, ncols=2, sharex='col', sharey='row')

    #print('Plotting')
    # Interpolated Data
    ax_sgfit[0][0].plot(new_x_neg[::10], new_y_neg[::10],
                        label=r_key)
    ax_sgfit[0][1].plot(new_x_pos[::10], new_y_pos[::10],
                        label=r_key)
    # ax_sgfit[0][2].plot(new_x, new_y, label=r_key)

    # Spline Smoothed Data
    ax_sgfit[0][0].plot(new_x_neg[::10],
                        (new_y_neg - new_y_neg_reduced)[::10],
                        color=sns.xkcd_rgb['medium green'],
                        label='win = {w}\norder = {o}\nlen = {l}'
                        .format(w=win_neg, o=order, l=new_y_neg.size))
    ax_sgfit[0][1].plot(new_x_pos[::10],
                        (new_y_pos - new_y_pos_reduced)[::10],
                        color=sns.xkcd_rgb['medium green'],
                        label='win = {w}\norder = {o}\nlen = {l}'
                        .format(w=win_pos, o=order, l=new_y_neg.size))

    ax_sgfit[0][0].legend(loc='best')
    ax_sgfit[0][1].legend(loc='best')

    # Reduced data
    ax_sgfit[1][0].plot(new_x_neg[::10], new_y_neg_reduced[::10],
                        label=r_key)
    ax_sgfit[1][1].plot(new_x_pos[::10], new_y_pos_reduced[::10],
                        label=r_key)

    #print('Finished plotting')

    ax_sgfit[0][0].set_title('Negative')
    ax_sgfit[0][1].set_title('Positive')

    ax_sgfit[0][0].set_ylabel('Interpolated Data\n$R_s$ {}'
                              .format(yunit))
    ax_sgfit[1][0].set_ylabel('Bkgnd Reduced\n$R_s$ {}'
                              .format(yunit))

    ax_sgfit[1][0].set_xlabel(r'{k} {u}'.format(k=r_key, u=xunit))
    ax_sgfit[1][1].set_xlabel(r'{k} {u}'.format(k=r_key, u=xunit))

    fig_sgfit.tight_layout()

    return fig_sgfit, ax_sgfit, new_measurement
