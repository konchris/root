#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Combine the TDMS files into one HDF5

"""

import os
import pandas as pd
import argparse

df_itc = []
df_lake = []

data_dir = '/home/chris/Documents/PhD/root/data'
sample = 'fonin_heliox'

def main():
    prog_desc = 'Combine the TDMS files of a given measurement run into on HDF5 file.'
    parser = argparse.ArgumentParser(description=prog_desc)
    parser.add_argument("measrun", help='measurement run to combine')
    args = parser.parse_args()
    
    src_path = os.path.join(data_dir, sample, args.measrun)
    dst_fname = args.measrun + '.h5'
    dst_file = os.path.join(src_path, dst_fname)

    os.remove(dst_file)
    
    if not os.path.exists(src_path):
        print('Source directory does not exist.')
        print('Please try again.')
        return
    
    for fname in sorted(os.listdir(src_path)):
        
        full_fname = os.path.join(src_path, fname)
        
        if (os.path.isfile(full_fname) and
            fname.split('.')[-1] == 'h5' and
            not (fname == dst_fname)):

            #print('Opening file {}...'.format(fname))
            
            hdf_in = pd.HDFStore(full_fname)
            
            #print("Loading {}'s data...".format(fname))
            
            #df_itc.append(hdf_in['/raw/ITC503'])
            #df_lake.append(hdf_in['/raw/Lakeshore'])

            hdf_in['/raw/ITC503'].to_hdf(dst_file, '/raw/ITC503', append=True, complevel=9, complib='zlib')
            
            #print('Closing {}...'.format(fname))
            
            hdf_in.close()
            
            #print('Done.')

    #all_itc = pd.concat(df_itc)
    #all_lake = pd.concat(df_lake)

    #print(all_itc.describe())
    #print(all_lake.describe())


    #print('Writing to {}...'.format(dst_file))

    #hdf_out = pd.HDFStore(dst_file, complevel=9, complib='zlib')

    #hdf_out.append('raw/ITC503', all_itc)
    #hdf_out.append('raw/Lakeshore', all_lake)
    #all_itc.to_hdf(dst_file, '/raw/ITC503', append=False, complevel=9, complib='zlib')
    #all_lake.to_hdf(dst_file, '/raw/Lakeshore', append=False, complevel=9, complib='zlib')
    #print('Done.')
        
if __name__ == '__main__':
    main()
