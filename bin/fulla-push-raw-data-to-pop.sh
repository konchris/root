#!/usr/bin/env bash 
# -*- coding: utf-8 -*-

LOGFILE="/home/chris/Documents/PhD/root/log/raw-data-sync.log"

RSYNC_OPTIONS="-avhz --update --stats --no-i-r --info=progress2 --exclude=*/.PowerFolder/* $*"

SOURCE="/home/chris/Documents/PhD/root/raw-data/"
TARGET="/home/chris/Espy/root/raw-data/"

echo "********************************************" >> $LOGFILE

echo "$(date): Pushing from $SOURCE to $TARGET" >> $LOGFILE

rsync $RSYNC_OPTIONS $SOURCE $TARGET >> $LOGFILE

echo "********************************************" >> $LOGFILE
echo "" >> $LOGFILE
