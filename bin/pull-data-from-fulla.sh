#!/usr/bin/env bash 
# -*- coding: utf-8 -*-

LOGFILE="/home/chris/Documents/PhD/root/log/data-sync.log"

RSYNC_OPTIONS="-avh --update --stats --progress -e ssh --no-i-r --exclude=*/.PowerFolder/* $*"

SOURCE="fulla:Documents/PhD/root/data/"
TARGET="/home/chris/Documents/PhD/root/data/"

echo "********************************************" | tee $LOGFILE

echo "$(date): Pulling from $SOURCE to $TARGET" | tee $LOGFILE

rsync $RSYNC_OPTIONS $SOURCE $TARGET | tee $LOGFILE

echo "********************************************" | tee $LOGFILE
echo "" | tee $LOGFILE
