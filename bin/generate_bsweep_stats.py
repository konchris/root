#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import argcomplete

sys.path.append(os.path.join(os.path.expanduser('~'), 'Documents', 'PhD',
                             'root', 'lib'))

import matplotlib as mpl
mpl.use('AGG')
import seaborn as sns

import numpy as np
import pandas as pd

# The following is bad practice, but it works for me for now.
from SuperconductivityCalculations import (coherence_length)
from MiscFunctions import (load_sweep_data, DATA_DIR)

sns.set_context("talk", font_scale=1.25, rc={"lines.linewidth": 3})
sns.set_style("whitegrid")

# Options to implement:
# 1) Overwrite existing image files or not
# 2) Pass individual file names


def sample_runs(parsed_args, **kwargs):
    sample_dir = os.path.join(DATA_DIR, parsed_args.sample,
                              'cryo_measurements')
    return next(os.walk(sample_dir))[1]


def samples(**kwargs):
    return next(os.walk(DATA_DIR))[1]


def main(argv=None):
    """The main function."""

    prog_desc = "Calculate magnet field characteristics of bsweeps."
    parser = argparse.ArgumentParser(description=prog_desc)
    parser.add_argument('sample', help='The sample for which the statistics'
                        ' should be calculated').completer = samples
    parser.add_argument('sample_run', help='The measurement run for which'
                        ' statistics should be calculated').completer = \
                        sample_runs
    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    sns.set_context("paper", font_scale=1.25, rc={"lines.linewidth": 1})
    sns.set_style("whitegrid")

    # Load Data

    print('Loading data')
    (df, df_extra) = load_sweep_data('bsweep', args.sample, args.sample_run)

    # Determine $B_c \left(T\right)$

    # We'll say that $B_c$ is at $0.5 \cdot (dR_{max} + dR_{min})$

    extra_data = {}

    temps = []
    bcsp = []
    bcsm = []

    i = 0
    for key in sorted(df.keys()):

        print('Generating SC stats for {}'.format(df[key]['filename']))

        temp_df = df[key]['ADWin']

        dR_max = temp_df['dR'].max()
        dR_min = temp_df['dR'].min()

        # Use only those sweeps during which the superconducting state was
        # reached
        if dR_min < 0.6:

            temp = int(key.split('mK')[0])
            temps.append(temp)

            dR_t = 0.5 * (dR_max + dR_min)

            extra_data[i] = {}
            extra_data[i]['name'] = key
            extra_data[i][r'$dR(B=B_c)$'] = dR_t
            extra_data[i][r'$dR_N$'] = dR_max
            extra_data[i]['T'] = temp

            # This has to be done twice: once for B < 0 and
            # once for B > 0

            # 1) Subtract the dR value from the array
            dR_array_pos = temp_df['dR'][temp_df['B'] > 0] - dR_t
            dR_array_neg = temp_df['dR'][temp_df['B'] < 0] - dR_t

            # 2) Take the absolute value
            dR_array_abs_pos = np.abs(dR_array_pos)
            dR_array_abs_neg = np.abs(dR_array_neg)

            # 3) Find the index of the minimum value
            ind_t_pos = dR_array_abs_pos.idxmin()
            ind_t_neg = dR_array_abs_neg.idxmin()

            # 4) Get the value of the magnet field at the index
            b_c_t_pos = temp_df['B'][ind_t_pos]
            b_c_t_neg = temp_df['B'][ind_t_neg]

            bcsp.append(b_c_t_pos)
            bcsm.append(b_c_t_neg)

            extra_data[i][r'$B_c^+(T)$'] = b_c_t_pos
            extra_data[i][r'$B_c^-(T)$'] = np.abs(b_c_t_neg)

            # 5) Calculate the Coherence Length
            extra_data[i][r'$\xi^+$'] = coherence_length(b_c_t_pos)
            extra_data[i][r'$\xi^-$'] = coherence_length(np.abs(b_c_t_neg))

            i += 1

    df_extra = pd.DataFrame(extra_data).T

    print('Saving SC stats to file.')
    df_extra.to_csv(os.path.join(DATA_DIR, args.sample, 'cryo_measurements',
                                 args.sample_run,
                                 'critical_fields_bsweeps.csv'))

    print('Done.')

if __name__ == "__main__":
    main()
