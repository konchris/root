'<ADbasic Header, Headerversion 001.001>
' Process_Number                 = 1
' Initial_Processdelay           = 303
' Eventsource                    = Timer
' Control_long_Delays_for_Stop   = No
' Priority                       = High
' Version                        = 1
' ADbasic_Version                = 5.0.8
' Optimize                       = Yes
' Optimize_Level                 = 1
' Info_Last_Save                 = METHIS  METHIS\Chris
'<Header End>
' Basic ADwin program for two channel output and PID control.
'
' The program averages over len number of data points an stores
' this value in a FIFO buffer based on the channel the data
' is read from.
' The user has the opportunity to set the effective measurement
' rate and the gain with which each channel is read.
' For more information on this program please see the pdf document
' "ADwin Programming".
'
' Author: Christopher Espy
' Last Change: 2013-10-02
'
' Please note that the following global variables are being used
' by the program:
' Par_1 through Par_16: The is where the user can set the measurement
'         range for each respective channel externally.
' Data_1 through Data_16: These are FIFO buffers where an external
'         program can collect the averaged measurement data.
' FPar_1 through FPar_16: The user can activate these to be able to 
'         watch the current value from each channel.
' Par_20: This is where the user can set the effective sample rate
'         to something other than the default value of 10 Hz.
' Par_21: This is where the user can check to see how many data
'         points are averaged over.
' Par_22: The limiting range factor for the integral term
' Par_30: The setpoint. Units are to be determined.
' FPar_31: P - gain or proportional factor
' FPar_32: I - reset or integration time
' FPar_33: D - rate or derivative time

#Define BufferSize 100000
#Define offset 32768

Dim data_1[BufferSize] as float as fifo
Dim data_2[BufferSize] as float as fifo

Dim gain01, gain02 as long
Dim range01, range02 as float

Dim muxpattern as long
Dim chanpattern as long

Dim c1, c2, len, n as long

' PID is accomplished using the following formula:
'        mv = P(e + I*Iterm + D*Dterm)
' where
' e - error (e = sp - pv) the instantaneous error
' sp - the setpoint, in digits, from the user
' pv - the current process value
' err - the error calculated for the current cycle
' errold - the error from the last cycle
' mv - manipulation value or output to send to the
'      contorller, or heater in this case, in digits
' Iterm - the integral term as defined in PID theory
' Dterm - the differential term as defined in PID
'         theory
' dt - the time needed for a single process, in
'      seconds.
'      This is needed for the calculations of Iterm
'      and Dterm.

Dim sp, pv, Ilim as long
Dim err, errold, mv as float
Dim P, I, D, Iterm, Dterm, dt as float

Init: 'Beginning of a program block for initialization.
  Processdelay = 400
  dt = Processdelay * 25E-9
  If (par_20 = 0) Then
    len = 10000
  Else
    len = 1 / (Processdelay * par_20 * 25E-9)
  EndIf
  par_21 = len

  If (par_1 = 0) Then
    gain01 = 00b  ' set gain to 1
    range01 = 20
  Else
    range01 = par_1
    gain01 = LN(20 / range01) / LN(2)
  EndIf

  If (par_2 = 0) Then
    gain02 = 00b  ' set gain to 1
    range02 = 20
  Else
    range02 = par_2
    gain02 = LN(20 / range02) / LN(2)
  EndIf

  chanpattern = 000000b ' set initial channels to 1 and 2

  muxpattern = (chanpattern | shift_left(gain01, 6)) | shift_left(gain02, 8)

  ' Set a bunch of the variables to initial values for PID
  P = FPar_31
  I = FPar_32
  D = FPar_33
  Iterm = 0
  Dterm = 0
  errold = 0
  ' We set the error to zero because we first take an
  ' average of len number of pv values and calculate
  ' an average error from that.
  err = 0

  If (par_22 = 0) Then
    Ilim = 1E3
  Else
    Ilim = Par_22
  EndIf

  Set_Mux(muxpattern)
  Sleep(65)

Event:
  mv = P * (err + I * Iterm + D * Dterm) + offset
  ' We want to limit the power output to our heater
  ' to protect the system.
  ' These numbers are based on calculations using
  ' system value of R = 460 Ohm.
  If (mv > 37520) Then mv = 37520
  If (mv < offset) Then mv = offset
  
  Start_Conv(11b)
  DAC(2, mv)
  errold = err

  Wait_EOC(11b)

  c1 = c1 + ReadADC(1)
  c2 = c2 + ReadADC(2)

  Inc n
  If (n > len) Then
    ' Set the old error
    errold = err
    ' Get the setpoint
    sp = Par_30
    ' calculate the average pv
    pv = c2 / n
    ' calculate the current error
    err = pv - sp
    ' calculate the integral term
    ' note that the time between each average error
    ' is the single process time (cycle time), dt,
    ' times the number of processes to collect the
    ' data, n.
    Iterm = Iterm + dt * n * Err
    If (Iterm > Ilim) Then Iterm = Ilim
    If (Iterm < -Ilim) Then Iterm = -Ilim
    ' calculate the differential term
    Dterm = (err - errold) / (dt * n)

    fpar_1 = (c1 - offset * n) * (range01 / (n * 65536))
    data_1 = fpar_1
    fpar_2 = (c2 - offset * n) * (range02 / (n * 65536))
    data_2 = fpar_2

    c1 = 0
    c2 = 0
    n = 0
  EndIf

Finish:
  ' For fail-safe reasons always finish by setting the
  ' output of the contorl channel to 0 V.
  DAC(2, offset)
