'<ADbasic Header, Headerversion 001.001>
' Process_Number                 = 1
' Initial_Processdelay           = 1818
' Eventsource                    = Timer
' Control_long_Delays_for_Stop   = No
' Priority                       = High
' Version                        = 1
' ADbasic_Version                = 5.0.8
' Optimize                       = Yes
' Optimize_Level                 = 1
' Info_Last_Save                 = METHIS  METHIS\Chris
'<Header End>
#Define gain1 20 ' [V] This is the gain range in volts. See ADwin-Gold manual pg 47
#Define timing0 10 ' the desired measurement frequency in Hz, i.e. data collection
#DEFINE BUFFSIZE 10000
#Define gainfactor 1 
' The gain factors are the following for the corresponding ranges
'1 = -10 V to 10 V
'2 = -5 V to 5 V
'4 = -2.5 V to 2.5 V
'8 = -1.25 V to 1.25 V
' The voltage is then calculated with the following
'   Voltage = (Digits - 32768) * Range / 65536
Dim n as long ' this is the counter for averaging
Dim len as long ' this determines how many measurement values are collected for averaging
Dim c1, c2 as long ' these are where measurement values are stored before averaging
Dim t0,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,offset as long ' these are for timing purposes
Dim timing as long ' measurement timing in Hz

dim data_1[BUFFSIZE] as integer as Fifo  ' Declaration of an Integer Fifo for data exchange with the PC
dim data_2[BUFFSIZE] as integer as Fifo  ' Declaration of an Integer Fifo for data exchange with the PC

' par_1 shows the offset
' par_2 shows len
' par_3 time for init
' par_4 process time
' par_5 value of n
' par_6 allows user to set process delay via timing at runtime

LowInit:   'Beginning of a program block for initialization.
  'This has a low priority. Usefull for the definition of variables.
  n = 0
  c1=0
  c2=0
  
Init: '#########################################################
  
  '####### For testing timing uncomment the next few lines
  'offset = Read_Timer()
  'offset = Read_Timer() - offset
  'par_1 = offset
  '########
  
  
  Set_Mux(000000b)       'Set Mux for the first measurement channels 1+2
  
  'ProcessDelay = delay  '*25ns, Load should be <90%!
  timing = par_6 ' allow the user to specify a process delay through par_6
  if (timing=0) then timing=timing0

  len = 1 / (Processdelay  * 25E-9 * timing)

  Sleep(62)        'Total wait time of 6.5s. This is critical!
  
  '#### Timing
  't1 = Read_Timer()
  'par_3 = (t1 - t0 - offset) * 25
  '#####
  par_2 = len
      
Event: '#########################################################

  'The function FIFO_EMPTY calculates the number of free elements in the FIFO
  par_11 = FIFO_Empty(1)
  par_12 = FIFO_Empty(2)

  '#### Timing
  't2 = Read_Timer()
  'par_4 = (t2 - t1 - offset) * 25
  '####
  
  Start_Conv(11b)    'Start conversion (channels 1+2)
  Wait_EOC(11b)      'Wait for end of conversion (channels 1+2)
  
  ' ReadADC(adc_no)
  
  c1 = c1 + ReadADC(1)  'Read out ADC1, channel 1'
  c2 = c2 + ReadADC(2)  'Read out ADC2, channel 2'
  
  'Sleep(25)
  't1 = Read_Timer()
  
  Inc n              'increase n by one
  If (n>=len) Then  
    fpar_1 = (c1-32768*n)*(gain1/(n*65536)) ' calculate the average value of channel 1
    fpar_2 = (c2-32768*n)*(gain1/(n*65536)) ' calculate the average value of channel 2

    data_1 = fpar_1
    data_2 = fpar_2

    par_5 = n
    c1=0
    c2=0
    n = 0
  endif
   
Finish: '#########################################################
  



  




