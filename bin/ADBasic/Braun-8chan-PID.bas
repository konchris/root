'<ADbasic Header, Headerversion 001.001>
' Process_Number                 = 1
' Initial_Processdelay           = 1818
' Eventsource                    = Timer
' Control_long_Delays_for_Stop   = No
' Priority                       = High
' Version                        = 1
' ADbasic_Version                = 5.0.8
' Optimize                       = Yes
' Optimize_Level                 = 1
' Info_Last_Save                 = BALDUR-WIN7  baldur-win7\julian
'<Header End>
'#Define IN1 Data_1
'#Define IN2 Data_2
'#Define len0 2259   'Standard Averaging Number: the number of values averaged.
'#Define delay 1770  '*25ns = Sampling-Time for all 10 channels [ns] = 31.25 microseconds
'#Define gain1 20 ' [V] This is the gain range in volts. See ADwin-Gold manual pg 47
#Define takt0 10 ' the desired measurement frequency in Hz, i.e. data collection
#DEFINE BUFFSIZE 10000
'1 = -10 V to 10 V
'2 = -5 V to 5 V
'4 = -2.5 V to 2.5 V
'8 = -1.25 V to 1.25 V
'#Define SConv 20400010h  'Hexadecimal number used with poke
'#Define ueberlauf 2000000000  'Abbruchb. fuer n im urspr. Programm
Dim n,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,len As Long
'Dim chan As Long
Dim t0,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,offset,st,takt As Long
Dim out, Iterm, R, SetP, error, lastVal as Float
dim data_1[BUFFSIZE] as integer as Fifo  ' Declaration of an Integer Fifo for data exchange with the PC
dim data_2[BUFFSIZE] as integer as Fifo  ' Declaration of an Integer Fifo for data exchange with the PC
dim data_3[BUFFSIZE] as integer as Fifo  ' Declaration of an Integer Fifo for data exchange with the PC
dim data_4[BUFFSIZE] as integer as Fifo  ' Declaration of an Integer Fifo for data exchange with the PC
'dim data_5[BUFFSIZE] as integer as Fifo  ' Declaration of an Integer Fifo for data exchange with the PC


LowInit:   'Beginning of a program block for initialization.
  'This has a low priority. Usefull for the definition of variables.
  n = 0
  c1=0
  c2=0
  c3=0
  c4=0
  c5=0
  c6=0
  c7=0
  c8=0
  'c9=0
  'c10=0
  '  c11=0
  '  c12=0
  '  c13=0
  '  c14=0
  '  c15=0
  '  c16=0
  '  CONF_DIO(12)         'Configures DIO 31 to 16 as output
  
  ' Variables for PID control
  out = 0
  Iterm = 0
  error = 0
  
  
Init: '#########################################################
  '####### For testing timing uncomment the next few lines
  offset = Read_Timer()
  offset = Read_Timer() - offset
  par_1 = offset
  '########
  Set_Mux(1111000000b)       'Set Mux for the first measurement channels 1+2
  '#### Timing
  't0 = Read_Timer()
  't11 = t0
  '####
  'ProcessDelay = delay  '*25ns, Load should be <90%!
  takt = par_6
  SetP = fpar_4
  if (takt=0) then takt=takt0
  'par_5 = Processdelay
  len = 1 / (Processdelay  * 25E-9 * takt)
  'if (len=0) then len=len0     'if len=0: standard averaging number
  Sleep(62)        'Total wait time of 6.5s. This is critical!
  '#### Timing
  't1 = Read_Timer()
  'par_3 = (t1 - t0 - offset) * 25
  '#####
  par_2 = len
  fpar_3 = 1
  fpar_2 = 5
  fpar_1 = 1
  fpar_8 = 2500
  fpar_7 = 0
      
Event: '#########################################################


  'The function FIFO_EMPTY calculates the number of free elements in the FIFO
  par_11 = FIFO_Empty(1)
  par_12 = FIFO_Empty(2)
  par_13 = FIFO_Empty(3)
  par_14 = FIFO_Empty(4)
  'par_15 = FIFO_Empty(5)
  t1 = Read_Timer()
  if (((((par_11>0) AND (par_12>0)) AND (par_13>0)) AND (par_14>0))) Then
    par_1 = 0
    '#### Timing
    't2 = Read_Timer()
    'par_4 = (t2 - t11 - offset) * 25
    '####
    't3 = Read_Timer()
    Start_Conv(11b)    'Start conversion (channels 1+2)
    Set_Mux(001001b)   'Set Mux (channels 3+4)
    Inc n              'increase n by one
    't3 = Read_Timer()  
    Wait_EOC(11b)      'Wait for end of conversion (channels 1+2)
    't4 = Read_Timer()
    c1 = c1 + ReadADC(1)  'Read out ADC1, channel 1'
    c2 = c2 + ReadADC(2)  'Read out ADC2, channel 2'
    't4 = Read_Timer()
    Sleep(24) ' The conversion takes 5 us wait the remaining 1.5 us 
    't5 = Read_Timer()
    'par_5 = (t4 - t3) * 25
    'par_7 = (t5 - t3) * 25

    Start_Conv(11b)    'Start conversion (channels 3+4)
    Set_Mux(1111011011b)   'Set Mux (channels 7+8) with gain 8
    't5 = Read_Timer()
    Wait_EOC(11b)      'Wait for end of conversion (channels 3+4)
    c3 = c3 + ReadADC(1)  'Read out ADC1, channel 3'
    c4 = c4 + ReadADC(2)  'Read out ADC2, channel 4'
    Sleep(24)
    't6 = Read_Timer()
    'par_6 = (t6 - t5 - offset) * 25
  
    Start_Conv(11b)    'Start conversion (channels 7+8)
    Set_Mux(100100b)   'Set Mux (channels 9+10)
    If (n>=len) Then
      t7 = Read_Timer()
      data_1 = c1/n | SHIFT_LEFT(c2/n,16)
      data_2 = c3/n | SHIFT_LEFT(c4/n,16)
      c1 = 0 : c2 = 0 : c3 = 0 : c4 = 0
    endif
    Wait_EOC(11b)      'Wait for end of conversion (channels 5+6)
    't7 = Read_Timer()
    c5 = c5 + ReadADC(1)  'Read out ADC1, channel 5'
    c6 = c6 + ReadADC(2)  'Read out ADC2, channel 6'
    'fpar_6 = error * fpar_3 * 0.000026
    'NOP : NOP : NOP
    par_7 = Max_Float(par_7,(Read_Timer() - t7 - offset) * 25)
    Sleep(25)
     
    Start_Conv(11b)    'Start conversion (channels 9+10)
    Set_Mux(1111000000b)   'Set Mux (channels 1+2)
    t3 = Read_Timer() 
    
    Wait_EOC(11b)      'Wait for end of conversion (channels 9+10)
    c7 = c7 + ReadADC(1)  'Read out ADC1, channel 7'
    c8 = c8 + ReadADC(2)  'Read out ADC2, channel 8'
    Sleep(25)
    
    If (n>=len) Then
      t3 = Read_Timer() 
      R = c7/n
      par_9=R
      data_3 = c5/n | SHIFT_LEFT(c6/n,16)
      c5 = 0 : c6 = 0
      R = (R-32768)*0.08255+3955 ' 
      fpar_5=R
      error = R-fpar_8
      fpar_6 = error
      Iterm = Max_Float(0,Min_Float(100,Iterm + error * fpar_1 * fpar_2 / takt0))
      out = fpar_1 * error + Iterm + fpar_1 * fpar_3 * (R-lastVal) * takt0
      lastVal = R
    endif
   
    'Start_Conv(11b)    'Start conversion (channels 9+10)
    'Set_Mux(000000b)   'Set Mux (channels 1+2)
    't11 = Read_Timer()
    'Wait_EOC(11b)      'Wait for end of conversion (channels 9+10)
    'c9 = c9 + ReadADC(1)  'Read out ADC1, channel 9'
    'c10 = c10 + ReadADC(2)  'Read out ADC2, channel 10'
    't12 = Read_Timer()
    'par_9 = (t12 - t11 - offset) * 25
    'par_10 = (t12 - t2 - offset) * 25
    
    
    If (n>=len) Then'
      'fpar_1 = (c1-32768*n)*(gain1/(n*65536))
      'fpar_2 = (c2-32768*n)*(gain1/(n*65536))
      'fpar_3 = (c3-32768*n)*(gain1/(n*65536))
      'fpar_4 = (c4-32768*n)*(gain1/(n*65536))
      'fpar_5 = (c5-32768*n)*(gain1/(n*65536))
      'fpar_6 = (c6-32768*n)*(gain1/(n*65536))
      'fpar_7 = (c7-32768*n)*(gain1/(n*65536))
      'fpar_8 = (c8-32768*n)*(gain1/(n*65536))
      'fpar_9 = (c9-32768*n)*(gain1/(n*65536))
      'fpar_10 = (c10-32768*n)*(gain1/(n*65536))
      data_4 = c7/n | SHIFT_LEFT(c8/n,16)
      c7 = 0 : c8 = 0
      'data_5 = c9/n | SHIFT_LEFT(c10/n,16)
      'data_1 = READADC(1) + SHIFT_LEFT(READADC(2),16) ' channel 1 / 2
      '    fpar_11 = (c11-32768*n)*(gain1/(n*65536))
      '    fpar_12 = (c12-32768*n)*(gain1/(n*65536))
      '    fpar_13 = (c13-32768*n)*(gain1/(n*65536))
      '    fpar_14 = (c14-32768*n)*(gain1/(n*65536))
      '    fpar_15 = (c15-32768*n)*(gain1/(n*65536))
      '    fpar_16 = (c16-32768*n)*(gain1/(n*65536))
      par_4 = n
      'c9=0
      'c10=0
      '    c11=0
      '    c12=0
      '    c13=0
      '    c14=0
      '    c15=0
      '    c16=0
      fpar_4 =  Max_Float(0,Min_Float(100,out))
      DAC(2,fpar_4*Min_float(fpar_7,10)+32768)
      n = 0
      t4 = Read_Timer()
      par_5 = (t4-t3-offset) * 25
    Else  
      t4 = Read_Timer()
      par_6 = (t4-t3-offset) * 25
    endif
    'par_3 = n
    Sleep(25)
  Else 
    par_1=1             ' there was data leakage, because the Fifo is full.
    ' Par_1 is watched by the PC program. If Par_1 = 1, 
    ' the PC-program shows an error message.
  EndIf
  par_8=(Read_Timer()-t1 - offset)*25
   
Finish: '#########################################################
  



  




