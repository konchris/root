
Sleep(26)

Start_Conv(11b)
chanpattern = 011011b ' Set the channel pattern to channels 7 and 8
muxpattern = (chanpattern | shift_left(gain07, 6)) | shift_left(gain08, 8)
Set_Mux(muxpattern)
Wait_EOC(11b)
c3 = c3 + ReadADC(1)
c4 = c4 + ReadADC(2)

Sleep(26)

Start_Conv(11b)
chanpattern = 1001000b ' Set the channel pattern to channels 9 and 10
muxpattern = (chanpattern | shift_left(gain09, 6)) | shift_left(gain10, 8)
Set_Mux(muxpattern)
Wait_EOC(11b)
c7 = c7 + ReadADC(1)
c8 = c8 + ReadADC(2)

Sleep(26)

Start_Conv(11b)
chanpattern = 000000b ' Set the channel pattern to channels 1 and 2
muxpattern = (chanpattern | shift_left(gain01, 6)) | shift_left(gain02, 8)
Set_Mux(muxpattern)
Wait_EOC(11b)
c9 = c9 + ReadADC(1)
c10 = c10 + ReadADC(2)
