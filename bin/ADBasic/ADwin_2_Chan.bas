'<ADbasic Header, Headerversion 001.001>
' Process_Number                 = 1
' Initial_Processdelay           = 303
' Eventsource                    = Timer
' Control_long_Delays_for_Stop   = No
' Priority                       = High
' Version                        = 1
' ADbasic_Version                = 5.0.8
' Optimize                       = Yes
' Optimize_Level                 = 1
' Info_Last_Save                 = METHIS  METHIS\Chris
'<Header End>
' Basic ADwin program for two channel output.
'
' The program averages over len number of data points an stores
' this value in a FIFO buffer based on the channel the data
' is read from.
' The user has the opportunity to set the effective measurement
' rate and the gain with which each channel is read.
' For more information on this program please see the pdf document
' "ADwin Programming".
'
' Author: Christopher Espy
' Last Change: 2013-10-01
'
' Please note that the following global variables are being used
' by the program:
' Par_1 through Par_16: The is where the user can set the measurement
'         range for each respective channel externally.
' Data_1 through Data_16: These are FIFO buffers where an external
'         program can collect the averaged measurement data.
' FPar_1 through FPar_16: The user can activate these to be able to 
'         watch the current value from each channel.
' Par_20: This is where the user can set the effective sample rate
'         to something other than the default value of 10 Hz.
' Par_21: This is where the user can check to see how many data
'         points are averaged over.

#Define BufferSize 100000

Dim data_1[BufferSize] as float as fifo
Dim data_2[BufferSize] as float as fifo

Dim gain01, gain02 as long
Dim range01, range02 as float

Dim muxpattern as long
Dim chanpattern as long

Dim c1, c2, len, n as long

Init: 'Beginning of a program block for initialization.
  Processdelay = 303
  If (par_20 = 0) Then
    len = 13201
  Else
    len = 1 / (Processdelay * par_20 * 25E-9)
  EndIf
  par_21 = len

  If (par_1 = 0) Then
    gain01 = 00b  ' set gain to 1
    range01 = 20
  Else
    range01 = par_1
    gain01 = LN(20 / range01) / LN(2)
  EndIf

  If (par_2 = 0) Then
    gain02 = 00b  ' set gain to 1
    range02 = 20
  Else
    range02 = par_2
    gain02 = LN(20 / range02) / LN(2)
  EndIf

  chanpattern = 000000b ' set initial channels to 1 and 2

  muxpattern = (chanpattern | shift_left(gain01, 6)) | shift_left(gain02, 8)

  Set_Mux(muxpattern)
  Sleep(65)

Event:
  Start_Conv(11b)

  Wait_EOC(11b)

  c1 = c1 + ReadADC(1)
  c2 = c2 + ReadADC(2)

  Inc n
  If (n > len) Then
    data_1 = (c1 - 32768 * n) * (range01 / (n * 65536))
    data_2 = (c2 - 32768 * n) * (range02 / (n * 65536))

    c1 = 0
    c2 = 0
    n = 0
  EndIf

Finish:
