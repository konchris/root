'<ADbasic Header, Headerversion 001.001>
' Process_Number                 = 1
' Initial_Processdelay           = 1430
' Eventsource                    = Timer
' Control_long_Delays_for_Stop   = No
' Priority                       = High
' Version                        = 1
' ADbasic_Version                = 5.0.8
' Optimize                       = Yes
' Optimize_Level                 = 1
' Info_Last_Save                 = URD  urd\Measurement
'<Header End>
' Basic ADwin program for eight channel output and PID control.
'
' The program averages over len number of data points and stores
' this value in a FIFO buffer based on the channel the data
' is read from.
' The user has the opportunity to set the effective measurement
' rate and the gain with which each channel is read.
' In addition the user has the opportunity to do PID control on
' input channel 10, with the output going from output channel 2.
' For more information on this program please see the pdf document
' "ADwin PID With 8 Channels".
'
' NOTE 2014-07-10:  The input channel for PID control has been
' changed to FPar_26.
' There is no averaging done on this parameter!
'
' Author: Christopher Espy
' Last Change: 2014-07-10
'
' Please note that the following global variables are being used
' by the program:
' Par_1 through Par_16: The is where the user can set the measurement
'         range for each respective channel externally.
' Data_1 through Data_25: These are FIFO buffers where an external
'         program can collect the averaged measurement data.
' FPar_1 through FPar_25: The user can activate these to be able to 
'         watch the current value from each channel.
' Par_20: This is where the user can set the effective sample rate
'         to something other than the default value of 10 Hz.
' Par_21: This is where the user can check to see how many data
'         points are averaged over.
' Par_22: The user can set the limiting factor of the integral term
' Par_23: The user can set a limiting factor for the output voltage
'         applied on output channel 2.
' Par_28: Displays the output voltage applied to the control element
'         via output channel 2.
' Par_29: Displays the current average error
' Par_30: The user can set the setpoint, in digits.
' Par_31: The user sets the femto voltage amplification factor
' Par_32: The user sets the femto current amplification factor
' Par_33: The user sets the LakeShore 370 analog output high value in Ohm
' Par_34: The user sets the LakeShore 370 analog output low values in Ohm
' FPar_26: This is where the value from the A&H (reading the capacitive thermometer)
'          is input from an external program. 
' FPar_31: P - gain or proportional factor
' FPar_32: I - reset or integration time
' FPar_33: D - rate or derivative time
' FPar_34: The user sets the voltage lock-in's sensitivity in V
' FPar_35: The user sets the current lock-in's sensitivity in V
' FPar_36: The user inputs the p0 value from the RuO thermometer fit parameters
' FPar_37: The user inputs the p1 value from the RuO thermometer fit parameters
' FPar_38: The user inputs the r0 value from the RuO thermometer fit parameters
' FPar_39: This holds the calculated voltage-to-resistance slope for the LakeShore 370 analog output
' FPar_40: This holds the calculated voltage-to-resistance offset for the LakeShore 370 analog output

' Define a variable and set the value for the
' maximum size the FIFO buffers can get.
#Define BufferSize 100000
' The offset corresponds to 0 V, i.e. the middle
' of the 16-bit range (0 = -10 V, 65536 = + 10 V).
' This variable just helps typing in formulas later.
#Define offset 32768

' Setup all of the buffers needed.
Dim data_1[BufferSize] as float as fifo
Dim data_2[BufferSize] as float as fifo
Dim data_3[BufferSize] as float as fifo
Dim data_4[BufferSize] as float as fifo
Dim data_5[BufferSize] as float as fifo
Dim data_6[BufferSize] as float as fifo
Dim data_7[BufferSize] as float as fifo
Dim data_8[BufferSize] as float as fifo
Dim data_9[BufferSize] as float as fifo
Dim data_10[BufferSize] as float as fifo
Dim data_17[BufferSize] as float as fifo
Dim data_18[BufferSize] as float as fifo
Dim data_19[BufferSize] as float as fifo
Dim data_20[BufferSize] as float as fifo
Dim data_21[BufferSize] as float as fifo
Dim data_22[BufferSize] as float as fifo
Dim data_23[BufferSize] as float as fifo
Dim data_24[BufferSize] as float as fifo
Dim data_25[BufferSize] as float as fifo

' Define variables for each channel's gain and range (which are
' related to each other).
Dim gain01, gain02, gain03, gain04, gain05, gain06 as long
Dim gain07, gain08, gain09, gain10 as long
Dim range01, range02, range03, range04, range05, range06 as float
Dim range07, range08, range09, range10 as float
Dim VAmp, IAmp, LSHigh, LSLow as long
Dim LVSens, LISens, p0, p1, r0 as float
Dim vrslope, vroffset as float

' These variables hold the channel and multiplexer
' patterns
Dim muxpattern as long
Dim chanpattern as long

' Define variables to hold the data directly read from each ADC
Dim c1, c2, c3, c4, c5, c6, c7, c8, len, n as long
Dim c9, c10 as long

' PID is accomplished using the following formula:
'        mv = P(e + I*Iterm + D*Dterm)
' where
' sp - the setpoint, in digits, from the user
' pv - the current process value
' err = (sp - pv) - the error calculated for the current
'      cycle
' errold - the error from the last cycle
' mv - manipulation value or output to send to the
'      controller, or heater in this case, in digits
' Iterm - the integral term as defined in PID theory
' Dterm - the differential term as defined in PID
'         theory
' dt - the time needed for a single process, in
'      seconds.
'      This is needed for the calculations of Iterm
'      and Dterm.

Dim sp, pv, Ilim, mvlim as long
Dim err, errold, mv as float
Dim P, I, D, Iterm, Dterm, dt as float

Init: 'Beginning of a program block for initialization.
' Set the processdelay and the number of data points
' that are averaged over.
  Processdelay = 1430
  dt = Processdelay * 25E-9
  If (par_20 = 0) Then
    len = 2797
  Else
    len = 1 / (Processdelay * par_20 * 25E-9)
  EndIf
  par_21 = len

' The gain factor of each ADC channel can be set individually.
' If the gain is changed from the default value of 1, then the
' corresponding voltage range needs to be calculated.
' This range is needed to convert digits to volts.
If (par_1 = 0) Then
  gain01 = 00b  ' set gain to 1
  range01 = 20
Else
  range01 = par_1
  gain01 = LN(20 / range01) / LN(2)
EndIf

If (par_2 = 0) Then
  gain02 = 00b  ' set gain to 1
  range02 = 20
Else
  range02 = par_2
  gain02 = LN(20 / range02) / LN(2)
EndIf

If (par_3 = 0) Then
   gain03 = 00b  ' set gain to 1
   range03 = 20
Else
   range03 = par_3
   gain03 = LN(20 / range03) / LN(2)
EndIf

If (par_4 = 0) Then
   gain04 = 00b  ' set gain to 1
   range04 = 20
Else
   range04 = par_4
   gain04 = LN(20 / range04) / LN(2)
EndIf

If (par_7 = 0) Then
   gain07 = 00b  ' set gain to 1
   range07 = 20
Else
   range07 = par_7
   gain07 = LN(20 / range07) / LN(2)
EndIf

If (par_8 = 0) Then
   gain08 = 00b  ' set gain to 1
   range08 = 20
Else
   range08 = par_8
   gain08 = LN(20 / range08) / LN(2)
EndIf

If (par_9 = 0) Then
   gain09 = 00b  ' set gain to 1
   range09 = 20
Else
   range09 = par_9
   gain09 = LN(20 / range09) / LN(2)
EndIf

If (par_10 = 0) Then
   gain10 = 00b  ' set gain to 1
   range10 = 20
Else
   range10 = par_10
   gain10 = LN(20 / range10) / LN(2)
EndIf

chanpattern = 000000b ' set initial channels to 1 and 2

' Create the muxpattern from the chanpattern and gains for each channel.
muxpattern = (chanpattern | shift_left(gain01, 6)) | shift_left(gain02, 8)

' Set a bunch of the variables to initial values for PID
P = FPar_31
I = FPar_32
D = FPar_33
Iterm = 0
Dterm = 0
errold = 0
' We set the error to zero because we first take an
' average of len number of pv values and calculate
' an average error from that.
err = 0

' Setup the integral term limiting factor.
' If the user didn't supply a value then
' set the default value of 1000
If (par_22 = 0) Then
   Ilim = 1E3
Else
   Ilim = Par_22
   Par_22 = 1E3
EndIf

' Setup the voltage output limiting term.
' Remember that 32768 corresponds to 0 V!
If (par_23 = 0) Then
   mvlim = 37520
   Par_23 = 37520
Else
   mvlim = par_23
EndIf

' If no Setpoint is given set it to the minimum
If (par_30 = 0) Then
   par_30 = 65536
EndIf

' Grab the parameters for making the calculations
If (par_31 = 0) Then
  par_31 = 10000
Endif
If (par_32 = 0) Then
  par_32 = 100000000
Endif
If (par_33 = 0) Then
  par_33 = 6660
Endif
If (par_34 = 0) Then
  par_34 = 1250
Endif
If (fpar_34 = 0) Then
  fpar_34 = 0.01
Endif
If (fpar_35 = 0) Then
  fpar_35 = 0.5
Endif
If (fpar_36 = 0) Then
  fpar_36 = 8.584
Endif
If (fpar_37 = 0) Then
  fpar_37 = -1.156
Endif
If (fpar_38 = 0) Then
  fpar_38 = 1259.9
Endif

VAmp = par_31
IAmp = par_32
LSHigh = par_33
LSLow = par_34
LVSens = fpar_34
LISens = fpar_35
p0 = fpar_36
p1 = fpar_37
r0 = fpar_38


vrslope = (LSHigh - LSLow) / (20)
fpar_39 = vrslope
vroffset = (LSHigh + LSLow) / 2
fpar_40 = vroffset

Set_Mux(muxpattern) ' Set the multiplexer to channels 1 and 2
Sleep(65)

Event:
  Start_Conv(11b) ' Start the ADC conversion on channels 1 and 2

  chanpattern = 001001b ' Set the channel pattern to channels 3 and 4
  muxpattern = (chanpattern | shift_left(gain03, 6)) | shift_left(gain04, 8)
  Set_Mux(muxpattern) ' Set the multiplexer to channels 3 and 4

  ' Calculate the manipulation value.
  mv = P * (err + I * Iterm + D * Dterm) + offset
  Par_28 = mv
  ' We want to limit the power output to our heater
  ' to protect the system.
  ' These numbers are based on calculations using
  ' system value of R = 460 Ohm.
  If (mv > mvlim) Then mv = mvlim
  If (mv < offset) Then mv = offset

  ' Change the output of channel 2 to the manipulation value
  ' just calculated
  DAC(2, mv)

  Wait_EOC(11b)

c1 = c1 + ReadADC(1)
c2 = c2 + ReadADC(2)

Sleep(28)

Start_Conv(11b)
chanpattern = 011011b ' Set the channel pattern to channels 7 and 8
muxpattern = (chanpattern | shift_left(gain07, 6)) | shift_left(gain08, 8)
Set_Mux(muxpattern)

Inc n
  
If (n > len) Then
   ' Average I and V voltage inputs and write to buffer
   fpar_1 = (c1 - offset * n) * (range01 / (n * 65536))
   data_1 = fpar_1
   fpar_2 = (c2 - offset * n) * (range02 / (n * 65536))
   data_2 = fpar_2
   ' Calculate adjusted values and write to buffer
   fpar_17 = (fpar_1 / IAmp) * 10^6
   data_17 = fpar_17
   fpar_18 = (fpar_2 / VAmp) * 10^3
   data_18 = fpar_18
   ' Calculate resistance (R) and write to buffer
   fpar_19 = (fpar_18 / fpar_17) * 10^3
   data_19 = fpar_19
EndIf

Wait_EOC(11b)

c3 = c3 + ReadADC(1)
c4 = c4 + ReadADC(2)

Sleep(27)

Start_Conv(11b)
chanpattern = 100100b ' Set the channel pattern to channels 9 and 10
muxpattern = (chanpattern | shift_left(gain09, 6)) | shift_left(gain10, 8)
Set_Mux(muxpattern)

If (n > len) Then
   ' Average dI and dV voltage inputs and write to buffer
   fpar_3 = (c3 - offset * n) * (range03 / (n * 65536))
   data_3 = fpar_3
   fpar_4 = (c4 - offset * n) * (range04 / (n * 65536))
   data_4 = fpar_4
   ' Calculate adjusted values and write to buffer
   fpar_20 = ((fpar_3 / IAmp) / 10) * LISens * 10^6
   data_20 = fpar_20
   fpar_21 = ((fpar_4 / VAmp) / 10) * LVSens * 10^3
   data_21 = fpar_21
   ' Calculate resistance (dR) and write to buffer
   fpar_22 = (fpar_21 / fpar_20) * 10^3
   data_22 = fpar_22
EndIf  

Wait_EOC(11b)
c7 = c7 + ReadADC(1)
c8 = c8 + ReadADC(2)

Sleep(50)

Start_Conv(11b)
chanpattern = 000000b ' Set the channel pattern to channels 1 and 2
muxpattern = (chanpattern | shift_left(gain01, 6)) | shift_left(gain02, 8)
Set_Mux(muxpattern)

If (n > len) Then
   ' Average the xMagnet and zMagnet voltage inputs and write to buffer 
   fpar_7 = (c7 - offset * n) * (range07 / (n * 65536))
   data_5 = fpar_7
   fpar_8 = (c8 - offset * n) * (range08 / (n * 65536))
   data_6 = fpar_8
EndIf

Wait_EOC(11b)
c9 = c9 + ReadADC(1)
c10 = c10 + ReadADC(2)

If (n > len) Then
   ' Set the old error
   errold = err
   ' Get the setpoint
   sp = Par_30
   ' calculate the average pv
   pv = FPar_26
   ' calculate the current error
   err = pv - sp
   Par_29 = err
   ' calculate the integral term
   ' note that the time between each average error
   ' is the single process time (cycle time), dt,
   ' times the number of processes to collect the
   ' data, n.
   Iterm = Iterm + dt * n * err
   If (Iterm > Ilim) Then Iterm = Ilim
   If (Iterm < -Ilim) Then Iterm = -Ilim
   ' calculate the differential term
   Dterm = (err - errold) / (dt * n)

' Average over the TCap and VRuO values and write to buffer
fpar_9 = (c9 - offset * n) * (range09 / (n * 65536))
data_7 = fpar_9
fpar_10 = (c10 - offset * n) * (range10 / (n * 65536))
data_8 = fpar_10
' Calculate the Res_RuO
fpar_23 = fpar_10 * vrslope + vroffset
data_23 = fpar_23
' Calculate the Temp_RuO
fpar_25 = p0+(p1*ln(fpar_23-r0))
data_25 = fpar_25
fpar_24 = exp(fpar_25)
data_24 = fpar_24

VAmp = par_31
    IAmp = par_32
    LSHigh = par_33
    LSLow = par_34
    LVSens = fpar_34
    LISens = fpar_35
    p0 = fpar_36
    p1 = fpar_37
    r0 = fpar_38

  c1 = 0
  c2 = 0
  c3 = 0
  c4 = 0
  c7 = 0
  c8 = 0
  c9 = 0
  c10 = 0
  n = 0
  P = FPar_31
  I = FPar_32
  D = FPar_33
  Ilim = Par_22
  mvlim = Par_23
EndIf

Finish:
  ' For fail-safe reasons always finish by setting the
  ' output of the control channel to 0 V.
  DAC(2, offset)
  Par_28 = offset
