'<ADbasic Header, Headerversion 001.001>
' Process_Number                 = 1
' Initial_Processdelay           = 1000
' Eventsource                    = Timer
' Control_long_Delays_for_Stop   = No
' Priority                       = High
' Version                        = 1
' ADbasic_Version                = 5.0.8
' Optimize                       = Yes
' Optimize_Level                 = 1
' Info_Last_Save                 = METHIS  METHIS\Chris
'<Header End>
#Define BufferSize 10000

Dim data_1[BufferSize] as float as fifo
Dim data_2[BufferSize] as float as fifo
Dim gain01 as long
Dim gain02 as long
Dim muxpattern as long
Dim chanpattern as long
Dim c1, c2 as long

LowInit: 'Beginning of a program block for initialization.
  gain01 = 00b
  gain02 = 11b
  chanpattern = 000000b ' set the channels to 1 and 2
  
Init:
  muxpattern = chanpattern | shift_left(gain01, 6)
  muxpattern = muxpattern | shift_left(gain02, 8)
  fpar_10 = muxpattern
  fpar_11 = 20 / (2^gain01)
  par_12 = 20 / (2^19)
  Set_Mux(muxpattern)
  Sleep(65)
  
Event:
  Start_Conv(11b)
  Wait_EOC(11b)
  c1 = ReadADC(1)
  c2 = ReadADC(2)
  
  par_1 = c1
  fpar_1 = (c1 - 32768) * 20 / (2^(16+gain01))
  par_2 = c2
  fpar_2 = (c2 - 32768) * (20)/65536
Finish:
