#!/usr/bin/env bash 
# -*- coding: utf-8 -*-

LOGFILE="/home/chris/Documents/PhD/root/log/raw-data-sync.log"

RSYNC_OPTIONS="-avhz --update --stats -e ssh --exclude=*/.PowerFolder/* $*"

SOURCE="/home/chris/Documents/PhD/root/raw-data/"
TARGET="fulla:Documents/PhD/root/raw-data/"

echo "********************************************" >> $LOGFILE

echo "$(date): Pushing from $SOURCE to $TARGET" >> $LOGFILE

rsync $RSYNC_OPTIONS $SOURCE $TARGET >> $LOGFILE

echo "********************************************" >> $LOGFILE
echo "" >> $LOGFILE
