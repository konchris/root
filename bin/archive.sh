#!/usr/bin/env bash 
# -*- coding: utf-8 -*-

DIR="$*"

LOGFILE="/home/chris/Documents/PhD/root/log/archive.log"

TARCMD="tar -cJf $DIR.tar.xz $DIR/"

RMCMD="rm -rf $DIR/"

echo "$(date): Archiving $DIR" >> $LOGFILE

$TARCMD >> $LOGFILE
