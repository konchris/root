#!/usr/bin/env bash 
# -*- coding: utf-8 -*-

LOGFILE="/home/chris/Documents/PhD/root/log/data-sync.log"

RSYNC_OPTIONS="-avhz --update --stats -e ssh --no-i-r --exclude=*/.PowerFolder/* --exclude=*/data_bak/* $*"

SOURCE="/home/chris/Documents/PhD/root/data/"
TARGET="fulla:Documents/PhD/root/data/"

echo "********************************************" | tee $LOGFILE

echo "$(date): Pushing from $SOURCE to $TARGET" | tee $LOGFILE

rsync $RSYNC_OPTIONS $SOURCE $TARGET | tee $LOGFILE

echo "********************************************" | tee $LOGFILE
echo "" | tee $LOGFILE
