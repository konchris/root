#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import argcomplete
import csv


sys.path.append(os.path.join(os.path.expanduser('~'), 'Documents', 'PhD',
                             'root', 'lib'))

from Locations import (RAW_DIR)


def sample_runs(parsed_args, **kwargs):
    sample_dir = os.path.join(RAW_DIR, parsed_args.sample, 'cryo_measurements')
    return next(os.walk(sample_dir))[1]


def samples(**kwargs):
    return next(os.walk(RAW_DIR))[1]


def main(argv=None):
    """The main function."""

    prog_desc = "Generate Summary Plots of TSweep-type TDMS measurement files."
    parser = argparse.ArgumentParser(description=prog_desc)
    parser.add_argument('sample', help='The sample for which the summaries'
                        ' should be generated').completer = samples
    parser.add_argument('sample_run', help='The measurement run for which'
                        ' summaries should be generated').completer = \
        sample_runs
    parser.add_argument('-r', '--run', help="the default action is a dry run, "
                        "this executes moving", action='store_true',
                        dest='run')
    parser.set_defaults(run=False)
    argcomplete.autocomplete(parser)

    args = parser.parse_args()

    SAMPLE_DIR = os.path.join(args.sample)
    SAMPLE_RUN = args.sample_run

    full_dir = os.path.join(RAW_DIR, SAMPLE_DIR, 'cryo_measurements',
                            SAMPLE_RUN)

    RENAMES = os.path.join(full_dir, 'Renames.csv')

    if not os.path.exists(RENAMES):
        print('Renamces.csv does not exist! Please create it.')

    # Get the renames
    with open(RENAMES) as csvfile:
        csvreader = csv.reader(csvfile)

        # Files moved counter
        i = 0
        # Files deleted counter
        j = 0
        # Files not needing renaming counter
        d = 0
        # Lines in Rename.csv
        r = 0
        # Missing original files counter
        m = 0

        # Rename each file individually
        for row in csvreader:

            r += 1

            # The original filename
            orig = row[1]
            # The file extension
            ext = orig.split('.')[-1]
            # List of elements for the new filename
            new = row[2:]
            # Remove empty fields from the filename list
            new01 = [x for x in new if x]
            new02 = [x for x in new01 if x != '0']
            # Create the new name
            renamed = '_'.join(new02)+'.'+ext

            # Move the file
            if 'Filename' not in orig and 'Dev' not in orig\
                    and 'Dev' not in renamed:

                # The full original path
                full_orig = os.path.join(full_dir, orig)
                # The full new path
                full_renamed = os.path.join(full_dir, renamed)

                # Only move if the original exists and the new does not
                if (os.path.exists(full_orig) and
                        not os.path.exists(full_renamed)):

                    # Increase the moved file counter
                    i += 1

                    print('{} Moving {} to {}'.format(i, orig, renamed))

                    if args.run:
                        os.rename(full_orig, full_renamed)

                elif (os.path.exists(full_orig) and
                      os.path.exists(full_renamed)):

                    d += 1
                    print(d, 'Both paths exist:', orig, renamed)

                elif not os.path.exists(full_orig):
                    m += 1
                    print('For some reason the original path does not exist',
                          orig)

            elif 'Filename' not in orig and ('Dev' in orig or
                                             'Dev' in renamed):

                # The full original path
                full_orig = os.path.join(full_dir, orig)
                # The full new path
                full_renamed = os.path.join(full_dir, renamed)

                if os.path.exists(full_orig):
                    j += 1
                    print('{} Deleting {}'.format(j, orig))

                    if args.run:
                        os.remove(full_orig)
                elif os.path.exists(full_renamed):
                    j += 1
                    print('{} Deleting {}'.format(j, renamed))
                    if args.run:
                        os.remove(full_renamed)

    print('\n')
    print(i, 'files moved.')
    print(j, 'files deleted')
    print(d, "files don't need to be renamed")
    print(m, 'original files missing from the count')
    print(i+j+d+m, "files handled")
    print(r, 'lines in Renames.csv\n')

    if not args.run:
        print('***This was just a test!***\n')

    sys.exit()

if __name__ == "__main__":
    main()
