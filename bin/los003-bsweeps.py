#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys

sys.path.append(os.path.join(os.path.expanduser('~'), 'Documents', 'PhD',
                             'root', 'lib'))

sys.path.append(os.path.join(os.path.expanduser('~'), 'Documents', 'PhD',
                             'root', 'results', 'los003'))

import matplotlib as mpl
mpl.use('Qt4Agg')
import matplotlib.pyplot as plt

import seaborn as sns
sns.set_style("whitegrid")

import numpy as np
# import pandas as pd
# from scipy import stats
# from scipy.interpolate import interp1d

# from ChannelModel import ChannelRegistry

from Constants import PHI_0
from Helper_Mod import (load_sweep_data)  # , SAMPLE_DIR, save_data)
SAMPLE_RUN = 'cooldown-2015-05-18'


# def main(argv=None):
#     """The main function."""

# 1) Load Data
print('Loading...')
(df, df_extra) = load_sweep_data('bsweep', sample_run=SAMPLE_RUN)

# 2) Add the normalized resistance in Ohm
normal_resistanace = 23.3

# 3) Add the small loop size in nm
r = 324.245

# 4) Generate the color palette
i = 0
for key in df.keys():
    if 'down' in key:
        i += 1

sns.set_palette("coolwarm_r", i)
sns.set_context("talk", font_scale=1.25, rc={"lines.linewidth": 1.5})

print('Plotting...')

fig01 = plt.figure()

ax01 = fig01.add_axes([0.1, 0.1, 0.7, 0.75])

legend_entry = []

for key in sorted(df.keys())[::-1]:
    if 'down' in key:
        temp_df = df[key]['ADWin']

        x = temp_df['$\Phi / \Phi_0$']
        y = temp_df['$dR/dR_N$']

        ax01.plot(x, y)

        legend_entry.append(key)

ax01.legend(legend_entry, loc='center left', bbox_to_anchor=(1.05, 0.5),
            ncol=1, prop={'size': 10})

xmin = -10.5
xmax = 10.5
bmin = xmin * PHI_0 / r**2
bmax = xmax * PHI_0 / r**2
ax01.set_xlim(xmin, xmax)
ax01_B = ax01.twiny()
ax01_B.set_xlim(bmin, bmax)
ax01_B.grid(False)
ax01_B.set_xlabel('B [mT]')

# bticks = np.hstack((xmin, np.linspace(xmin, xmax,
#                                       (xmax - xmin) / 2.5 + 1))) \
#                    * PHI_0 / r**2

# ax01_B.set_xticks(np.delete(bticks, [1]))

ax01.set_ylabel(r'$dR / dR_N$ = {res:.1f} $\Omega$'
                .format(res=normal_resistanace))
ax01.set_xlabel(r'$\Phi / \Phi_0$')
ax01_B.set_title('Magnet Field Sweeps', y=1.12)

print('Showing...')
# fig01.tight_layout()
plt.show()

# sys.exit()

# if __name__ == "__main__":
#     main()
