#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import argcomplete
import csv

sys.path.append(os.path.join(os.path.expanduser('~'), 'Documents', 'PhD',
                             'root', 'lib'))

from ChannelModel import ChannelRegistry
from Locations import (RAW_DIR, DOCS_DIR, DATA_DIR)


def sample_runs(parsed_args, **kwargs):
    sample_dir = os.path.join(RAW_DIR, parsed_args.sample, 'cryo_measurements')
    return next(os.walk(sample_dir))[1]


def samples(**kwargs):
    return next(os.walk(RAW_DIR))[1]


def main(argv=None):
    """The main function."""

    prog_desc = "Generate Summary Plots of TSweep-type TDMS measurement files."
    parser = argparse.ArgumentParser(description=prog_desc)
    parser.add_argument('sample', help='The sample, and network if there is'
                        ' one, whose data files should be generated').completer\
        = samples
    parser.add_argument('sample_run', help='The measurement run for which'
                        ' summaries should be generated').completer = \
        sample_runs
    parser.add_argument('--overwrite', dest='overwrite', action='store_true')
    parser.set_defaults(overwrite=False)
    argcomplete.autocomplete(parser)

    args = parser.parse_args()

    # Split up the args.sample into sample_name and network, if there is one
    SAMPLE = args.sample.split('/')[0]
    try:
        SAMPLE_NETWORK = args.sample.split('/')[1]
    except IndexError:
        SAMPLE_NETWORK = ''

    SAMPLE_DIR = os.path.join(SAMPLE)

    SAMPLE_RUN = args.sample_run

    # Create the source directory, i.e. where the raw-data files are stored.
    source_dir = os.path.join(RAW_DIR, SAMPLE_DIR, SAMPLE_NETWORK,
                              'cryo_measurements', SAMPLE_RUN)

    # Create the documentation directory, where the file lists are stored.
    documents_dir = os.path.join(DOCS_DIR, SAMPLE_DIR)

    # Create the destination directory, where the data files will be saved.
    destination_dir = os.path.join(DATA_DIR, SAMPLE_DIR, SAMPLE_NETWORK,
                                   'cryo_measurements', SAMPLE_RUN)

    # Create the path to the files list
    file_list = os.path.join(DOCS_DIR, SAMPLE, '{}.csv'.format(SAMPLE_RUN))

    # Make sure the above sources exist
    assert os.path.exists(source_dir), ('The source directory for sample'
                                        ' {} does not exist'.format(SAMPLE))

    assert os.path.exists(documents_dir), ('The doc directory for sample'
                                           ' {} does not exist'.format(SAMPLE))

    assert os.path.exists(file_list), ('The files list for sample'
                                       ' {} does not exist'.format(SAMPLE))

    # Create the destination directory if it does not exist
    if not os.path.exists(destination_dir):
        print('Creating {}'.format(destination_dir))
        os.makedirs(destination_dir)

    chan_reg = ChannelRegistry()

    # Sample SiO2Al002 raw-data files have the extensioni 'dat'.
    # All others are 'tdms'.
    if SAMPLE == 'sio2al002':
        EXT = 'dat'
    else:
        EXT = 'tdms'

    with open(file_list) as csvfile:

        csvreader = csv.DictReader(csvfile)

        for row in csvreader:

            # Find out if the file has been flagged for use.
            # Flagged means the 'Use?' field contains '1'.
            # If a file is not to be used then the 'Use?' field is either an
            # empty string '', or '0'.
            try:
                truth = int(row['Use?'])
            except ValueError:
                truth = row['Use?']

            basename = '{f}.{e}'.format(f=row['Filename'], e=EXT)
            filepath = os.path.join(source_dir, basename)

            target_basename = '{f}.{e}'.format(f=row['Filename'], e='h5')
            target_dir = source_dir.replace('raw-', '')
            target_path = os.path.join(target_dir, target_basename)

            if truth and (not os.path.exists(target_path) or args.overwrite):

                print('\nLoading', filepath)
                chan_reg.loadFromFile(filepath)

                print('\tAdding transport channels')
                chan_reg.addTransportChannels()

                print('\tRemoving magnetifield zeros')
                chan_reg.removeMagetfieldZeros()

                print('\tInterpolating magnetfield to ADWin')
                chan_reg.addInterpolatedB()

                print('\tAdding sample temperature')
                chan_reg.add_TSample_AD()

                print('\tExporting')
                chan_reg.exprtToFile()

    sys.exit()

if __name__ == "__main__":
    main()
