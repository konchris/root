#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import argcomplete

sys.path.append(os.path.join(os.path.expanduser('~'), 'Documents', 'PhD',
                             'root', 'lib'))

import matplotlib as mpl
mpl.use('AGG')
import seaborn as sns
import matplotlib.pyplot as plt

from ChannelModel import ChannelRegistry
from Locations import (RAW_DIR, RESULTS_DIR)

# Options to implement:
# 1) Overwrite existing image files or not
# 2) Pass individual file names


def sample_runs(parsed_args, **kwargs):
    sample_dir = os.path.join(RAW_DIR, parsed_args.sample, 'cryo_measurements')
    return next(os.walk(sample_dir))[1]


def samples(**kwargs):
    return next(os.walk(RAW_DIR))[1]


def main(argv=None):
    """The main function."""

    prog_desc = "Generate Summary Plots of BSweep-type TDMS measurement files."
    parser = argparse.ArgumentParser(description=prog_desc)
    parser.add_argument('sample', help='The sample for which the summaries'
                        ' should be generated').completer = samples
    parser.add_argument('sample_run', help='The measurement run for which'
                        ' summaries should be generated').completer = \
        sample_runs
    parser.add_argument('-B', '--magnetfield', help='the name of the magnet'
                        ' field channel to use if different from B or zMagnet')
    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    SAMPLE_DIR = os.path.join(args.sample, 'cryo_measurements')
    SAMPLE_RUN = args.sample_run

    sns.set_context("paper", font_scale=1.25, rc={"lines.linewidth": 1})
    sns.set_style("whitegrid")

    full_dir = os.path.join(RAW_DIR, SAMPLE_DIR, SAMPLE_RUN)
    summaries_dir = os.path.join(RESULTS_DIR, SAMPLE_DIR, SAMPLE_RUN,
                                 'summaries')

    if args.magnetfield and args.magnetfield not in ['B', 'zMagnet']:
        print('Invalid magnet field argument:', args.magnetfield)

    chan_reg = ChannelRegistry()

    if not os.path.exists(summaries_dir):
        print('Creating {}'.format(summaries_dir))
        os.makedirs(summaries_dir)

    # i = 0
    # Walk through the raw-data files
    for (roots, dir_names, fnames) in os.walk(full_dir):
        if 'PowerFolder' not in roots and\
          'back' not in roots and\
          'backup' not in roots:

            for fname in fnames:

                # Generate the raw data filename
                filename = os.path.join(roots, fname)

                # Test that you created the name correctly
                assert os.path.exists(filename), ('The file {} does not exist!'
                                                  .format(filename))

                # Generate the picture name based on the base file name
                picture_base = fname.rstrip('tdms') + 'png'
                picture_name = os.path.join(summaries_dir, picture_base)

                if ('tdms' in filename) and ('tdms_index' not in filename) and \
                   (('BSweep' in filename) or
                    ('BSweek' in filename) or
                    ('IPS' in filename) or
                    ('Bramp' in filename) or
                    ('BRamp' in filename)) and \
                   (not os.path.exists(picture_name)):

                    print('Generating {}'.format(os.path.
                                                 basename(picture_name)))

                    chan_reg.clear()

                    chan_reg.loadFromFile(filename)

                    try:
                        chan_reg.removeMagetfieldZeros()
                        chan_reg.addInterpolatedB()
                    except IndexError as err:
                        print(fname, err)

                    try:
                        temp_AD = chan_reg['proc01/ADWin/TSample_AD'].data[::10]
                    except KeyError:
                        chan_reg.add_TSample_AD()
                        temp_AD = chan_reg['proc01/ADWin/TSample_AD'].data[::10]

                    chan_reg.addTransportChannels()

                    #chan_reg.addTemperatureMode()

                    fig, ax = plt.subplots(ncols=2, nrows=3,
                                           figsize=(8.27, 11.7))

                    temp = temp_AD.mean()
                    fig.suptitle('{:.0f} mK'.format(temp*1000), fontsize=14,
                                 fontweight='bold')

                    # Plot 1: dR vs B
                    print('\tPlotting dR vs B')

                    ax[0][0].grid(True)

                    # Get the magnet field
                    if not args.magnetfield:
                        try:
                            B = chan_reg['proc01/ADWin/B'].data[::10]
                        except KeyError:
                            B = chan_reg['proc01/ADWin/zMagnet'].data[::10]
                    else:
                        BChan = 'proc01/ADWin/{}'.format(args.magnetfield)
                        B = chan_reg[BChan].data[::10]

                    # Get the resistance
                    try:
                        dR = chan_reg['proc01/ADWin/dR'].data[::10]
                    except KeyError:
                        dR = chan_reg['proc01/ADWin/dRSample'].data[::10]

                    ax[0][0].plot(B, dR)
                    ax[0][0].set_xlabel('B [mT]')
                    ax[0][0].set_ylabel(r'dR [$\Omega$]')

                    # Plot 2: B and Magentfield vs Time
                    print('\tPlotting B vs t')

                    # Get times
                    time_AD = chan_reg['proc01/ADWin/Time_m'].data[::10]
                    try:
                        time_IPS = chan_reg['proc01/IPS/Time_m'].data[::10]
                        Magnetfield = chan_reg['proc01/IPS/Magnetfield'].data[::10]
                    except KeyError:
                        time_IPS = None
                        Magnetfield = None

                    # Plot B vs Time
                    ln1 = ax[0][1].plot(time_AD, B, lw=1.5, label='B')

                    ax1 = ax[0][1].twinx()

                    # Plot IPS/Magnetfield vs Time
                    ln2 = ax1.plot(time_IPS, Magnetfield,
                                   color=sns.xkcd_rgb['pale red'],
                                   label='IPS B (right)')

                    lns = ln1+ln2
                    labs = [l.get_label() for l in lns]
                    ax[0][1].legend(lns, labs, loc='best')
                    ax[0][1].set_xlabel('Time [min]')
                    ax[0][1].set_ylabel('B [mT]')
                    ax1.set_ylabel('Magnetfield [T]')
                    ax1.grid(False)

                    # Plot 3: TSample vs Magnetfield.
                    print('\tPlotting T vs B')

                    ax[1][0].get_yaxis().get_major_formatter()\
                            .set_useOffset(False)

                    ax[1][0].plot(B, temp_AD)

                    T_std_p = temp + temp_AD.std()
                    T_std_n = temp - temp_AD.std()

                    ax[1][0].axhline(temp, color=sns.xkcd_rgb['pale red'])
                    ax[1][0].axhline(T_std_p, color=sns.xkcd_rgb['pale red'],
                                     lw=1, ls=':')
                    ax[1][0].axhline(T_std_n, color=sns.xkcd_rgb['pale red'],
                                     lw=1, ls=':')

                    ax[1][0].set_xlabel('B [mT]')
                    ax[1][0].set_ylabel('T [mK]')

                    # Plot 4: ITC Temperatures vs Time
                    print('\tPlotting ITC Ts vs t')

                    ax3 = ax[1][1].twinx()

                    # Get ITC data
                    try:
                        time_ITC = chan_reg['proc01/ITC503/Time_m'].data[::10]
                        T1K = chan_reg['proc01/ITC503/T1K'].data[::10]
                        THe3 = chan_reg['proc01/ITC503/THe3'].data[::10]
                    except KeyError:
                        time_ITC = None
                        T1K = None
                        THe3 = None

                    if time_ITC is not None:
                        try:
                            TSorp = chan_reg['proc01/ITC503/TSorp'].data[::10]
                        except KeyError:
                            TSorp = chan_reg['proc01/ITC503/Sorption'].data[::10]
                    else:
                        TSorp = None

                    ln1 = ax[1][1].plot(time_ITC, TSorp, label='TSorp')
                    ln2 = ax3.plot(time_ITC, T1K,
                                   color=sns.xkcd_rgb['medium green'],
                                   label='T1K (right)')
                    ln3 = ax3.plot(time_ITC, THe3,
                                   color=sns.xkcd_rgb['pale red'],
                                   label='THe3 (right)')

                    ln4 = ax3.plot(time_AD, temp_AD,
                                   color=sns.xkcd_rgb['light purple'],
                                   label='TSample (right)')

                    lns = ln1 + ln2 + ln3 + ln4
                    labs = [l.get_label() for l in lns]
                    ax3.legend(lns, labs, loc='best')

                    ax3.grid(False)

                    ax[1][1].set_xlabel('Time [min]')
                    ax[1][1].set_ylabel('T [K]')
                    ax3.set_ylabel('T [K]')

                    # Plot 5: Current vs B
                    print('\tPlotting I vs B')

                    # Get current
                    try:
                        I = chan_reg['proc01/ADWin/I'].data[::10]
                    except KeyError:
                        I = chan_reg['proc01/ADWin/ISample'].data[::10]

                    ax[2][0].plot(B, I)

                    ax[2][0].set_xlabel('B [T]')
                    ax[2][0].set_ylabel(r'I [$\mu$A]')

                    # Plot 6: Cap vs Temp
                    print('\tPlotting C vs t')

                    # Get cap
                    if ['Cap' in x for x in chan_reg.keys()]:
                        try:
                            cap = chan_reg['proc01/ADWin/TCap'].data[::10]
                        except KeyError:
                            cap = chan_reg['proc01/ADWin/Cap'].data[::10]

                        ax[2][1].plot(temp_AD*1000, cap)
                    else:
                        ax[2][1].plot()


                    ax[2][1].get_xaxis().get_major_formatter().set_useOffset(False)
                    ax[2][1].get_yaxis().get_major_formatter().set_useOffset(False)
                    ax[2][1].set_xlabel('T [mK]')
                    ax[2][1].set_ylabel('C [pF]')

                    fig.tight_layout()

                    fig.savefig(picture_name, bbox_inches='tight', dpi=90)

                    print('{} successfully created'
                          .format(os.path.basename(picture_name)))

                    plt.close(fig)

    sys.exit()

if __name__ == "__main__":
    main()
