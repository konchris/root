#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import argcomplete
import csv


sys.path.append(os.path.join(os.path.expanduser('~'), 'Documents', 'PhD',
                             'root', 'lib'))

from Locations import (RAW_DIR)


def sample_runs(parsed_args, **kwargs):
    sample_dir = os.path.join(RAW_DIR, parsed_args.sample,
                              parsed_args.sample_network, 'cryo_measurements')
    return next(os.walk(sample_dir))[1]


def sample_networks(parsed_args, **kwargs):
    network_dir = os.path.join(RAW_DIR, parsed_args.sample)
    return next(os.walk(network_dir))[1]


def samples(**kwargs):
    return next(os.walk(RAW_DIR))[1]


def main(argv=None):
    """The main function."""

    prog_desc = "Generate Summary Plots of TSweep-type TDMS measurement files."
    parser = argparse.ArgumentParser(description=prog_desc)
    parser.add_argument('sample', help='The sample for which the summaries'
                        ' should be generated').completer = samples
    parser.add_argument('sample_network', help='The network on the sample '
                        ' that was measured').completer = sample_networks
    parser.add_argument('sample_run', help='The measurement run for which'
                        ' summaries should be generated').completer = \
                        sample_runs
    argcomplete.autocomplete(parser)

    args = parser.parse_args()

    SAMPLE_DIR = os.path.join(args.sample)
    SAMPLE_NETWORK = args.sample_network
    SAMPLE_RUN = args.sample_run

    full_dir = os.path.join(RAW_DIR, SAMPLE_DIR, SAMPLE_NETWORK,
                            'cryo_measurements', SAMPLE_RUN)

    RENAMES = os.path.join(full_dir, 'Renames.csv')

    if not os.path.exists(RENAMES):
        print('Renamces.csv does not exist! Please create it.')

    # Get the renames
    with open(RENAMES) as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            orig = row[1]
            new = row[2:]
            new01 = [x for x in new if x]
            renamed = '_'.join(new01)+'.dat'
            if 'Filename' not in orig:
                full_orig = os.path.join(full_dir, orig)
                full_renamed = os.path.join(full_dir, renamed)
                if os.path.exists(full_orig) and \
                  not os.path.exists(full_renamed):
                    print('Moving {} to {}'.format(orig, renamed))
                    os.rename(full_orig, full_renamed)

    # Walk through the raw-data files
    #for (roots, dir_names, fnames) in os.walk(full_dir):
    #    if 'PowerFolder' not in roots:

    #        for fname in fnames:

    #            if 'dat' in fname:

    #                print(fname)

    sys.exit()

if __name__ == "__main__":
    main()
