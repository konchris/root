#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import argcomplete

sys.path.append(os.path.join(os.path.expanduser('~'), 'Documents', 'PhD',
                             'root', 'lib'))

import matplotlib as mpl
mpl.use('AGG')
import seaborn as sns
import matplotlib.pyplot as plt

from ChannelModel import ChannelRegistry
from Locations import (RAW_DIR, RESULTS_DIR)

# Options to implement:
# 1) Overwrite existing image files or not
# 2) Pass individual file names


def sample_runs(parsed_args, **kwargs):
    sample_dir = os.path.join(RAW_DIR, parsed_args.sample, 'cryo_measurements')
    return next(os.walk(sample_dir))[1]


def samples(**kwargs):
    return next(os.walk(RAW_DIR))[1]


def main(argv=None):
    """The main function."""

    prog_desc = "Generate Summary Plots of TSweep-type TDMS measurement files."
    parser = argparse.ArgumentParser(description=prog_desc)
    parser.add_argument('sample', help='The sample for which the summaries'
                        ' should be generated').completer = samples
    parser.add_argument('sample_run', help='The measurement run for which'
                        ' summaries should be generated').completer = \
                        sample_runs
    argcomplete.autocomplete(parser)

    args = parser.parse_args()

    SAMPLE_DIR = os.path.join(args.sample, 'cryo_measurements')
    SAMPLE_RUN = args.sample_run

    sns.set_context("paper", font_scale=1.25, rc={"lines.linewidth": 1})
    sns.set_style("whitegrid")

    full_dir = os.path.join(RAW_DIR, SAMPLE_DIR, SAMPLE_RUN)
    summaries_dir = os.path.join(RESULTS_DIR, SAMPLE_DIR, SAMPLE_RUN,
                                 'summaries')

    chan_reg = ChannelRegistry()

    if not os.path.exists(summaries_dir):
        print('Creating {}'.format(summaries_dir))
        os.mkdir(summaries_dir)

    # Walk through the raw-data files
    for (roots, dir_names, fnames) in os.walk(full_dir):
        if 'PowerFolder' not in roots:

            for fname in fnames:

                # Generate the raw data filename
                filename = os.path.join(roots, fname)

                # Test that you created the name correctly
                assert os.path.exists(filename), ('The file {} does not exist!'
                                                  .format(filename))

                # Generate the picture name based on the base file name
                picture_base = fname.rstrip('tdms') + 'png'
                picture_name = os.path.join(summaries_dir, picture_base)

                if ('tdms' in filename) and \
                   ('tdms_index' not in filename) and \
                   (('IVSweep' in filename) or
                    ('IVTesting' in filename) or
                    ('IV-Test' in filename) or
                    ('IVTest' in filename) or
                    ('IV-Sweep' in filename) or
                    ('IRamp' in filename)) and \
                   (not os.path.exists(picture_name)):

                    print('Generating {}'.format(os.path
                                                 .basename(picture_name)))

                    chan_reg.clear()

                    chan_reg.loadFromFile(filename)

                    try:
                        T = chan_reg['proc01/ADWin/TSample_AD'].data
                    except KeyError:
                        chan_reg.add_TSample_AD()
                        T = chan_reg['proc01/ADWin/TSample_AD'].data

                    chan_reg.addTransportChannels()

                    try:
                        fig, ax = plt.subplots(ncols=2, nrows=2,
                                               figsize=(8.27, 11.7))

                        # Plot 1: V vs I
                        print('\tPlotting V vs I')

                        I = chan_reg['proc01/ADWin/I'].data
                        V = chan_reg['proc01/ADWin/V'].data

                        T = chan_reg['proc01/ADWin/TSample_AD'].data

                        temp = T.mean()
                        fig.suptitle('{:.0f} mK'.format(temp*1000),
                                     fontsize=14,
                                     fontweight='bold')


                        ax[0][0].grid(True)

                        ax[0][0].plot(I, V)

                        ax[0][0].set_xlabel(r'I [$\mu$A]')
                        ax[0][0].set_ylabel('V [mV]')

                        # Plot 2: B and Magentfield vs Time
                        print('\tPlotting T vs t')

                        t = chan_reg['proc01/ADWin/Time_m'].data

                        try:
                            I = chan_reg['proc01/ADWin/I'].data
                        except KeyError:
                            I = chan_reg['proc01/ADWin/ISample'].data

                        try:
                            dI = chan_reg['proc01/ADWin/dI'].data
                        except KeyError:
                            dI = chan_reg['proc01/ADWin/dISample'].data

                        try:
                            V = chan_reg['proc01/ADWin/V'].data
                        except KeyError:
                            V = chan_reg['proc01/ADWin/VSample'].data

                        try:
                            dV = chan_reg['proc01/ADWin/dV'].data
                        except KeyError:
                            dV = chan_reg['proc01/ADWin/dVSample'].data

                        ln1 = ax[0][1].plot(t, I, lw=1.5, label='I')
                        ln2 = ax[0][1].plot(t, V, lw=1.5,
                                            color=sns.xkcd_rgb['medium green'],
                                            label='V')
                        ax1 = ax[0][1].twinx()
                        ln3 = ax1.plot(t, dI,
                                       color=sns.xkcd_rgb['pale red'],
                                       label='dI (right)')
                        ln4 = ax1.plot(t, dV,
                                       color=sns.xkcd_rgb['light purple'],
                                       label='dV (right)')
                        lns = ln1+ln2+ln3+ln4
                        labs = [l.get_label() for l in lns]
                        ax[0][1].legend(lns, labs, loc='best')
                        ax[0][1].set_xlabel('Time [min]')
                        ax[0][1].set_ylabel(r'[$\mu$A, mV]')
                        ax1.set_ylabel(r'[$\mu$A, mV]')
                        ax1.grid(False)

                        # Plot 3: Cap vs Temperature
                        print('\tPlotting C vs T')

                        T1 = T[T <= 300]
                        try:
                            Cap = chan_reg['proc01/ADWin/Cap'].data[T <= 300]

                            ax[1][0].plot(T1[Cap > 1]*1000, Cap[Cap > 1])
                        except KeyError:
                            pass

                        ax[1][0].get_xaxis().get_major_formatter()\
                          .set_useOffset(False)
                        ax[1][0].get_yaxis().get_major_formatter()\
                          .set_useOffset(False)
                        ax[1][0].set_xlabel('T [mK]')
                        ax[1][0].set_ylabel('C [pF]')

                        # Plot 4: ITC Temperatures vs Time
                        print('\tPlotting ITC Ts vs t')

                        ax1 = ax[1][1].twinx()
                        try:
                            t1 = chan_reg['proc01/ITC503/Time_m'].data
                            TSorp = chan_reg['proc01/ITC503/TSorp'].data
                            T1K = chan_reg['proc01/ITC503/T1K'].data
                            THe3 = chan_reg['proc01/ITC503/THe3'].data

                            ln1 = ax[1][1].plot(t1, TSorp, label='TSorp')
                            ln2 = ax1.plot(t1, T1K,
                                           color=sns.xkcd_rgb['medium green'],
                                           label='T1K (right)')
                            ln3 = ax1.plot(t1, THe3,
                                           color=sns.xkcd_rgb['pale red'],
                                           label='THe3 (right)')
                        except KeyError:
                            ln1 = ax[1][1].plot()
                            ln2 = ax[1][1].plot()
                            ln3 = ax[1][1].plot()

                        ln4 = ax1.plot(t[T <= 300], T[T <= 300],
                                       color=sns.xkcd_rgb['light purple'],
                                       label='TSampleAD (right)')
                        lns = ln1 + ln2 + ln3 + ln4
                        labs = [l.get_label() for l in lns]
                        ax1.legend(lns, labs, loc='best')
                        ax1.grid(False)

                        ax[1][1].set_xlabel('Time [min]')
                        ax[1][1].set_ylabel('T [K]')
                        ax1.set_ylabel('T [K]')

                        fig.tight_layout()

                        fig.savefig(picture_name, bbox_inches='tight', dpi=90)

                        print('{} successfully created'
                              .format(os.path.basename(picture_name)))

                        plt.close(fig)

                    except ValueError as err:
                        print(fname, err)

    sys.exit()

if __name__ == "__main__":
    main()
