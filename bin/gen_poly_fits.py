#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import argcomplete

sys.path.append(os.path.join(os.path.expanduser('~'), 'Documents', 'PhD',
                             'root', 'lib'))

import matplotlib as mpl
mpl.use('AGG')
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator

from scipy.interpolate import interp1d
from scipy.optimize import curve_fit

from Locations import (DATA_DIR, RESULTS_DIR)
from MiscFunctions import save_figure
from Helper_Mod import load_sweep_data


def sample_runs(parsed_args, **kwargs):
    sample_dir = os.path.join(DATA_DIR, parsed_args.sample, 'cryo_measurements')
    return next(os.walk(sample_dir))[1]


def samples(**kwargs):
    return next(os.walk(DATA_DIR))[1]


def main(argv=None):
    """The main function."""

    prog_desc = "Generate Summary Plots of TSweep-type TDMS measurement files."
    parser = argparse.ArgumentParser(description=prog_desc)
    parser.add_argument('sample', help='The sample, and network if there is'
                        ' one, whose data files should be generated').completer\
        = samples
    parser.add_argument('sample_run', help='The measurement run for which'
                        ' summaries should be generated').completer = \
        sample_runs
    argcomplete.autocomplete(parser)

    args = parser.parse_args()

    # Split up the args.sample into sample_name and network, if there is one
    SAMPLE = args.sample.split('/')[0]

    try:
        SAMPLE_NETWORK = args.sample.split('/')[1]
    except IndexError:
        SAMPLE_NETWORK = ''

    SAMPLE_DIR = os.path.join(SAMPLE)

    SAMPLE_RUN = args.sample_run

    sns.set_context("paper", font_scale=1.25, rc={"lines.linewidth": 1})
    sns.set_style("whitegrid")

    # Get the boundaries from the samples module in the library
    if SAMPLE == 'sio2al002':
        from sio2al002 import BOUNDARIES
    elif SAMPLE == 'omri01':
        from omri01 import BOUNDARIES

    # Create the DATA DIRECTORY, i.e. where the raw-data files are stored.
    source_dir = os.path.join(DATA_DIR, SAMPLE_DIR, SAMPLE_NETWORK,
                              'cryo_measurements', SAMPLE_RUN)

    # Create the RESULTS DIRECTORY, where the file lists are stored.
    results_dir = os.path.join(RESULTS_DIR, SAMPLE_DIR, 'cryo_measurements',
                                 SAMPLE_RUN)

    # Make sure the above sources exist
    assert os.path.exists(source_dir), ('The source directory for sample'
                                        ' {} does not exist'.format(SAMPLE))

    # Create the destination directory if it does not exist
    if not os.path.exists(results_dir):
        print('Creating {}'.format(results_dir))
        os.makedirs(results_dir)

    # Set the resistance device
    if SAMPLE in ['sio2al002']:
        res_device = 'all'
    else:
        res_device = 'ADWin'

    s_dir = source_dir.rstrip(SAMPLE_RUN)

    # Load the data
    df = load_sweep_data('bsweep', s_dir, SAMPLE_RUN, root='proc02')

    for key in sorted(df.keys()):
        # Select the data
        pos_df = df[key][res_device][df[key][res_device]['$\Phi / \Phi_0$'] >= 0]
        neg_df = df[key][res_device][df[key][res_device]['$\Phi / \Phi_0$'] <= 0]

        t_pos = pos_df.Time_m
        t_neg = neg_df.Time_m
        t1 = df[key][res_device]['Time_m']

        y_pos = pos_df.dRSample
        y_neg = neg_df.dRSample
        y1 = df[key][res_device]

        

if __name__ == "__main__":
    main()
