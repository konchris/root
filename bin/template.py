#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Chris's python module template

This the file I will start with for writing all of my python scripts.

"""

import sys
import os
import argparse

# Because I am mostly dealing with data analysis load these modules as
# a part of standard procedure
import numpy as np
import matplotlib as mpl
mpl.use('Qt4Agg')
import matplotlib.pyplot as plt

__author__ = "Christopher Espy"
__copyright__ = "Copyright (C) 2013, Christopher Espy"
__credits__ = ["Christopher Espy"]
__license__ = "GPL"
__version__ = "0.0.0"
__maintainer__ = "Christopher Espy"
__email__ = "christopher.espy@uni-konstanz.de"
__status__ = "Development"

PROGNAME = os.path.basename(sys.argv[0])
PROGVERSION = __version__

class Usage(Exception):
    """Define custom usage exceptions for the script/module"""
    
    def __init__(self, msg):
        """Construct the exception
        
        Parameters
        ----------
        msg :
        
        """
        self.msg = msg
        
def main():
    """
    
    """
    prog_desc = "The basic template for my Unix-like command-line programs"
    parser = argparse.ArgumentParser(description=prog_desc)

    args = parser.parse_args()

    print "Hello World!"
        

if __name__ == '__main__':
    main()

    
