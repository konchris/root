#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import argcomplete

sys.path.append(os.path.join(os.path.expanduser('~'), 'Documents', 'PhD',
                             'root', 'lib'))

import matplotlib as mpl
mpl.use('AGG')
import seaborn as sns
import matplotlib.pyplot as plt

from ChannelModel import ChannelRegistry
from Locations import (RAW_DIR, RESULTS_DIR)

# Options to implement:
# 1) Overwrite existing image files or not
# 2) Pass individual file names


def sample_runs(parsed_args, **kwargs):
    sample_dir = os.path.join(RAW_DIR, parsed_args.sample, 'cryo_measurements')
    return next(os.walk(sample_dir))[1]


def samples(**kwargs):
    return next(os.walk(RAW_DIR))[1]


def main(argv=None):
    """The main function."""

    prog_desc = "Generate Summary Plots of TSweep-type TDMS measurement files."
    parser = argparse.ArgumentParser(description=prog_desc)
    parser.add_argument('sample', help='The sample for which the summaries'
                        ' should be generated').completer = samples
    parser.add_argument('sample_run', help='The measurement run for which'
                        ' summaries should be generated').completer = \
        sample_runs
    argcomplete.autocomplete(parser)

    args = parser.parse_args()

    SAMPLE_DIR = os.path.join(args.sample, 'cryo_measurements')
    SAMPLE_RUN = args.sample_run

    sns.set_context("paper", font_scale=1.25, rc={"lines.linewidth": 1})
    sns.set_style("whitegrid")

    full_dir = os.path.join(RAW_DIR, SAMPLE_DIR, SAMPLE_RUN)
    summaries_dir = os.path.join(RESULTS_DIR, SAMPLE_DIR, SAMPLE_RUN,
                                 'summaries')

    chan_reg = ChannelRegistry()

    # Define colors here
    green = sns.xkcd_rgb['medium green']
    red = sns.xkcd_rgb['pale red']
    blue = sns.xkcd_rgb['denim blue']

    print(full_dir)

    if not os.path.exists(summaries_dir):
        print('Creating {}'.format(summaries_dir))
        os.makedirs(summaries_dir)

    # Walk through the raw-data files
    for (roots, dir_names, fnames) in os.walk(full_dir):
        if 'PowerFolder' not in roots and\
          'back' not in roots and\
          'backup' not in roots:

            for fname in fnames:

                # Generate the raw data filename
                filename = os.path.join(roots, fname)

                # Test that you created the name correctly
                assert os.path.exists(filename), ('The file {} does not exist!'
                                                  .format(filename))

                # Generate the picture name based on the base file name
                if 'tdms' in filename:
                    picture_base = fname.rstrip('tdms') + 'png'
                    ADWin = 'ADWin'
                    ITC = 'ITC503'
                elif 'dat' in filename:
                    picture_base = fname.rstrip('dat') + 'png'
                    ADWin = 'all'
                    ITC = 'all'
                picture_name = os.path.join(summaries_dir, picture_base)

                if (('tdms' in filename) or 'dat' in filename)and \
                   ('tdms_index' not in filename) and \
                   (('Cooldown' in filename) or
                    ('Condense' in filename) or
                    ('TSweep' in filename) or
                    ('Termalize' in filename) or
                    ('Thermalize' in filename) or
                    ('Heat_Test' in filename) or
                    ('Heat-Test' in filename) or
                    ('Labview-Test' in filename) or
                    ('He-Transfer' in filename) or
                    ('ZN-Test' in filename) or
                    ('Cool-to' in filename) or
                    ('TRamp' in filename) or
                    ('Tramp' in filename) or
                    ('ITC' in filename) or
                    ('Hold' in filename) or
                    ('Pump' in filename) or
                    ('PID' in filename) or
                    ('Warm-up' in filename) or
                    ('Warm-Up' in filename) or
                    ('Warm_up' in filename)) and \
                   ('NO_ADWIN' not in filename) and \
                   (not os.path.exists(picture_name)):

                   # (('45mT' not in filename) or
                   #  ('60mT' not in filename) or
                   #  ('30mT' not in filename) or
                   #  ('120mT' not in filename) or
                   #  ('BTRamp' not in filename)) and \

                    print('Generating {}'.format(os.path
                                                 .basename(picture_name)))

                    chan_reg.clear()

                    try:
                        chan_reg.loadFromFile(filename)

                        chan_reg.add_TSample_AD()

                        print(list(chan_reg.keys())[0])

                        fig, ax = plt.subplots(ncols=2, nrows=2,
                                               figsize=(8.27, 11.7))

                        # Plot 1: dR vs T
                        print('\tPlotting dR vs T')

                        if 'proc01/{}/TSample'.format(ADWin) not in \
                           chan_reg.keys():

                            try:
                                T = chan_reg['proc01/{}/TSample_AD'
                                             .format(ADWin)].data
                            except KeyError:
                                chan_reg.add_TSample_AD()
                                T = chan_reg['proc01/{}/TSample_AD'
                                             .format(ADWin)].data

                        elif 'proc01/{}/TSample'.format(ADWin) in \
                             chan_reg.keys():
                            T = chan_reg['proc01/{}/TSample'
                                         .format(ADWin)].data

                        try:
                            dR = chan_reg['proc01/{}/dR'.format(ADWin)].data
                        except KeyError:
                            dR = chan_reg['proc01/{}/dRSample'
                                          .format(ADWin)].data

                        ax[0][0].grid(True)

                        ax[0][0].plot(T[T <= 4.5][::10], dR[T <= 4.5][::10])

                        ax[0][0].set_xlabel('T [K]')
                        ax[0][0].set_ylabel(r'dR [$\Omega$]')

                        # Plot 2: I, V, dI, dV vs Time
                        print('\tPlotting I, V, dI, dV vs t')

                        try:
                            t = chan_reg['proc01/{}/Time_m'.format(ADWin)].data
                        except KeyError:
                            t = chan_reg['proc01/{}/Time_s'.format(ADWin)].data

                        try:
                            I = chan_reg['proc01/{}/I'.format(ADWin)].data
                            V = chan_reg['proc01/{}/V'.format(ADWin)].data
                            dI = chan_reg['proc01/{}/dI'.format(ADWin)].data
                            dV = chan_reg['proc01/{}/dV'.format(ADWin)].data
                        except KeyError:
                            I = chan_reg['proc01/{}/ISample'
                                         .format(ADWin)].data
                            V = chan_reg['proc01/{}/VSample'
                                         .format(ADWin)].data
                            dI = chan_reg['proc01/{}/dISample'
                                          .format(ADWin)].data
                            dV = chan_reg['proc01/{}/dVSample'
                                          .format(ADWin)].data

                        ln1 = ax[0][1].plot(t[::10], I[::10], lw=1.5,
                                            label='I')
                        ln2 = ax[0][1].plot(t[::10], V[::10], lw=1.5,
                                            color=green,
                                            label='V')
                        ax1 = ax[0][1].twinx()
                        ln3 = ax1.plot(t[::10], dI[::10],
                                       color=red,
                                       label='dI (right)')
                        ln4 = ax1.plot(t[::10], dV[::10],
                                       color=blue,
                                       label='dV (right)')
                        lns = ln1+ln2+ln3+ln4
                        labs = [l.get_label() for l in lns]
                        ax[0][1].legend(lns, labs, loc='best')
                        ax[0][1].set_xlabel('Time [min]')
                        ax[0][1].set_ylabel(r'[$\mu$A, mV]')
                        ax1.set_ylabel(r'[$\mu$A, mV]')
                        ax1.grid(False)

                        # Plot 3: Cap vs Time
                        print('\tPlotting C vs t')

                        T1 = T[T <= 4.5]
                        try:
                            Cap = chan_reg['proc01/{}/Cap'
                                           .format(ADWin)].data[T <= 4.5]

                            ax[1][0].plot(T1[Cap > 1][::10]*1000,
                                          Cap[Cap > 1][::10])
                        except KeyError as err:
                            ax[1][0].plot()

                        ax[1][0].get_xaxis().get_major_formatter()\
                            .set_useOffset(False)
                        ax[1][0].get_yaxis().get_major_formatter()\
                            .set_useOffset(False)
                        ax[1][0].set_xlabel('T [mK]')
                        ax[1][0].set_ylabel('C [pF]')

                        # Plot 4: ITC Temperatures vs Time
                        print('\tPlotting ITC Ts vs t')

                        if 'proc01/{}/Time_m'.format(ITC) in chan_reg.keys() or \
                           'proc01/{}/Time_s'.format(ITC) in chan_reg.keys():

                            try:
                                TSorp = chan_reg['proc01/{}/TSorp'
                                                 .format(ITC)].data
                            except KeyError:
                                TSorp = chan_reg['proc01/{}/Sorption'
                                                 .format(ITC)].data

                            try:
                                t1 = chan_reg['proc01/{}/Time_m'
                                              .format(ITC)].data
                            except KeyError:
                                t1 = chan_reg['proc01/{}/Time_s'
                                              .format(ITC)].data

                            try:
                                T1K = chan_reg['proc01/{}/T1K'
                                               .format(ITC)].data
                                THe3 = chan_reg['proc01/{}/THe3'
                                                .format(ITC)].data

                                ln1 = ax[1][1].plot(t1[::10], TSorp[::10],
                                                    label='TSorp')
                                ax1 = ax[1][1].twinx()
                                ln2 = ax1.plot(t1[::10], T1K[::10],
                                               color=green,
                                               label='T1K (right)')
                                ln3 = ax1.plot(t1[::10], THe3[::10],
                                               color=red,
                                               label='THe3 (right)')
                                ln4 = ax1.plot(t[T <= 4.5][::10],
                                               T[T <= 4.5][::10],
                                               color=blue,
                                               label='TSampleAD (right)')
                                lns = ln1 + ln2 + ln3 + ln4
                                labs = [l.get_label() for l in lns]
                                ax1.legend(lns, labs, loc='best')
                                ax1.grid(False)

                                ax[1][1].set_xlabel('Time [min]')
                                ax[1][1].set_ylabel('T [K]')
                                ax1.set_ylabel('T [K]')
                            except KeyError as err:
                                print(fname, err)

                        fig.tight_layout()

                        fig.savefig(picture_name, bbox_inches='tight', dpi=90)

                        print('{} successfully created'
                              .format(os.path.basename(picture_name)))

                        plt.close(fig)

                    except ValueError as err:
                        print(fname, err)
                    except OverflowError:
                        print('Got the overflow error again on ', fname)

    sys.exit()

if __name__ == "__main__":
    main()
