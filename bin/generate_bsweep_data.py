#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" The main script.

"""

__author__ = "Christopher Espy"
__copyright__ = "Copyright (C) 2015, Christopher Espy"
__credits__ = ["Christopher Espy"]
__license__ = "GPLv2"
__version__ = "0.5"
__maintainer__ = "Christopher Espy"
__email__ = "christopher.espy@uni-konstanz.de"
__status__ = "Development"

import os
import sys
import argparse

import pandas as pd
import numpy as np

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..',
                             'lib'))
from FileLists import FILE_LIST
from TimeFunctions import calculate_minutes
from MiscFunctions import interpolate_bfield

DATA_DIR = '/home/chris/Documents/PhD/root/data'
RESULTS_DIR = '/home/chris/Documents/PhD/root/results'
SAVE_CHANNELS = {'ADWin': ['dR', 'B', 'TSample_AD', 'Time_m', 'Res_RuO',
                           'Cap', 'Phi/Phi_0']}


def load_gen_dimensions(dir, sample):
    """Load and generate loop and line dimensions for the sample.

    This function loads the dimensions for the sample from the user-generated
    'dimensions.csv' file in the data/<sample name> directory.
    At the minimum the file must contain (all in nanometers):
        - Two small loop side length dimensions (sl1, sl2)
        - Two large loop side length dimensions (ll1, ll2)
        - Two line width dimensions (w1, w2)
    The following dimensions may be included.
    If they are not included they will be automatically generated and written
    into the file:
        - Small loop area (s_area)
        - Large loop area (l_area)
        - Small loop average side length (sl_avg)
        - Large loop average side length (ll_avg)
        - Small loop effective radius (s_r)
              This is calulated with
                  r_eff = sqrt(A / pi)
        - Large loop effective radius (l_r)

    Parameters
    ----------
    dir : string
        The data directory
    sample : string
        The name of the sample

    Returns
    -------
    df_dimensions : pandas.DataFrame
        A data frame with one row and each column is one of the dimensions

    """
    new_dimension = False

    dimensions_path = os.path.join(dir, sample, 'dimensions.csv')

    # Check to see if the file exists and if it has anything in it, then open
    # If the file does not exist or is empty this funtion returns
    if os.path.exists(dimensions_path):
        try:
            df_dimensions = pd.read_csv(dimensions_path)
        except ValueError:
            print('Not all required dimensions have been supplied.')
            print('Please update the dimensions file to include:')
            print('\tTwo small loop line lengths,')
            print('\tTwo large loop line lengths,')
            print('\tTwo line widths.')
            sys.exit()
    else:
        print('The dimensions file does not yet exist for {}.'.format(sample))
        print('Please create it now.')
        sys.exit()

    # Check to see if the minimum required dimensions are present.
    # Return if they are not.
    for dim in ['sl1', 'sl2', 'll1', 'll2', 'w1', 'w2']:
        if not dim in df_dimensions.keys():
            print('Not all required dimensions have been supplied.')
            print('Please update the dimensions file to include:')
            print('\tTwo small loop line lengths,')
            print('\tTwo large loop line lengths,')
            print('\tTwo line widths.')
            sys.exit()

    # The following is not pretty, but it checks what dimensions need to be
    # calculated and performs the calculation
    if 's_area' not in df_dimensions:
        df_dimensions['s_area'] = df_dimensions['sl1'] * df_dimensions['sl2']
        new_dimension = True

    if 'l_area' not in df_dimensions:
        df_dimensions['l_area'] = df_dimensions['ll1'] * df_dimensions['ll2']
        new_dimension = True

    if 'sl_avg' not in df_dimensions:
        df_dimensions['sl_avg'] = np.average((df_dimensions['sl1'],df_dimensions['sl2']))
        new_dimension = True

    if 'll_avg' not in df_dimensions:
        df_dimensions['ll_avg'] = np.average((df_dimensions['ll1'],df_dimensions['ll2']))
        new_dimension = True

    if 's_r' not in df_dimensions:
        df_dimensions['s_r'] = np.sqrt(df_dimensions['s_area'] / np.pi)
        new_dimension = True

    if 'l_r' not in df_dimensions:
        df_dimensions['l_r'] = np.sqrt(df_dimensions['l_area'] / np.pi)
        new_dimension = True

    # If a new dimension was calculated, write all of the dimensions back to
    # the file
    if new_dimension:
        df_dimensions.to_csv(dimensions_path)

    return df_dimensions

def main(argv=None):
    """The main function."""

    # Setup argument parser
    prog_desc = "Generate a unified data file for all bsweeps for a sample."
    parser = argparse.ArgumentParser(description=prog_desc)
    parser.add_argument("sample", help="the name of the sample")
    parser.add_argument("sample_run", help="the name of the measurement series")
    parser.add_argument("resistance", help="measurement resistance regime")
    parser.add_argument("-w", "--write", help="whether to write the file",
                        action='store_true')
    parser.set_defaults(write=False)
    
    args = parser.parse_args()

    # Get the list of usable bsweep files
    file_list = FILE_LIST[args.sample][args.resistance]

    # Initiate the DataFrame dictionaries
    df_adwin_all = {}
    df_ips_all = {}
    df_itc_all = {}

    # Generate the filename to save bsweeps too
    save_file_name = 'BSweeps_{}_Ohm.h5'.format(args.resistance)

    store_path = os.path.join(DATA_DIR, args.sample, 'cryo_measurements',
                              args.sample_run, save_file_name)

    # Load the dimensions of the sample's rings
    df_dimensions = load_gen_dimensions(DATA_DIR, args.sample)

    # Begin loading the data from the files in the file list
    for fname in file_list:

        # Parse the data contained in the filename
        (datetime, meas_type, temp, goal, rate) = fname.split('_')

        # A few measurements were done with a rate of 2.5 mT/min.
        # The '.' can throw things off, so we'll just take it out for now and
        # have to re-insert it later when loading to file to doing the analysis
        rate = rate.split('.')[0]

        # To be able to sort the temprature strings in ascending order we need
        # to add a '0' to the front of temperatures in the hundreds of mK for
        # a valid comparison to temperatures in the thousands of mK (i.e. K).
        if len(temp) == 5:
            temp = '0' + temp

        full_path = os.path.join(DATA_DIR, args.sample, 'cryo_measurements',
                                 args.sample_run, fname)

        # Generate the dictionary key which will also serve a legend key for
        # any plots that are generated
        if '-' in goal:
            direction = 'down'
            key = temp + ' ' + direction
        elif '+' in goal:
            direction = 'up'
            key = temp + ' ' + direction

        # Open the file and add the data tables
        hdf_store = pd.HDFStore(full_path)

        df_adwin_all[key] = hdf_store['/raw/ADWin']
        df_ips_all[key] = hdf_store['/raw/IPS']
        df_ips_all[key] = df_ips_all[key][df_ips_all[key]['Magnetfield'] != 0]
        df_itc_all[key] = hdf_store['/raw/ITC503']

        hdf_store.close()

        # Create a time channel in minutes instead of just the timestamps
        df_adwin_all[key]['Time_m'] = calculate_minutes(df_adwin_all[key].index)
        df_ips_all[key]['Time_m'] = calculate_minutes(df_ips_all[key].index)
        df_itc_all[key]['Time_m'] = calculate_minutes(df_itc_all[key].index)

        # Create the magnet field channel in the ADWin table
        df_adwin_all[key]['B'] = interpolate_bfield(df_ips_all[key], df_adwin_all[key])
        
        # Generate the list of TimeSeries for each device that will be combined into 
        # that device's table to be saved
        save_list = []
        for save_chan in SAVE_CHANNELS['ADWin']:
            if save_chan in df_adwin_all[key]:
                save_list.append(df_adwin_all[key][save_chan])

        # Create the DataFrame to be saved and the internal path in the hdf5 file
        df_save = pd.concat(save_list, axis=1)
        save_path = 'proc/01/{t}/{d}/{r}/ADWin'.format(t=temp, d=direction, r=rate)

        if args.write:
            df_save.to_hdf(store_path, save_path, format='table')
            print('Successfully wrote {0} to \n\t{1}'.format(fname, store_path))

    if args.write:
        print('Wrote {0} data files to {1}'.format(len(file_list), store_path))
    elif not args.write:
        print('Tested writing {0} data files to {1}'.format(len(file_list),
                                                            store_path))
        #print(store_path, save_path)

if __name__ == "__main__":
    main()
