#!/usr/bin/env bash 
# -*- coding: utf-8 -*-

#LOGFILE="/home/chris/Documents/PhD/root/log/data-sync.log"

RSYNC_OPTIONS="-avh --update --stats --progress -e ssh --no-i-r --include="*/" --include="*.png" --exclude="*" $*"

TARGET="fulla:Documents/PhD/root/results/"
SOURCE="/home/chris/Documents/PhD/root/results/"

echo "********************************************" # >> $LOGFILE

echo "$(date): Pulling from $SOURCE to $TARGET" # >>  $LOGFILE

rsync $RSYNC_OPTIONS $SOURCE $TARGET # >> $LOGFILE

echo "********************************************" # >> $LOGFILE
echo "" # >> $LOGFILE
