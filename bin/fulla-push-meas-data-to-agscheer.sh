#!/usr/bin/env bash 
# -*- coding: utf-8 -*-

LOGFILE="/home/chris/Documents/PhD/root/log/meas-data-sync.log"

RSYNC_OPTIONS="-avhz --update --stats --no-i-r --info=progress2 --exclude=*/.PowerFolder/* --exclude=*/.ipynb_checkpoints/* $*"

SOURCE="/home/chris/Documents/PhD/root/raw-data/"
TARGET="/home/chris/Documents/AGScheer/Data/Data/Doktorarbeiten/Christopher_Espy/measurement_data"

if [ ! -d $TARGET ]; then
    echo "$TARGET does not exist!"
    exit 1
fi

if [ ! -f $LOGFILE ]; then
    echo "$LOGFILE does not exist!"
    exit 1
fi

echo "********************************************" 2>&1 >> $LOGFILE

echo "$(date): Pushing from $SOURCE to $TARGET" 2>&1 >> $LOGFILE

rsync $RSYNC_OPTIONS $SOURCE $TARGET 2>&1 >> $LOGFILE

echo "********************************************" 2>&1 >> $LOGFILE
echo "" 2>&1 >> $LOGFILE
