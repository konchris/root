#!/usr/bin/env bash 
# -*- coding: utf-8 -*-

LOGFILE="/home/chris/Documents/PhD/root/log/data-sync.log"

RSYNC_OPTIONS="-avhz --update --stats $*"

SOURCE="/home/chris/Espy/root/data/"
TARGET="/home/chris/Documents/PhD/root/data/"

echo "********************************************" >> $LOGFILE


echo "$(date): Pulling from $SOURCE to $TARGET" >> $LOGFILE

rsync $RSYNC_OPTIONS $SOURCE $TARGET >> $LOGFILE

echo "********************************************" >> $LOGFILE
echo "" >> $LOGFILE
